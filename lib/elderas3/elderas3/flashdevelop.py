import codecs
import re
import shlex

from lxml import etree

from buildtools import log

REG_FIND_BAD_SELFCLOSERS = re.compile(r'([^ ])/>')
FIX_BAD_SELFCLOSERS = '\\1 />'

REG_FIND_LONG_TAGS = re.compile(r'([ \t]*)<[^>]{100,}>')

tagsNeedingExpansion = [
    'intrinsics',
    'rslPaths',
    'library'
]
REG_FIND_SELFCLOSERS = re.compile('<({}) ?/>'.format('|'.join(tagsNeedingExpansion)))
FIX_SELFCLOSER = '<\\1></\\1>'


def countIndent(string):
    level = 0
    for c in string:
        if c == ' ':
            level += 1
        elif c == '\t':
            level += 4
        else:
            break
    return level


def _fixLongTag(match):
    tag = match.group(0)
    tagPaddingCount = countIndent(tag)
    tagParts = shlex.split(tag)
    tagPadding = ' ' * tagPaddingCount
    tagAttrPadding = ' ' * (len(tagParts[0]) + tagPaddingCount)
    newXML = tagPadding
    for i in xrange(len(tagParts)):
        if i > 1:
            newXML += '\n' + tagAttrPadding
        if i > 0:
            newXML += ' '
        part = tagParts[i].strip()
        suffix = ''
        if part.endswith('>'):
            suffix = '>'
            part = part[:-1]
        if '=' in part:
            attrChunks = part.split('=', 1)
            part = '{}="{}"'.format(*attrChunks)
        newXML += part + suffix
    return newXML


def fixDump(xml):
    xml = REG_FIND_BAD_SELFCLOSERS.sub(FIX_BAD_SELFCLOSERS, xml)
    xml = REG_FIND_LONG_TAGS.sub(_fixLongTag, xml)
    xml = REG_FIND_SELFCLOSERS.sub(FIX_SELFCLOSER, xml)
    return xml


class FlashDevelopProjectType(object):
    APPLICATION = 'Application'


class XMLSerializable(object):
    COMMENT_CONTENT = "This comment is here to work around a stupid FlashDevelop bug."

    def appendComment(self, parent, content=None):
        if content is None:
            content = self.COMMENT_CONTENT
        content = ' {} '.format(content)
        parent.append(etree.Comment(content))

    def serializeToNodeList(self, parent, parentElemName, elemName, attrName, data):
        listElem = parent.makeelement(parentElemName)
        if len(data) == 0:
            self.appendComment(listElem)
        else:
            for datum in data:
                listElem.append(listElem.makeelement(elemName, **{attrName: datum}))
        parent.append(listElem)

    def serializeDictToNodeList(self, parent, parentElemName, elemName, data):
        listElem = parent.makeelement(parentElemName)
        if len(data) == 0:
            self.appendComment(listElem)
        else:
            for k, v in data.items():
                listElem.append(listElem.makeelement(elemName, **{k: v}))
        parent.append(listElem)

    def serializeToXML(self):
        return


class FlashDevelopBuildSettings(XMLSerializable):

    def __init__(self):
        self.accessible = False
        self.advancedTelemetry = False
        self.allowSourcePathOverlap = False
        self.benchmark = False
        self.es = False
        self.inline = False
        self.locale = ''
        self.loadConfig = ''
        self.optimize = True
        self.omitTraces = True
        self.showActionScriptWarnings = True
        self.showBindingWarnings = True
        self.showInvalidCSS = True
        self.showDeprecationWarnings = True
        self.showUnusedTypeSelectorWarnings = True
        self.strict = True
        self.useNetwork = True
        self.useResourceBundleMetadata = True
        self.warnings = True
        self.verboseStackTraces = False
        self.linkReport = ''
        self.loadExterns = ''
        self.staticLinkRSL = True
        self.additional = ''
        self.compilerConstants = ''
        self.minorVersion = ''

    def serializeToXML(self, parent):
        return self.serializeDictToNodeList(parent, 'options', 'option', {
            'accessible': str(self.accessible),
            'advancedTelemetry': str(self.advancedTelemetry),
            'allowSourcePathOverlap': str(self.allowSourcePathOverlap),
            'benchmark': str(self.benchmark),
            'es': str(self.es),
            'inline': str(self.inline),
            'locale': str(self.locale),
            'loadConfig': str(self.loadConfig),
            'optimize': str(self.optimize),
            'omitTraces': str(self.omitTraces),
            'showActionScriptWarnings': str(self.showActionScriptWarnings),
            'showBindingWarnings': str(self.showBindingWarnings),
            'showInvalidCSS': str(self.showInvalidCSS),
            'showDeprecationWarnings': str(self.showDeprecationWarnings),
            'showUnusedTypeSelectorWarnings': str(self.showUnusedTypeSelectorWarnings),
            'strict': str(self.strict),
            'useNetwork': str(self.useNetwork),
            'useResourceBundleMetadata': str(self.useResourceBundleMetadata),
            'warnings': str(self.warnings),
            'verboseStackTraces': str(self.verboseStackTraces),
            'linkReport': str(self.linkReport),
            'loadExterns': str(self.loadExterns),
            'staticLinkRSL': str(self.staticLinkRSL),
            'additional': str(self.additional),
            'compilerConstants': str(self.compilerConstants),
            'minorVersion': str(self.minorVersion),
        })


class FlashDevelopOutput(XMLSerializable):

    def __init__(self):
        self.outputType = FlashDevelopProjectType.APPLICATION
        self.input = ''
        self.path = 'bin/TEST.swf'
        self.fps = 30
        self.width = 800
        self.height = 600
        self.version = 21
        self.minorVersion = 0
        self.platform = 'Flash Player'
        self.background = '#FFFFFF'

    def serializeToXML(self, project):
        return self.serializeDictToNodeList(project, 'output', 'movie', {
            'outputType': str(self.outputType),
            'input': str(self.input),
            'path': str(self.path),
            'fps': str(self.fps),
            'width': str(self.width),
            'height': str(self.height),
            'version': str(self.version),
            'minorVersion': str(self.minorVersion),
            'platform': str(self.platform),
            'background': str(self.background),
        })


class FlashDevelopProject(XMLSerializable):
    VERSION = 2

    def __init__(self):
        self.output = FlashDevelopOutput()

        self.classpaths = []
        self.includeLibraries = []
        self.libraryPaths = []
        self.externalLibraryPaths = []
        self.intrinsics = []
        self.library = []
        self.compileTargets = []
        self.hiddenPaths = ['obj']
        self.rslPaths = []

        self.build = FlashDevelopBuildSettings()

        self.showHiddenPaths = False
        self.testMovie = "Default"
        self.testMovieCommand = ""

    def serializeToXML(self):
        root = etree.Element('project', attrib={'version': str(self.VERSION)})
        self.appendComment(root,'Output SWF options')
        self.output.serializeToXML(root)
        self.appendComment(root,'Other classes to be compiled into your SWF')
        self.serializeToNodeList(root, 'classpaths', 'class', 'path', self.classpaths)
        self.appendComment(root,'Build options')
        self.build.serializeToXML(root)
        self.appendComment(root,'SWC Include Libraries')
        self.serializeToNodeList(root, 'includeLibraries', 'element', 'path', self.includeLibraries)
        self.appendComment(root,'SWC Libraries')
        self.serializeToNodeList(root, 'libraryPaths', 'element', 'path', self.libraryPaths)
        self.appendComment(root,'External Libraries')
        self.serializeToNodeList(root, 'externalLibraryPaths', 'element', 'path', self.externalLibraryPaths)
        self.appendComment(root,'Runtime Shared Libraries')
        self.serializeToNodeList(root, 'rslPaths', 'element', 'path', self.rslPaths)
        self.appendComment(root,'Intrinsic Libraries')
        self.serializeToNodeList(root, 'intrinsics', 'element', 'path', self.intrinsics)
        '''
        <!-- Assets to embed into the output SWF -->
        <library>
          <!-- example: <asset path="..." id="..." update="..." glyphs="..." mode="..." place="..." sharepoint="..." /> -->
        </library>
        '''
        # self.serializeToNodeList(root, 'library', 'asset', ...)
        self.appendComment(root,'Assets to embed into the output SWF')
        root.append(etree.Element('library'))
        self.appendComment(root,'Class files to compile (other referenced classes will automatically be included)')
        self.serializeToNodeList(root, 'compileTargets', 'compile', 'path', self.compileTargets)
        self.appendComment(root,'Paths to exclude from the Project Explorer tree')
        self.serializeToNodeList(root, 'hiddenPaths', 'hidden', 'path', self.hiddenPaths)
        self.appendComment(root,'Executed before build')
        root.append(etree.Element('preBuildCommand'))
        self.appendComment(root,'Executed after build')
        root.append(etree.Element('postBuildCommand', attrib={'alwaysRun': 'False'}))
        self.appendComment(root,'Other project options')
        self.serializeDictToNodeList(root, 'options', 'option', {
            'showHiddenPaths': str(self.showHiddenPaths),
            'testMovie': str(self.testMovie),
            'testMovieCommand': str(self.testMovieCommand),
        })
        self.appendComment(root,'Plugin storage')
        root.append(etree.Element('storage'))
        return root

    def Save(self, filename):
        with codecs.open(filename, 'w', encoding='utf-8') as f:
            f.write(fixDump(etree.tostring(self.serializeToXML(), pretty_print=True, xml_declaration=True, encoding='utf-8')))  # Must be serialized as HTML or FD will crash.
