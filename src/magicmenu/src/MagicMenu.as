class MagicMenu extends ItemMenu
{
   var bPCControlsReady = true;
   function MagicMenu()
   {
      super();
      this.bMenuClosing = false;
      this.iHideButtonFlag = 0;
   }
   function InitExtensions()
   {
      super.InitExtensions();
      gfx.io.GameDelegate.addCallBack("DragonSoulSpent",this,"DragonSoulSpent");
      gfx.io.GameDelegate.addCallBack("AttemptEquip",this,"AttemptEquip");
      this.BottomBar_mc.UpdatePerItemInfo({type:InventoryDefines.ICT_SPELL_DEFAULT});
      this.MagicButtonArt = [{PCArt:"M1M2",XBoxArt:"360_LTRT",PS3Art:"PS3_LTRT"},{PCArt:"F",XBoxArt:"360_Y",PS3Art:"PS3_Y"},{PCArt:"R",XBoxArt:"360_X",PS3Art:"PS3_X"},{PCArt:"Tab",XBoxArt:"360_B",PS3Art:"PS3_B"}];
      this.BottomBar_mc.SetButtonsArt(this.MagicButtonArt);
   }
   function PositionElements()
   {
      super.PositionElements();
      (MovieClip)this.InventoryLists_mc.Lock("R");
      this.InventoryLists_mc._x = this.InventoryLists_mc._x + 20;
      var _loc3_ = Stage.visibleRect.x + Stage.safeRect.x;
      this.ItemCard_mc._parent._x = (_loc3_ + this.InventoryLists_mc._x - this.InventoryLists_mc._width) / 2 - this.ItemCard_mc._parent._width / 2 + 25;
      (MovieClip)this.ExitMenuRect.Lock("TR");
      this.ExitMenuRect._y = this.ExitMenuRect._y - Stage.safeRect.y;
      this.RestoreCategoryRect._x = this.ExitMenuRect._x - this.InventoryLists_mc.__get__CategoriesList()._parent._width;
      (MovieClip)this.ItemsListInputCatcher.Lock("L");
      this.ItemsListInputCatcher._x = this.ItemsListInputCatcher._x - Stage.safeRect.x;
      this.ItemsListInputCatcher._width = this.RestoreCategoryRect._x - Stage.visibleRect.x + 10;
   }
   function handleInput(details, pathToFocus)
   {
      if(this.bFadedIn && !pathToFocus[0].handleInput(details,pathToFocus.slice(1)))
      {
         if(Shared.GlobalFunc.IsKeyPressed(details))
         {
            if(this.InventoryLists_mc.__get__currentState() == InventoryLists.ONE_PANEL && details.navEquivalent == gfx.ui.NavigationCode.RIGHT)
            {
               this.StartMenuFade();
               gfx.io.GameDelegate.call("ShowTweenMenu",[]);
            }
            else if(details.navEquivalent == gfx.ui.NavigationCode.TAB)
            {
               this.StartMenuFade();
               gfx.io.GameDelegate.call("CloseTweenMenu",[]);
            }
         }
      }
      return true;
   }
   function onExitMenuRectClick()
   {
      this.StartMenuFade();
      gfx.io.GameDelegate.call("ShowTweenMenu",[]);
   }
   function StartMenuFade()
   {
      this.InventoryLists_mc.HideCategoriesList();
      this.ToggleMenuFade();
      this.SaveIndices();
      this.bMenuClosing = true;
   }
   function onFadeCompletion()
   {
      if(this.bMenuClosing)
      {
         gfx.io.GameDelegate.call("CloseMenu",[]);
      }
   }
   function onShowItemsList(event)
   {
      super.onShowItemsList(event);
      if(event.index != -1)
      {
         this.UpdateButtonText();
      }
   }
   function onItemHighlightChange(event)
   {
      super.onItemHighlightChange(event);
      if(event.index != -1)
      {
         this.UpdateButtonText();
      }
   }
   function DragonSoulSpent()
   {
      this.ItemCard_mc.__get__itemInfo().soulSpent = true;
      this.UpdateButtonText();
   }
   function __get__hideButtonFlag()
   {
      return this.iHideButtonFlag;
   }
   function __set__hideButtonFlag(aiHideFlag)
   {
      this.iHideButtonFlag = aiHideFlag;
      return this.__get__hideButtonFlag();
   }
   function UpdateButtonText()
   {
      if(this.InventoryLists_mc.__get__ItemsList().__get__selectedEntry() != undefined)
      {
         var _loc3_ = (this.InventoryLists_mc.__get__ItemsList().__get__selectedEntry().filterFlag & this.InventoryLists_mc.__get__CategoriesList().__get__entryList()[0].flag) == 0?"$Favorite":"$Unfavorite";
         var _loc2_ = this.ItemCard_mc.__get__itemInfo().showUnlocked != true?"":"$Unlock";
         if((this.InventoryLists_mc.__get__ItemsList().__get__selectedEntry().filterFlag & this.iHideButtonFlag) != 0)
         {
            this.BottomBar_mc.HideButtons();
         }
         else
         {
            this.BottomBar_mc.SetButtonsText("$Equip",_loc3_,_loc2_);
         }
      }
   }
   function onHideItemsList(event)
   {
      super.onHideItemsList(event);
      this.BottomBar_mc.UpdatePerItemInfo({type:InventoryDefines.ICT_SPELL_DEFAULT});
   }
   function AttemptEquip(aiSlot)
   {
      if(this.ShouldProcessItemsListInput(true))
      {
         gfx.io.GameDelegate.call("ItemSelect",[aiSlot]);
      }
   }
   function onItemSelect(event)
   {
      if(event.entry.enabled)
      {
         if(event.keyboardOrMouse != 0)
         {
            gfx.io.GameDelegate.call("ItemSelect",[]);
         }
      }
      else
      {
         gfx.io.GameDelegate.call("ShowShoutFail",[]);
      }
   }
}
