class StartMenu extends MovieClip
{
   static var PRESS_START_STATE = "PressStart";
   static var MAIN_STATE = "Main";
   static var MAIN_CONFIRM_STATE = "MainConfirm";
   static var CHARACTER_LOAD_STATE = "CharacterLoad";
   static var CHARACTER_SELECTION_STATE = "CharacterSelection";
   static var SAVE_LOAD_STATE = "SaveLoad";
   static var SAVE_LOAD_CONFIRM_STATE = "SaveLoadConfirm";
   static var DELETE_SAVE_CONFIRM_STATE = "DeleteSaveConfirm";
   static var DLC_STATE = "DLC";
   static var MARKETPLACE_CONFIRM_STATE = "MarketplaceConfirm";
   static var START_ANIM_STR = "StartAnim";
   static var END_ANIM_STR = "EndAnim";
   static var CONTINUE_INDEX = 0;
   static var NEW_INDEX = 1;
   static var LOAD_INDEX = 2;
   static var DLC_INDEX = 3;
   static var MOD_INDEX = 4;
   static var CREDITS_INDEX = 5;
   static var QUIT_INDEX = 6;
   static var HELP_INDEX = 7;
   static var LOADING_ICON_OFFSET = 50;
   static var PLATFORM_PC_KBMOUSE = 0;
   static var PLATFORM_PC_GAMEPAD = 1;
   static var PLATFORM_DURANGO = 2;
   static var PLATFORM_ORBIS = 3;
   static var ALPHA_AVAILABLE = 100;
   static var ALPHA_DISABLED = 50;
   function StartMenu()
   {
      super();
      this.hasContinueButton = false;
      this.MainList = this.MainListHolder.List_mc;
      this.SaveLoadListHolder = this.SaveLoadPanel_mc;
      this.DLCList_mc = this.DLCPanel.DLCList;
      this.DeleteSaveButton = this.DeleteButton;
      this.ChangeUserButton = this.ChangeUserButton;
      this.MarketplaceButton = this.DLCPanel.MarketplaceButton;
      this.MarketplaceButton._visible = false;
      this.CharacterSelectionHint = this.SaveLoadListHolder.CharacterSelectionHint_mc;
      this.ShowCharacterSelectionHint(false);
   }
   function InitExtensions()
   {
      Shared.GlobalFunc.SetLockFunction();
      this._parent.Lock("BR");
      this.Logo_mc.Lock("BL");
      this.Logo_mc._y = this.Logo_mc._y - 80;
      this.GamerTagWidget_mc.Lock("TL");
      this.GamerTag_mc = this.GamerTagWidget_mc.GamerTag_mc;
      this.GamerIcon_mc = this.GamerTagWidget_mc.GamerIcon_mc;
      this.GamerIconSize = this.GamerIcon_mc._width;
      this.GamerIconLoader = new MovieClipLoader();
      this.GamerIconLoader.addListener(this);
      gfx.io.GameDelegate.addCallBack("sendMenuProperties",this,"setupMainMenu");
      gfx.io.GameDelegate.addCallBack("ConfirmNewGame",this,"ShowConfirmScreen");
      gfx.io.GameDelegate.addCallBack("ConfirmContinue",this,"ShowConfirmScreen");
      gfx.io.GameDelegate.addCallBack("FadeOutMenu",this,"DoFadeOutMenu");
      gfx.io.GameDelegate.addCallBack("FadeInMenu",this,"DoFadeInMenu");
      gfx.io.GameDelegate.addCallBack("onProfileChange",this,"onProfileChange");
      gfx.io.GameDelegate.addCallBack("StartLoadingDLC",this,"StartLoadingDLC");
      gfx.io.GameDelegate.addCallBack("DoneLoadingDLC",this,"DoneLoadingDLC");
      gfx.io.GameDelegate.addCallBack("ShowGamerTagAndIcon",this,"ShowGamerTagAndIcon");
      gfx.io.GameDelegate.addCallBack("OnDeleteSaveUISanityCheck",this,"OnDeleteSaveUISanityCheck");
      gfx.io.GameDelegate.addCallBack("OnSaveDataEventLoadSUCCESS",this,"OnSaveDataEventLoadSUCCESS");
      gfx.io.GameDelegate.addCallBack("OnSaveDataEventLoadCANCEL",this,"OnSaveDataEventLoadCANCEL");
      gfx.io.GameDelegate.addCallBack("onStartButtonProcessFinished",this,"onStartButtonProcessFinished");
      this.MainList.addEventListener("itemPress",this,"onMainButtonPress");
      this.MainList.addEventListener("listPress",this,"onMainListPress");
      this.MainList.addEventListener("listMovedUp",this,"onMainListMoveUp");
      this.MainList.addEventListener("listMovedDown",this,"onMainListMoveDown");
      this.MainList.addEventListener("selectionChange",this,"onMainListMouseSelectionChange");
      this.ButtonRect.handleInput = function()
      {
         return false;
      };
      this.ButtonRect.AcceptMouseButton.addEventListener("click",this,"onAcceptMousePress");
      this.ButtonRect.CancelMouseButton.addEventListener("click",this,"onCancelMousePress");
      this.ButtonRect.AcceptMouseButton.SetPlatform(0,false);
      this.ButtonRect.CancelMouseButton.SetPlatform(0,false);
      this.SaveLoadListHolder.addEventListener("loadGameSelected",this,"ConfirmLoadGame");
      this.SaveLoadListHolder.addEventListener("saveListPopulated",this,"OnSaveListOpenSuccess");
      this.SaveLoadListHolder.addEventListener("saveListCharactersPopulated",this,"OnsaveListCharactersOpenSuccess");
      this.SaveLoadListHolder.addEventListener("saveListOnBatchAdded",this,"OnSaveListBatchAdded");
      this.SaveLoadListHolder.addEventListener("OnCharacterSelected",this,"OnCharacterSelected");
      this.SaveLoadListHolder.addEventListener("saveHighlighted",this,"onSaveHighlight");
      this.SaveLoadListHolder.addEventListener("OnSaveLoadPanelBackClicked",Shared.Proxy.create(this,this.OnSaveLoadPanelBackClicked));
      this.SaveLoadListHolder.List_mc.addEventListener("listPress",this,"onSaveLoadListPress");
      this.DeleteSaveButton._alpha = StartMenu.ALPHA_AVAILABLE;
      this.DeleteMouseButton._alpha = StartMenu.ALPHA_AVAILABLE;
      this.MarketplaceButton._alpha = StartMenu.ALPHA_DISABLED;
      this.DeleteSaveButton._x = - this.DeleteSaveButton.textField.textWidth - StartMenu.LOADING_ICON_OFFSET;
      this.DeleteMouseButton._x = this.DeleteSaveButton._x;
      this.ChangeUserButton._x = - this.ChangeUserButton.textField.textWidth - StartMenu.LOADING_ICON_OFFSET;
      this.DLCList_mc._visible = false;
      this.CharacterSelectionHint.addEventListener("OnMousePressCharacterChange",Shared.Proxy.create(this,this.OnMousePressCharacterChange));
   }
   function setupMainMenu()
   {
      var _loc8_ = 0;
      var _loc5_ = 1;
      var _loc6_ = 2;
      var _loc10_ = 3;
      var _loc11_ = 4;
      var _loc9_ = 5;
      var _loc7_ = 6;
      var _loc4_ = StartMenu.NEW_INDEX;
      if(this.MainList.__get__entryList().length > 0)
      {
         _loc4_ = this.MainList.__get__centeredEntry().index;
      }
      this.MainList.ClearList();
      if(arguments[_loc5_])
      {
         this.hasContinueButton = true;
         this.MainList.__get__entryList().push({text:"$CONTINUE",index:StartMenu.CONTINUE_INDEX,disabled:false});
         if(_loc4_ == StartMenu.NEW_INDEX)
         {
            _loc4_ = StartMenu.CONTINUE_INDEX;
         }
      }
      this.MainList.__get__entryList().push({text:"$NEW",index:StartMenu.NEW_INDEX,disabled:false});
      this.MainList.__get__entryList().push({text:"$LOAD",disabled:!arguments[_loc5_],index:StartMenu.LOAD_INDEX});
      if(arguments[_loc11_] == true)
      {
         this.MainList.__get__entryList().push({text:"$DOWNLOADABLE CONTENT",index:StartMenu.DLC_INDEX,disabled:false});
      }
      if(arguments[_loc7_])
      {
         this.MainList.__get__entryList().push({text:"$MOD MANAGER",disabled:false,index:StartMenu.MOD_INDEX});
      }
      this.MainList.__get__entryList().push({text:"$CREDITS",index:StartMenu.CREDITS_INDEX,disabled:false});
      if(arguments[_loc8_])
      {
         this.MainList.__get__entryList().push({text:"$QUIT",index:StartMenu.QUIT_INDEX,disabled:false});
      }
      if(arguments[_loc9_])
      {
         this.MainList.__get__entryList().push({text:"$HELP",index:StartMenu.HELP_INDEX,disabled:false});
      }
      var _loc3_ = 0;
      while(_loc3_ < this.MainList.__get__entryList().length)
      {
         if(this.MainList.__get__entryList()[_loc3_].index == _loc4_)
         {
            this.MainList.RestoreScrollPosition(_loc3_,false);
         }
         _loc3_ = _loc3_ + 1;
      }
      this.MainList.InvalidateData();
      if(this.__get__currentState() == undefined)
      {
         if(arguments[_loc10_])
         {
            this.StartState(StartMenu.PRESS_START_STATE);
         }
         else
         {
            this.StartState(StartMenu.MAIN_STATE);
         }
      }
      else if(this.__get__currentState() == StartMenu.SAVE_LOAD_STATE || this.__get__currentState() == StartMenu.SAVE_LOAD_CONFIRM_STATE || this.__get__currentState() == StartMenu.DELETE_SAVE_CONFIRM_STATE)
      {
         this.StartState(StartMenu.MAIN_STATE);
      }
      if(arguments[_loc6_] != undefined)
      {
         this.VersionText.SetText("v " + arguments[_loc6_]);
      }
      else
      {
         this.VersionText.SetText(" ");
      }
   }
   function ShowGamerTagAndIcon(strGamerTag)
   {
      if(strGamerTag.length > 0)
      {
         Shared.GlobalFunc.MaintainTextFormat();
         this.GamerTag_mc.GamerTagText_tf.text = strGamerTag;
         this.GamerTag_mc.visible = true;
         this.GamerIconRect = this.GamerIcon_mc.createEmptyMovieClip("GamerIconRect",this.getNextHighestDepth());
         this.GamerIconLoader.loadClip("img://BGSUserIcon",this.GamerIconRect);
      }
      else
      {
         this.GamerTag_mc.visible = false;
         this.GamerIcon_mc.visible = false;
      }
   }
   function onLoadInit(aTargetClip)
   {
      aTargetClip._width = this.GamerIconSize;
      aTargetClip._height = this.GamerIconSize;
   }
   function OnDeleteSaveUISanityCheck(aHasRecentSave, aCanLoadGame)
   {
      var _loc7_ = false;
      if(this.hasContinueButton)
      {
         if(!aHasRecentSave)
         {
            if(this.MainList.__get__entryList()[0].index == StartMenu.CONTINUE_INDEX)
            {
               this.MainList.__get__entryList().shift();
            }
            this.MainList.RestoreScrollPosition(1,true);
            _loc7_ = true;
         }
      }
      if(!aCanLoadGame)
      {
         var _loc2_ = 0;
         while(_loc2_ < this.MainList.__get__maxEntries())
         {
            if(this.MainList.__get__entryList()[_loc2_].index == StartMenu.LOAD_INDEX)
            {
               this.MainList.__get__entryList().splice(_loc2_,1,{text:"$LOAD",disabled:true,index:StartMenu.LOAD_INDEX,textColor:6316128});
               _loc7_ = true;
               this.MainList.RestoreScrollPosition(0,false);
               break;
            }
            _loc2_ = _loc2_ + 1;
         }
      }
      if(_loc7_)
      {
         this.MainList.InvalidateData();
      }
   }
   function ShowCharacterSelectionHint(abFlag)
   {
      if(this.iPlatform == StartMenu.PLATFORM_ORBIS)
      {
         this.CharacterSelectionHint._visible = false;
      }
      else
      {
         this.CharacterSelectionHint._visible = abFlag;
      }
   }
   function OnSaveDataEventLoadSUCCESS()
   {
      this.ShowCharacterSelectionHint(false);
      if(this.iPlatform == StartMenu.PLATFORM_ORBIS)
      {
         this.onCancelPress();
      }
   }
   function OnSaveDataEventLoadCANCEL()
   {
      if(this.iPlatform == StartMenu.PLATFORM_ORBIS)
      {
         this.RequestCharacterListLoad();
      }
   }
   function __get__currentState()
   {
      return this.strCurrentState;
   }
   function __set__currentState(strNewState)
   {
      if(strNewState == StartMenu.MAIN_STATE)
      {
         this.MainList.__set__disableSelection(false);
      }
      if(strNewState != this.strCurrentState)
      {
         this.__set__ShouldProcessInputs(true);
      }
      if(this.iPlatform == StartMenu.PLATFORM_ORBIS)
      {
         this.ShowDeleteButtonHelp(strNewState == StartMenu.CHARACTER_SELECTION_STATE);
      }
      else
      {
         this.ShowDeleteButtonHelp(strNewState == StartMenu.SAVE_LOAD_STATE);
      }
      this.ShowChangeUserButtonHelp(strNewState == StartMenu.MAIN_STATE);
      this.ShowCharacterSelectionHint(strNewState == StartMenu.SAVE_LOAD_STATE);
      this.SaveLoadListHolder.ShowSelectionButtons(strNewState == StartMenu.SAVE_LOAD_STATE || strNewState == StartMenu.CHARACTER_SELECTION_STATE);
      this.strCurrentState = strNewState;
      this.ChangeStateFocus(strNewState);
      return this.__get__currentState();
   }
   function __get__ShouldProcessInputs()
   {
      return this.shouldProcessInputs;
   }
   function __set__ShouldProcessInputs(abFlag)
   {
      this.shouldProcessInputs = abFlag;
      return this.__get__ShouldProcessInputs();
   }
   function handleInput(details, pathToFocus)
   {
      if(this.__get__currentState() == StartMenu.PRESS_START_STATE && this.iPlatform == StartMenu.PLATFORM_ORBIS)
      {
         if(Shared.GlobalFunc.IsKeyPressed(details))
         {
            gfx.io.GameDelegate.call("EndPressStartState",[]);
         }
      }
      else if(pathToFocus.length > 0 && !pathToFocus[0].handleInput(details,pathToFocus.slice(1)))
      {
         if(Shared.GlobalFunc.IsKeyPressed(details) && this.__get__ShouldProcessInputs())
         {
            if(details.navEquivalent == gfx.ui.NavigationCode.ENTER)
            {
               this.onAcceptPress();
            }
            else if(details.navEquivalent == gfx.ui.NavigationCode.TAB)
            {
               this.onCancelPress();
            }
            else if((details.navEquivalent == gfx.ui.NavigationCode.GAMEPAD_X || details.code == 88) && this.DeleteSaveButton._visible && this.DeleteSaveButton._alpha == StartMenu.ALPHA_AVAILABLE)
            {
               if(this.iPlatform == StartMenu.PLATFORM_ORBIS)
               {
                  var _loc5_ = this.SaveLoadListHolder.__get__selectedEntry();
                  if(_loc5_ != undefined)
                  {
                     var _loc4_ = _loc5_.flags;
                     if(_loc4_ == undefined)
                     {
                        _loc4_ = 0;
                     }
                     var _loc3_ = _loc5_.id;
                     if(_loc3_ == undefined)
                     {
                        _loc3_ = 4294967295;
                     }
                  }
                  gfx.io.GameDelegate.call("ORBISDeleteSave",[_loc3_,_loc4_]);
               }
               else
               {
                  this.ConfirmDeleteSave();
               }
            }
            else if((details.navEquivalent == gfx.ui.NavigationCode.GAMEPAD_Y || details.code == 84) && this.strCurrentState == StartMenu.SAVE_LOAD_STATE && !this.SaveLoadListHolder.__get__isSaving())
            {
               gfx.io.GameDelegate.call("PlaySound",["UIMenuCancel"]);
               this.EndState();
            }
            else if(details.navEquivalent == gfx.ui.NavigationCode.GAMEPAD_Y && this.__get__currentState() == StartMenu.DLC_STATE && this.MarketplaceButton._visible && this.MarketplaceButton._alpha == StartMenu.ALPHA_AVAILABLE)
            {
               this.SaveLoadConfirmText.textField.SetText("$Open Xbox LIVE Marketplace?");
               this.SetPlatform(this.iPlatform);
               this.StartState(StartMenu.MARKETPLACE_CONFIRM_STATE);
            }
            else if(details.navEquivalent == gfx.ui.NavigationCode.GAMEPAD_Y && this.__get__currentState() == StartMenu.MAIN_STATE && this.ChangeUserButton._visible)
            {
               gfx.io.GameDelegate.call("ChangeUser",[]);
            }
         }
      }
      return true;
   }
   function onMouseButtonDeleteSaveClick()
   {
      if(this.DeleteSaveButton._alpha == StartMenu.ALPHA_AVAILABLE)
      {
         this.ConfirmDeleteSave();
      }
   }
   function onMouseButtonDeleteRollOver()
   {
      gfx.io.GameDelegate.call("PlaySound",["UIMenuFocus"]);
   }
   function onStartButtonProcessFinished()
   {
      this.EndState(StartMenu.PRESS_START_STATE);
   }
   function onAcceptPress()
   {
      switch(this.strCurrentState)
      {
         case StartMenu.MAIN_CONFIRM_STATE:
            if(this.MainList.__get__selectedEntry().index == StartMenu.NEW_INDEX)
            {
               gfx.io.GameDelegate.call("PlaySound",["UIStartNewGame"]);
               this.FadeOutAndCall("StartNewGame");
            }
            else if(this.MainList.__get__selectedEntry().index == StartMenu.CONTINUE_INDEX)
            {
               gfx.io.GameDelegate.call("PlaySound",["UIMenuOK"]);
               this.FadeOutAndCall("ContinueLastSavedGame");
            }
            else if(this.MainList.__get__selectedEntry().index == StartMenu.QUIT_INDEX)
            {
               gfx.io.GameDelegate.call("PlaySound",["UIMenuOK"]);
               gfx.io.GameDelegate.call("QuitToDesktop",[]);
            }
            break;
         case StartMenu.CHARACTER_SELECTION_STATE:
            gfx.io.GameDelegate.call("PlaySound",["UIMenuOK"]);
            break;
         case StartMenu.SAVE_LOAD_CONFIRM_STATE:
            gfx.io.GameDelegate.call("PlaySound",["UIMenuOK"]);
            this.FadeOutAndCall("LoadGame",[this.SaveLoadListHolder.__get__selectedIndex()]);
            break;
         case StartMenu.DELETE_SAVE_CONFIRM_STATE:
            this.SaveLoadListHolder.DeleteSelectedSave();
            if(this.SaveLoadListHolder.__get__numSaves() == 0)
            {
               gfx.io.GameDelegate.call("DoDeleteSaveUISanityCheck",[]);
               this.StartState(StartMenu.MAIN_STATE);
            }
            else
            {
               this.EndState();
            }
            break;
         case StartMenu.MARKETPLACE_CONFIRM_STATE:
            gfx.io.GameDelegate.call("PlaySound",["UIMenuOK"]);
            gfx.io.GameDelegate.call("OpenMarketplace",[]);
            this.StartState(StartMenu.MAIN_STATE);
      }
   }
   function isConfirming()
   {
      return this.strCurrentState == StartMenu.SAVE_LOAD_CONFIRM_STATE || this.strCurrentState == StartMenu.DELETE_SAVE_CONFIRM_STATE || this.strCurrentState == StartMenu.MARKETPLACE_CONFIRM_STATE || this.strCurrentState == StartMenu.MAIN_CONFIRM_STATE;
   }
   function onAcceptMousePress()
   {
      if(this.isConfirming())
      {
         this.onAcceptPress();
      }
   }
   function OnMousePressCharacterChange(evt)
   {
      gfx.io.GameDelegate.call("PlaySound",["UIMenuCancel"]);
      this.EndState();
   }
   function onCancelMousePress()
   {
      if(this.isConfirming())
      {
         this.onCancelPress();
      }
   }
   function onCancelPress()
   {
      switch(this.strCurrentState)
      {
         case StartMenu.SAVE_LOAD_STATE:
            this.__set__currentState(StartMenu.CHARACTER_SELECTION_STATE);
            this.EndState();
            this.SaveLoadListHolder.ForceStopLoading();
            break;
         case StartMenu.CHARACTER_SELECTION_STATE:
         case StartMenu.MAIN_CONFIRM_STATE:
         case StartMenu.SAVE_LOAD_CONFIRM_STATE:
         case StartMenu.DELETE_SAVE_CONFIRM_STATE:
         case StartMenu.DLC_STATE:
         case StartMenu.MARKETPLACE_CONFIRM_STATE:
            gfx.io.GameDelegate.call("PlaySound",["UIMenuCancel"]);
            this.EndState();
      }
   }
   function onMainButtonPress(event)
   {
      if(this.strCurrentState == StartMenu.MAIN_STATE || this.iPlatform == 0)
      {
         switch(event.entry.index)
         {
            case StartMenu.CONTINUE_INDEX:
               gfx.io.GameDelegate.call("CONTINUE",[]);
               gfx.io.GameDelegate.call("PlaySound",["UIMenuOK"]);
               break;
            case StartMenu.NEW_INDEX:
               gfx.io.GameDelegate.call("NEW",[]);
               gfx.io.GameDelegate.call("PlaySound",["UIMenuOK"]);
               break;
            case StartMenu.QUIT_INDEX:
               this.ShowConfirmScreen("$Quit to desktop?  Any unsaved progress will be lost.");
               gfx.io.GameDelegate.call("PlaySound",["UIMenuOK"]);
               break;
            case StartMenu.LOAD_INDEX:
               if(!event.entry.disabled)
               {
                  this.SaveLoadListHolder.__set__isSaving(false);
                  this.RequestCharacterListLoad();
               }
               else
               {
                  gfx.io.GameDelegate.call("OnDisabledLoadPress",[]);
               }
               break;
            case StartMenu.DLC_INDEX:
               this.StartState(StartMenu.DLC_STATE);
               break;
            case StartMenu.CREDITS_INDEX:
               this.FadeOutAndCall("OpenCreditsMenu");
               break;
            case StartMenu.HELP_INDEX:
               gfx.io.GameDelegate.call("HELP",[]);
               gfx.io.GameDelegate.call("PlaySound",["UIMenuOK"]);
               break;
            case StartMenu.MOD_INDEX:
               gfx.io.GameDelegate.call("MOD",[]);
               gfx.io.GameDelegate.call("PlaySound",["UIMenuOK"]);
               break;
            default:
               gfx.io.GameDelegate.call("PlaySound",["UIMenuCancel"]);
         }
      }
   }
   function RequestCharacterListLoad()
   {
      gfx.io.GameDelegate.call("PopulateCharacterList",[this.SaveLoadListHolder.List_mc.entryList,this.SaveLoadListHolder.__get__batchSize()]);
      this.StartState(StartMenu.CHARACTER_LOAD_STATE);
   }
   function onMainListPress(event)
   {
      this.onCancelPress();
   }
   function onPCQuitButtonPress(event)
   {
      if(event.index == 0)
      {
         gfx.io.GameDelegate.call("QuitToMainMenu",[]);
      }
      else if(event.index == 1)
      {
         gfx.io.GameDelegate.call("QuitToDesktop",[]);
      }
   }
   function onSaveLoadListPress()
   {
      this.onAcceptPress();
   }
   function onMainListMoveUp(event)
   {
      gfx.io.GameDelegate.call("PlaySound",["UIMenuFocus"]);
      if(event.scrollChanged == true)
      {
         this.MainList._parent.gotoAndPlay("moveUp");
      }
   }
   function onMainListMoveDown(event)
   {
      gfx.io.GameDelegate.call("PlaySound",["UIMenuFocus"]);
      if(event.scrollChanged == true)
      {
         this.MainList._parent.gotoAndPlay("moveDown");
      }
   }
   function onMainListMouseSelectionChange(event)
   {
      if(event.keyboardOrMouse == 0 && event.index != -1)
      {
         gfx.io.GameDelegate.call("PlaySound",["UIMenuFocus"]);
      }
   }
   function SetPlatform(aiPlatform, abPS3Switch)
   {
      this.ButtonRect.AcceptGamepadButton._visible = aiPlatform != 0;
      this.ButtonRect.CancelGamepadButton._visible = aiPlatform != 0;
      this.ButtonRect.AcceptMouseButton._visible = aiPlatform == 0;
      this.ButtonRect.CancelMouseButton._visible = aiPlatform == 0;
      var _loc4_ = this.DeleteSaveButton._visible;
      if(aiPlatform == StartMenu.PLATFORM_PC_KBMOUSE)
      {
         this.DeleteSaveButton._visible = false;
         this.DeleteMouseButton.label = this.DeleteSaveButton.label;
         this.DeleteMouseButton._x = this.DeleteButton._x;
         this.DeleteMouseButton.trackAsMenu = true;
         this.DeleteSaveButton = this.DeleteMouseButton;
         this.DeleteSaveButton.onPress = Shared.Proxy.create(this,this.onMouseButtonDeleteSaveClick);
         this.DeleteSaveButton.addEventListener("rollOver",Shared.Proxy.create(this,this.onMouseButtonDeleteRollOver));
      }
      else if(aiPlatform == StartMenu.PLATFORM_PC_GAMEPAD && this.DeleteSaveButton == this.DeleteMouseButton)
      {
         this.DeleteSaveButton._visible = false;
         this.DeleteSaveButton = this.DeleteButton;
         this.DeleteSaveButton.onPress = undefined;
         this.DeleteMouseButton.removeEventListeners("rollOver",Shared.Proxy.create(this,this.onMouseButtonDeleteRollOver));
      }
      else
      {
         this.DeleteMouseButton._visible = false;
      }
      this.ShowDeleteButtonHelp(_loc4_);
      this.DeleteSaveButton.SetPlatform(aiPlatform,abPS3Switch);
      this.ChangeUserButton.SetPlatform(aiPlatform,abPS3Switch);
      this.MarketplaceButton.SetPlatform(aiPlatform,abPS3Switch);
      this.MainListHolder.SelectionArrow._visible = aiPlatform != 0;
      if(aiPlatform != 0)
      {
         this.ButtonRect.AcceptGamepadButton.SetPlatform(aiPlatform,abPS3Switch);
         this.ButtonRect.CancelGamepadButton.SetPlatform(aiPlatform,abPS3Switch);
      }
      this.CharacterSelectionHint.SetPlatform(aiPlatform);
      this.MarketplaceButton._visible = false;
      if(this.iPlatform == undefined)
      {
         this.DLCPanel.warningText.SetText("$Loading downloadable content..." + (this.iPlatform != StartMenu.PLATFORM_ORBIS?"":"_PS3"));
         this.LoadingContentMessage.Message_mc.textField.SetText("$Loading extra content." + (this.iPlatform != StartMenu.PLATFORM_ORBIS?"":"_PS3"));
      }
      this.iPlatform = aiPlatform;
      this.SaveLoadListHolder.__set__platform(this.iPlatform);
      this.MainList.SetPlatform(aiPlatform,abPS3Switch);
   }
   function DoFadeOutMenu()
   {
      this.FadeOutAndCall();
   }
   function DoFadeInMenu()
   {
      this._parent.gotoAndPlay("fadeIn");
      this.EndState();
   }
   function FadeOutAndCall(strCallback, paramList)
   {
      this.strFadeOutCallback = strCallback;
      this.fadeOutParams = paramList;
      this._parent.gotoAndPlay("fadeOut");
      gfx.io.GameDelegate.call("fadeOutStarted",[]);
   }
   function onFadeOutCompletion()
   {
      if(this.strFadeOutCallback != undefined && this.strFadeOutCallback.length > 0)
      {
         if(this.fadeOutParams != undefined)
         {
            gfx.io.GameDelegate.call(this.strFadeOutCallback,this.fadeOutParams);
         }
         else
         {
            gfx.io.GameDelegate.call(this.strFadeOutCallback,[]);
         }
      }
   }
   function StartState(strStateName)
   {
      this.__set__ShouldProcessInputs(false);
      if(strStateName == StartMenu.CHARACTER_SELECTION_STATE)
      {
         this.SaveLoadListHolder.__set__isShowingCharacterList(true);
      }
      else if(strStateName == StartMenu.SAVE_LOAD_STATE)
      {
         this.SaveLoadListHolder.__set__isShowingCharacterList(false);
      }
      else if(strStateName == StartMenu.DLC_STATE)
      {
         this.ShowMarketplaceButtonHelp(false);
      }
      if(this.strCurrentState == StartMenu.MAIN_STATE)
      {
         this.MainList.__set__disableSelection(true);
      }
      this.ShowDeleteButtonHelp(false);
      this.ShowChangeUserButtonHelp(false);
      this.SaveLoadListHolder.ShowSelectionButtons(false);
      this.strCurrentState = strStateName + StartMenu.START_ANIM_STR;
      this.gotoAndPlay(this.strCurrentState);
      gfx.managers.FocusHandler.__get__instance().setFocus(this,0);
   }
   function EndState()
   {
      if(this.strCurrentState == StartMenu.DLC_STATE)
      {
         this.ShowMarketplaceButtonHelp(false);
      }
      if(this.strCurrentState != StartMenu.MAIN_STATE)
      {
         this.strCurrentState = this.strCurrentState + StartMenu.END_ANIM_STR;
         this.gotoAndPlay(this.strCurrentState);
      }
      if(this.strCurrentState == StartMenu.SAVE_LOAD_CONFIRM_STATE || this.strCurrentState == StartMenu.DELETE_SAVE_CONFIRM_STATE)
      {
         this.SaveLoadListHolder.ShowSelectionButtons(true);
      }
   }
   function ChangeStateFocus(strNewState)
   {
      switch(strNewState)
      {
         case StartMenu.MAIN_STATE:
            gfx.managers.FocusHandler.__get__instance().setFocus(this.MainList,0);
            break;
         case StartMenu.CHARACTER_SELECTION_STATE:
         case StartMenu.SAVE_LOAD_STATE:
            gfx.managers.FocusHandler.__get__instance().setFocus(this.SaveLoadListHolder.List_mc,0);
            this.SaveLoadListHolder.List_mc.disableSelection = false;
            break;
         case StartMenu.DLC_STATE:
            this.iLoadDLCListTimerID = setInterval(this,"DoLoadDLCList",500);
            gfx.managers.FocusHandler.__get__instance().setFocus(this.DLCList_mc,0);
            break;
         case StartMenu.MAIN_CONFIRM_STATE:
         case StartMenu.SAVE_LOAD_CONFIRM_STATE:
         case StartMenu.DELETE_SAVE_CONFIRM_STATE:
         case StartMenu.PRESS_START_STATE:
         case StartMenu.MARKETPLACE_CONFIRM_STATE:
            gfx.managers.FocusHandler.__get__instance().setFocus(this.ButtonRect,0);
      }
   }
   function ShowConfirmScreen(astrConfirmText)
   {
      this.ConfirmPanel_mc.textField.SetText(astrConfirmText);
      this.SetPlatform(this.iPlatform);
      this.StartState(StartMenu.MAIN_CONFIRM_STATE);
   }
   function OnSaveListOpenSuccess()
   {
      if(this.SaveLoadListHolder.__get__numSaves() > 0 && this.strCurrentState.indexOf(StartMenu.SAVE_LOAD_STATE) == -1)
      {
         gfx.io.GameDelegate.call("PlaySound",["UIMenuOK"]);
         this.StartState(StartMenu.SAVE_LOAD_STATE);
      }
      else
      {
         gfx.io.GameDelegate.call("PlaySound",["UIMenuCancel"]);
      }
   }
   function OnsaveListCharactersOpenSuccess()
   {
      if(this.strCurrentState == StartMenu.CHARACTER_LOAD_STATE || this.strCurrentState == "CharacterLoadStartAnim")
      {
         this.SaveLoadListHolder.__set__isShowingCharacterList(true);
         this.ShowCharacterSelectionHint(false);
         gfx.io.GameDelegate.call("PlaySound",["UIMenuOK"]);
         this.StartState(StartMenu.CHARACTER_SELECTION_STATE);
      }
      else
      {
         gfx.io.GameDelegate.call("PlaySound",["UIMenuCancel"]);
      }
   }
   function OnSaveListBatchAdded()
   {
      if(this.SaveLoadListHolder.__get__numSaves() > 0 && this.strCurrentState == StartMenu.SAVE_LOAD_STATE)
      {
         this.ShowCharacterSelectionHint(true);
      }
   }
   function OnCharacterSelected()
   {
      if(this.iPlatform != StartMenu.PLATFORM_ORBIS)
      {
         this.StartState(StartMenu.SAVE_LOAD_STATE);
      }
   }
   function onSaveHighlight(event)
   {
      this.DeleteSaveButton._alpha = event.index != -1?StartMenu.ALPHA_AVAILABLE:StartMenu.ALPHA_DISABLED;
      if(this.iPlatform == 0)
      {
         gfx.io.GameDelegate.call("PlaySound",["UIMenuFocus"]);
      }
   }
   function ConfirmLoadGame(event)
   {
      this.SaveLoadListHolder.List_mc.disableSelection = true;
      this.SaveLoadConfirmText.textField.SetText("$Load this game?");
      this.SetPlatform(this.iPlatform);
      this.StartState(StartMenu.SAVE_LOAD_CONFIRM_STATE);
   }
   function ConfirmDeleteSave()
   {
      this.SaveLoadListHolder.List_mc.disableSelection = true;
      this.SaveLoadConfirmText.textField.SetText("$Delete this save?");
      this.SetPlatform(this.iPlatform);
      this.StartState(StartMenu.DELETE_SAVE_CONFIRM_STATE);
   }
   function ShowDeleteButtonHelp(abFlag)
   {
      this.DeleteSaveButton.__set__disabled(!abFlag);
      this.DeleteSaveButton._visible = abFlag;
      this.VersionText._visible = !abFlag;
   }
   function ShowChangeUserButtonHelp(abFlag)
   {
      if(this.iPlatform == StartMenu.PLATFORM_DURANGO)
      {
         this.ChangeUserButton.__set__disabled(!abFlag);
         this.ChangeUserButton._visible = abFlag;
         this.VersionText._visible = !abFlag;
      }
      else
      {
         this.ChangeUserButton.__set__disabled(true);
         this.ChangeUserButton._visible = false;
      }
   }
   function ShowMarketplaceButtonHelp(abFlag)
   {
      if(this.iPlatform == StartMenu.PLATFORM_DURANGO)
      {
         this.MarketplaceButton._visible = abFlag;
         this.VersionText._visible = !abFlag;
      }
      else
      {
         this.MarketplaceButton._visible = false;
      }
   }
   function ShowPressStartState()
   {
      if(this.strCurrentState != StartMenu.PRESS_START_STATE)
      {
         this.StartState(StartMenu.PRESS_START_STATE);
      }
   }
   function StartLoadingDLC()
   {
      this.LoadingContentMessage.gotoAndPlay("startFadeIn");
      clearInterval(this.iLoadDLCContentMessageTimerID);
      this.iLoadDLCContentMessageTimerID = setInterval(this,"onLoadingDLCMessageFadeCompletion",1000);
   }
   function onLoadingDLCMessageFadeCompletion()
   {
      clearInterval(this.iLoadDLCContentMessageTimerID);
      gfx.io.GameDelegate.call("DoLoadDLCPlugins",[]);
   }
   function DoneLoadingDLC()
   {
      this.LoadingContentMessage.gotoAndPlay("startFadeOut");
   }
   function DoLoadDLCList()
   {
      clearInterval(this.iLoadDLCListTimerID);
      this.DLCList_mc.__get__entryList().splice(0,this.DLCList_mc.__get__entryList().length);
      gfx.io.GameDelegate.call("LoadDLC",[this.DLCList_mc.__get__entryList()],this,"UpdateDLCPanel");
   }
   function UpdateDLCPanel(abMarketplaceAvail, abNewDLCAvail)
   {
      if(this.DLCList_mc.__get__entryList().length > 0)
      {
         this.DLCList_mc._visible = true;
         this.DLCPanel.warningText.SetText(" ");
         if(this.iPlatform != 0)
         {
            this.DLCList_mc.__set__selectedIndex(0);
         }
         this.DLCList_mc.InvalidateData();
      }
      else
      {
         this.DLCList_mc._visible = false;
         this.DLCPanel.warningText.SetText("$No content downloaded" + (this.iPlatform != StartMenu.PLATFORM_ORBIS?"":"_PS3"));
      }
      this.MarketplaceButton._visible = false;
      if(abNewDLCAvail == true)
      {
         this.DLCPanel.NewContentAvail.SetText("$New content available");
      }
   }
   function OnSaveLoadPanelSelectClicked()
   {
      this.onAcceptPress();
   }
   function OnSaveLoadPanelBackClicked()
   {
      this.onCancelPress();
   }
}
