class SaveLoadPanel extends MovieClip
{
   static var SCREENSHOT_DELAY = 750;
   static var CONTROLLER_PC = 0;
   static var CONTROLLER_PC_GAMEPAD = 1;
   static var CONTROLLER_DURANGO = 2;
   static var CONTROLLER_ORBIS = 3;
   function SaveLoadPanel()
   {
      super();
      gfx.events.EventDispatcher.initialize(this);
      this.SaveLoadList_mc = this.List_mc;
      this.bSaving = true;
      this.showCharacterBackHint = false;
      this.showingCharacterList = false;
      this.lastSelectedIndexMemory = 0;
      this.uiSaveLoadManagerProcessedElements = 0;
      this.uiSaveLoadManagerNumElementsToLoad = 0;
      this.isForceStopping = false;
   }
   function onLoad()
   {
      this.ScreenshotLoader = new MovieClipLoader();
      this.ScreenshotLoader.addListener(this);
      gfx.io.GameDelegate.addCallBack("ConfirmOKToLoad",this,"onOKToLoadConfirm");
      gfx.io.GameDelegate.addCallBack("onSaveLoadBatchComplete",this,"onSaveLoadBatchComplete");
      gfx.io.GameDelegate.addCallBack("onFillCharacterListComplete",this,"onFillCharacterListComplete");
      gfx.io.GameDelegate.addCallBack("ScreenshotReady",this,"ShowScreenshot");
      this.SaveLoadList_mc.addEventListener("itemPress",this,"onSaveLoadItemPress");
      this.SaveLoadList_mc.addEventListener("selectionChange",this,"onSaveLoadItemHighlight");
      this.iBatchSize = this.SaveLoadList_mc.maxEntries;
      this.PlayerInfoText.createTextField("LevelText",this.PlayerInfoText.getNextHighestDepth(),0,0,200,30);
      this.PlayerInfoText.LevelText.text = "$Level";
      this.PlayerInfoText.LevelText._visible = false;
   }
   function __get__isSaving()
   {
      return this.bSaving;
   }
   function __set__isSaving(abFlag)
   {
      this.bSaving = abFlag;
      return this.__get__isSaving();
   }
   function __get__isShowingCharacterList()
   {
      return this.showingCharacterList;
   }
   function __set__isShowingCharacterList(abFlag)
   {
      this.showingCharacterList = abFlag;
      if(this.iPlatform != 3)
      {
         this.ScreenshotHolder._visible = !this.showingCharacterList;
         this.ScreenShotRect_mc._visible = !this.showingCharacterList;
      }
      this.PlayerInfoText._visible = !this.showingCharacterList;
      return this.__get__isShowingCharacterList();
   }
   function __get__selectedIndex()
   {
      return this.SaveLoadList_mc.__get__selectedIndex();
   }
   function __get__platform()
   {
      return this.iPlatform;
   }
   function __set__platform(aiPlatform)
   {
      this.iPlatform = aiPlatform;
      if(this.iPlatform == SaveLoadPanel.CONTROLLER_PC)
      {
         this.BackMouseButton.SetPlatform(this.iPlatform);
         this.SelectMouseButton.SetPlatform(this.iPlatform);
         this.BackMouseButton.addEventListener("click",Shared.Proxy.create(this,this.OnBackClicked));
         this.SelectMouseButton.addEventListener("click",Shared.Proxy.create(this,this.OnSelectClicked));
         var _loc2_ = this.SelectMouseButton.getBounds(this);
         this.SelectMouseButton._x = this.SelectMouseButton._x + (this.CharacterSelectionHint_mc._x - _loc2_.xMin);
      }
      else
      {
         this.BackGamepadButton.SetPlatform(this.iPlatform);
         this.SelectGamepadButton.SetPlatform(this.iPlatform);
      }
      this.BackMouseButton._visible = this.SelectMouseButton._visible = this.iPlatform == SaveLoadPanel.CONTROLLER_PC;
      this.BackGamepadButton._visible = this.SelectGamepadButton._visible = this.iPlatform != SaveLoadPanel.CONTROLLER_PC;
      if(this.iPlatform == SaveLoadPanel.CONTROLLER_ORBIS)
      {
         this.ScreenshotHolder._visible = false;
         this.ScreenShotRect_mc._visible = false;
      }
      return this.__get__platform();
   }
   function __get__batchSize()
   {
      return this.iBatchSize;
   }
   function __get__numSaves()
   {
      return this.SaveLoadList_mc.__get__length();
   }
   function __get__selectedEntry()
   {
      return this.SaveLoadList_mc.__get__entryList()[this.SaveLoadList_mc.__get__selectedIndex()];
   }
   function __get__LastSelectedIndexMemory()
   {
      if(this.lastSelectedIndexMemory > this.SaveLoadList_mc.__get__entryList().length - 1)
      {
         this.lastSelectedIndexMemory = Math.max(0,this.SaveLoadList_mc.__get__entryList().length - 1);
      }
      return this.lastSelectedIndexMemory;
   }
   function onSaveLoadItemPress(event)
   {
      this.lastSelectedIndexMemory = this.SaveLoadList_mc.selectedIndex;
      if(this.__get__isShowingCharacterList())
      {
         var _loc4_ = this.SaveLoadList_mc.__get__entryList()[this.SaveLoadList_mc.__get__selectedIndex()];
         if(_loc4_ != undefined)
         {
            if(this.iPlatform != 0)
            {
               this.SaveLoadList_mc.__set__selectedIndex(0);
            }
            var _loc3_ = _loc4_.flags;
            if(_loc3_ == undefined)
            {
               _loc3_ = 0;
            }
            var _loc2_ = _loc4_.id;
            if(_loc2_ == undefined)
            {
               _loc2_ = 4294967295;
            }
            gfx.io.GameDelegate.call("CharacterSelected",[_loc2_,_loc3_,this.bSaving,this.SaveLoadList_mc.__get__entryList(),this.iBatchSize]);
            this.dispatchEvent({type:"OnCharacterSelected"});
         }
      }
      else if(!this.bSaving)
      {
         gfx.io.GameDelegate.call("IsOKtoLoad",[this.SaveLoadList_mc.__get__selectedIndex()]);
      }
      else
      {
         this.dispatchEvent({type:"saveGameSelected",index:this.SaveLoadList_mc.__get__selectedIndex()});
      }
   }
   function ShowSelectionButtons(show)
   {
      if(this.iPlatform == SaveLoadPanel.CONTROLLER_PC)
      {
         this.SelectMouseButton._visible = this.BackMouseButton._visible = show;
      }
      else
      {
         this.SelectGamepadButton._visible = this.BackGamepadButton._visible = show;
      }
   }
   function onOKToLoadConfirm()
   {
      this.dispatchEvent({type:"loadGameSelected",index:this.SaveLoadList_mc.__get__selectedIndex()});
   }
   function ForceStopLoading()
   {
      this.isForceStopping = true;
      if(this.uiSaveLoadManagerProcessedElements < this.uiSaveLoadManagerNumElementsToLoad)
      {
         gfx.io.GameDelegate.call("ForceStopSaveListLoading",[]);
      }
   }
   function onSaveLoadItemHighlight(event)
   {
      if(this.isForceStopping)
      {
         return undefined;
      }
      if(this.iScreenshotTimerID != undefined)
      {
         clearInterval(this.iScreenshotTimerID);
         this.iScreenshotTimerID = undefined;
      }
      if(this.ScreenshotRect != undefined)
      {
         this.ScreenshotRect.removeMovieClip();
         this.PlayerInfoText.textField.SetText(" ");
         this.PlayerInfoText.DateText.SetText(" ");
         this.PlayerInfoText.PlayTimeText.SetText(" ");
         this.ScreenshotRect = undefined;
      }
      if(!this.__get__isShowingCharacterList())
      {
         if(event.index != -1)
         {
            this.iScreenshotTimerID = setInterval(this,"PrepScreenshot",SaveLoadPanel.SCREENSHOT_DELAY);
         }
         this.dispatchEvent({type:"saveHighlighted",index:this.SaveLoadList_mc.__get__selectedIndex()});
      }
   }
   function PrepScreenshot()
   {
      clearInterval(this.iScreenshotTimerID);
      this.iScreenshotTimerID = undefined;
      if(this.bSaving)
      {
         gfx.io.GameDelegate.call("PrepSaveGameScreenshot",[this.SaveLoadList_mc.__get__selectedIndex() - 1,this.SaveLoadList_mc.__get__selectedEntry()]);
      }
      else
      {
         gfx.io.GameDelegate.call("PrepSaveGameScreenshot",[this.SaveLoadList_mc.__get__selectedIndex(),this.SaveLoadList_mc.__get__selectedEntry()]);
      }
   }
   function ShowScreenshot()
   {
      this.ScreenshotRect = this.ScreenshotHolder.createEmptyMovieClip("ScreenshotRect",0);
      this.ScreenshotLoader.loadClip("img://BGSSaveLoadHeader_Screenshot",this.ScreenshotRect);
      if(this.SaveLoadList_mc.__get__selectedEntry().corrupt == true)
      {
         this.PlayerInfoText.textField.SetText("$SAVE CORRUPT");
      }
      else if(this.SaveLoadList_mc.__get__selectedEntry().obsolete == true)
      {
         this.PlayerInfoText.textField.SetText("$SAVE OBSOLETE");
      }
      else if(this.SaveLoadList_mc.__get__selectedEntry().name != undefined)
      {
         var _loc2_ = this.SaveLoadList_mc.__get__selectedEntry().name;
         var _loc3_ = 20;
         if(_loc2_.length > _loc3_)
         {
            _loc2_ = _loc2_.substr(0,_loc3_ - 3) + "...";
         }
         if(this.SaveLoadList_mc.__get__selectedEntry().raceName != undefined && this.SaveLoadList_mc.__get__selectedEntry().raceName.length > 0)
         {
            _loc2_ = _loc2_ + (", " + this.SaveLoadList_mc.__get__selectedEntry().raceName);
         }
         if(this.SaveLoadList_mc.__get__selectedEntry().level != undefined && this.SaveLoadList_mc.__get__selectedEntry().level > 0)
         {
            _loc2_ = _loc2_ + (", " + this.PlayerInfoText.LevelText.text + " " + this.SaveLoadList_mc.__get__selectedEntry().level);
         }
         this.PlayerInfoText.textField.textAutoSize = "shrink";
         this.PlayerInfoText.textField.SetText(_loc2_);
      }
      else
      {
         this.PlayerInfoText.textField.SetText(" ");
      }
      if(this.SaveLoadList_mc.__get__selectedEntry().playTime != undefined)
      {
         this.PlayerInfoText.PlayTimeText.SetText(this.SaveLoadList_mc.__get__selectedEntry().playTime);
      }
      else
      {
         this.PlayerInfoText.PlayTimeText.SetText(" ");
      }
      if(this.SaveLoadList_mc.__get__selectedEntry().dateString != undefined)
      {
         this.PlayerInfoText.DateText.SetText(this.SaveLoadList_mc.__get__selectedEntry().dateString);
      }
      else
      {
         this.PlayerInfoText.DateText.SetText(" ");
      }
   }
   function onLoadInit(aTargetClip)
   {
      aTargetClip._width = this.ScreenshotHolder.sizer._width;
      aTargetClip._height = this.ScreenshotHolder.sizer._height;
   }
   function onFillCharacterListComplete(abDoInitialUpdate)
   {
      this.__set__isShowingCharacterList(true);
      var _loc2_ = 20;
      for(var _loc3_ in this.SaveLoadList_mc.__get__entryList())
      {
         if(this.SaveLoadList_mc.__get__entryList()[_loc3_].text.length > _loc2_)
         {
            this.SaveLoadList_mc.__get__entryList()[_loc3_].text = this.SaveLoadList_mc.__get__entryList()[_loc3_].text.substr(0,_loc2_ - 3) + "...";
         }
      }
      this.SaveLoadList_mc.InvalidateData();
      if(this.iPlatform != 0)
      {
         this.onSaveLoadItemHighlight({index:this.__get__LastSelectedIndexMemory()});
         this.SaveLoadList_mc.__set__selectedIndex(this.LastSelectedIndexMemory);
         this.SaveLoadList_mc.UpdateList();
      }
      this.dispatchEvent({type:"saveListCharactersPopulated"});
   }
   function onSaveLoadBatchComplete(abDoInitialUpdate, aNumProcessed, aSaveCount)
   {
      var _loc2_ = 20;
      this.uiSaveLoadManagerProcessedElements = aNumProcessed;
      this.uiSaveLoadManagerNumElementsToLoad = aSaveCount;
      for(var _loc3_ in this.SaveLoadList_mc.__get__entryList())
      {
         if(this.iPlatform == SaveLoadPanel.CONTROLLER_ORBIS)
         {
            if(this.SaveLoadList_mc.__get__entryList()[_loc3_].text == undefined)
            {
               this.SaveLoadList_mc.__get__entryList().splice(_loc3_,1);
            }
         }
         if(this.SaveLoadList_mc.__get__entryList()[_loc3_].text.length > _loc2_)
         {
            this.SaveLoadList_mc.__get__entryList()[_loc3_].text = this.SaveLoadList_mc.__get__entryList()[_loc3_].text.substr(0,_loc2_ - 3) + "...";
         }
      }
      var _loc4_ = "$[NEW SAVE]";
      if(this.bSaving && this.SaveLoadList_mc.__get__entryList()[0].text != _loc4_)
      {
         var _loc5_ = {name:" ",playTime:" ",text:_loc4_};
         this.SaveLoadList_mc.__get__entryList().unshift(_loc5_);
      }
      else if(!this.bSaving && this.SaveLoadList_mc.__get__entryList()[0].text == _loc4_)
      {
         this.SaveLoadList_mc.__get__entryList().shift();
      }
      this.SaveLoadList_mc.InvalidateData();
      if(this.iPlatform == SaveLoadPanel.CONTROLLER_ORBIS)
      {
         this.lastSelectedIndexMemory = 0;
      }
      if(abDoInitialUpdate)
      {
         this.isForceStopping = false;
         this.__set__isShowingCharacterList(false);
         if(this.iPlatform != 0)
         {
            this.onSaveLoadItemHighlight({index:this.__get__LastSelectedIndexMemory()});
            this.SaveLoadList_mc.__set__selectedIndex(this.LastSelectedIndexMemory);
            this.SaveLoadList_mc.UpdateList();
         }
         this.dispatchEvent({type:"saveListPopulated"});
      }
      else if(!this.isForceStopping)
      {
         this.dispatchEvent({type:"saveListOnBatchAdded"});
      }
   }
   function DeleteSelectedSave()
   {
      if(!this.bSaving || this.SaveLoadList_mc.__get__selectedIndex() != 0)
      {
         if(this.bSaving)
         {
            gfx.io.GameDelegate.call("DeleteSave",[this.SaveLoadList_mc.__get__selectedIndex() - 1]);
         }
         else
         {
            gfx.io.GameDelegate.call("DeleteSave",[this.SaveLoadList_mc.__get__selectedIndex()]);
         }
         this.SaveLoadList_mc.__get__entryList().splice(this.SaveLoadList_mc.__get__selectedIndex(),1);
         this.SaveLoadList_mc.InvalidateData();
         this.onSaveLoadItemHighlight({index:this.SaveLoadList_mc.__get__selectedIndex()});
      }
   }
   function PopulateEmptySaveList()
   {
      trace("PopulateEmptySaveList");
      this.SaveLoadList_mc.ClearList();
      this.SaveLoadList_mc.__get__entryList()().push(new Object());
      this.onSaveLoadBatchComplete(true,0,0);
   }
   function OnSelectClicked()
   {
      this.onSaveLoadItemPress(null);
   }
   function OnBackClicked()
   {
      this.dispatchEvent({type:"OnSaveLoadPanelBackClicked"});
   }
}
