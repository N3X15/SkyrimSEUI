class QuantitySlider extends gfx.controls.Slider
{
   function QuantitySlider()
   {
      super();
   }
   function handleInput(details, pathToFocus)
   {
      var _loc4_ = super.handleInput(details,pathToFocus);
      if(!_loc4_)
      {
         if(Shared.GlobalFunc.IsKeyPressed(details))
         {
            if(details.navEquivalent == gfx.ui.NavigationCode.PAGE_DOWN || details.navEquivalent == gfx.ui.NavigationCode.GAMEPAD_L1)
            {
               this.__set__value(Math.floor(this.__get__value() - this.__get__maximum() / 4));
               this.dispatchEvent({type:"change"});
               _loc4_ = true;
            }
            else if(details.navEquivalent == gfx.ui.NavigationCode.PAGE_UP || details.navEquivalent == gfx.ui.NavigationCode.GAMEPAD_R1)
            {
               this.__set__value(Math.ceil(this.__get__value() + this.__get__maximum() / 4));
               this.dispatchEvent({type:"change"});
               _loc4_ = true;
            }
         }
      }
      return _loc4_;
   }
}
