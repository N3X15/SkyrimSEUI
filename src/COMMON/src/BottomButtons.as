class BottomButtons extends MovieClip
{
   var _platform = 0;
   var _ps3Switch = false;
   var _initialized = false;
   var _buttons = [];
   var _buttonMap = {};
   static var ACCEPT = {PCArt:"Enter",XBoxArt:"360_A",PS3Art:"PS3_A",Label:"$Accept",KeyCode:13};
   static var CANCEL = {PCArt:"Esc",XBoxArt:"360_B",PS3Art:"PS3_B",Label:"$Cancel",KeyCode:9};
   static var LIBRARY = {PCArt:"T",XBoxArt:"360_Y",PS3Art:"PS3_Y",Label:"$Mod_MyLibrary",KeyCode:84};
   static var DETAILS = {PCArt:"Enter",XBoxArt:"360_A",PS3Art:"PS3_A",Label:"$Mod_Details",KeyCode:13};
   static var ENABLE_MOD = {PCArt:"Enter",XBoxArt:"360_A",PS3Art:"PS3_A",Label:"$Mod_LibraryEnable",KeyCode:13};
   static var REORDER_MOD = {PCArt:"X",XBoxArt:"360_X",PS3Art:"PS3_X",Label:"$Mod_Reorder",KeyCode:88};
   static var DONE_REORDER_MOD = {PCArt:"X",XBoxArt:"360_X",PS3Art:"PS3_X",Label:"$Done",KeyCode:88};
   static var DELETE_MOD = {PCArt:"T",XBoxArt:"360_Y",PS3Art:"PS3_Y",Label:"$Mod_LibraryDelete",KeyCode:84};
   static var SEARCH = {PCArt:"X",XBoxArt:"360_X",PS3Art:"PS3_X",Label:"$Mod_Search",KeyCode:88};
   static var SEARCH_CONFIRM = {PCArt:"Enter",XBoxArt:"360_A",PS3Art:"PS3_A",Label:"$Mod_Search",KeyCode:13};
   static var OPTIONS = {PCArt:"V",XBoxArt:"360_Back",PS3Art:"PS3_Back",Label:"$Mod_AccountSettings",KeyCode:86};
   static var CONFIRM = {PCArt:"Enter",XBoxArt:"360_A",PS3Art:"PS3_A",Label:"$Select",KeyCode:13};
   static var BUTTON_CLICKED = "BottomButtons_ButtonClicked";
   static var BUTTON_HORIZONTAL_SPACING = 26;
   function BottomButtons()
   {
      super();
      gfx.events.EventDispatcher.initialize(this);
      this._visible = false;
      this.onEnterFrame = Shared.Proxy.create(this,this.Init);
   }
   function Init()
   {
      this.onEnterFrame = null;
      this._initialized = true;
      this.SetPlatform(this._platform,this._ps3Switch);
      this._visible = true;
   }
   function SetPlatform(aiPlatform, abPS3Switch)
   {
      this._platform = aiPlatform;
      this._ps3Switch = abPS3Switch;
   }
   function SetButtons(buttons)
   {
      var _loc3_ = 0;
      while(_loc3_ < this._buttons.length)
      {
         var _loc4_ = this._buttons[_loc3_];
         _loc4_.removeAllEventListeners();
         _loc4_.removeMovieClip();
         _loc3_ = _loc3_ + 1;
      }
      this._buttons.splice(0);
      this._buttonMap = {};
      _loc3_ = 0;
      while(_loc3_ < buttons.length)
      {
         var _loc2_ = (Components.CrossPlatformButtons)this.attachMovie(this.GetButtonID(this._platform),this.GetButtonID(this._platform) + "_" + _loc3_,this.getNextHighestDepth());
         _loc2_.addEventListener("stateChange",Shared.Proxy.create(this,this.ButtonStateChange));
         _loc2_.addEventListener("click",Shared.Proxy.create(this,this.ButtonClick));
         _loc2_.addEventListener("releaseOutside",Shared.Proxy.create(this,this.ButtonClick));
         _loc2_.textField.onChanged = Shared.Proxy.create(this,this.Reposition);
         _loc2_.textField.autoSize = true;
         _loc2_.SetArt(buttons[_loc3_]);
         _loc2_.__set__label(buttons[_loc3_].Label);
         _loc2_.__set__visible(true);
         _loc2_.SetPlatform(this._platform,this._ps3Switch);
         this._buttons.push(_loc2_);
         this._buttonMap[_loc2_] = buttons[_loc3_];
         _loc3_ = _loc3_ + 1;
      }
   }
   function GetButtonByIndex(index)
   {
      return this[this.GetButtonID(this._platform) + "_" + index];
   }
   function GetButtonID(platform)
   {
      return platform != 0?"GamepadButton":"MouseButton";
   }
   function Reposition()
   {
      var _loc6_ = (this._buttons.length - 1) * BottomButtons.BUTTON_HORIZONTAL_SPACING;
      var _loc4_ = 0;
      while(_loc4_ < this._buttons.length)
      {
         var _loc3_ = this._buttons[_loc4_].getBounds(this._buttons[_loc4_]);
         _loc6_ = _loc6_ + (_loc3_.xMax - _loc3_.xMin);
         _loc4_ = _loc4_ + 1;
      }
      this.bgMiddle_mc._width = _loc6_;
      var _loc8_ = _loc6_ + this.bgLeft_mc._width * 2;
      var _loc5_ = Stage.width / 2 - _loc8_ / 2;
      this.bgLeft_mc._x = _loc5_;
      this.bgMiddle_mc._x = this.bgLeft_mc._x + this.bgLeft_mc._width - 1;
      this.bgRight_mc._x = this.bgMiddle_mc._x + this.bgMiddle_mc._width - 1;
      var _loc7_ = this.bgMiddle_mc._y + (this.bgMiddle_mc._height - this._buttons[0]._height) / 2;
      _loc5_ = this.bgMiddle_mc._x;
      _loc4_ = 0;
      while(_loc4_ < this._buttons.length)
      {
         var _loc2_ = this._buttons[_loc4_];
         _loc3_ = _loc2_.getBounds(_loc2_);
         _loc2_._x = _loc5_ - _loc3_.xMin;
         _loc2_._y = _loc7_;
         _loc5_ = _loc5_ + (_loc3_.xMax - _loc3_.xMin + BottomButtons.BUTTON_HORIZONTAL_SPACING);
         _loc4_ = _loc4_ + 1;
      }
   }
   function ButtonStateChange(event)
   {
      var _loc1_ = event.target;
      _loc1_.textField.autoSize = true;
   }
   function ButtonClick(event)
   {
      var _loc2_ = event.target;
      this.dispatchEvent({type:BottomButtons.BUTTON_CLICKED,target:this,data:this._buttonMap[_loc2_]});
   }
}
