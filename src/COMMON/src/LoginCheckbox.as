class LoginCheckbox extends MovieClip
{
   static var ON_CHECK = "LoginCheckbox::checked";
   function LoginCheckbox()
   {
      super();
      gfx.events.EventDispatcher.initialize(this);
      this._Checked = false;
      this.FocusRect_mc._visible = false;
      this.onRelease = Shared.Proxy.create(this,this.onCheckboxClicked);
   }
   function __get__text()
   {
      return this.textField.text;
   }
   function __set__text(value)
   {
      this.textField.SetText(value);
      return this.__get__text();
   }
   function __get__checked()
   {
      return this._Checked;
   }
   function __set__checked(value)
   {
      this._Checked = value;
      return this.__get__checked();
   }
   function onCheckboxClicked()
   {
      this._Checked = !this._Checked;
      this.gotoAndStop(!this._Checked?"unchecked":"checked");
      this.dispatchEvent({type:LoginCheckbox.ON_CHECK,target:this});
   }
}
