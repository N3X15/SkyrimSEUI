class BookBottomBar extends MovieClip
{
   function BookBottomBar()
   {
      super();
      this.PageTurnButton = this.ButtonRect.TurnPageButtonInstance;
      this.TakeButton = this.ButtonRect.TakeButtonInstance;
   }
   function InitExtensions()
   {
      gfx.io.GameDelegate.addCallBack("ShowTakeButton",this,"ShowTakeButton");
      Shared.GlobalFunc.SetLockFunction();
      (MovieClip)this.Lock("BL");
   }
   function ShowTakeButton(abShow, abSteal)
   {
      this.TakeButton.__set__visible(abShow);
      this.TakeButton.__set__label(!abSteal?"$Take":"$Steal");
   }
   function SetPlatform(aiPlatformIndex, abPS3Switch)
   {
      this.PageTurnButton.SetPlatform(aiPlatformIndex,abPS3Switch);
      this.TakeButton.SetPlatform(aiPlatformIndex,abPS3Switch);
   }
}
