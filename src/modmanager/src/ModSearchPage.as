class ModSearchPage extends MovieClip
{
   function ModSearchPage()
   {
      super();
      gfx.events.EventDispatcher.initialize(this);
      this.SpinnerHolder_mc._visible = false;
      var _loc3_ = new Object();
      _loc3_.onChanged = Shared.Proxy.create(this,this.onSearchTextChanged);
      this.Input_tf.addListener(_loc3_);
   }
   function __get__CodeObject()
   {
      return this.codeObj;
   }
   function __set__CodeObject(value)
   {
      this.codeObj = value;
      return this.__get__CodeObject();
   }
   function Destroy()
   {
      return delete this.codeObj;
   }
   function SearchForMods()
   {
      if(this.Input_tf.text.length > 0)
      {
         this.SpinnerHolder_mc.EmptyWarning_tf._visible = false;
         this.SpinnerHolder_mc.Spinner_mc._visible = true;
         this.SpinnerHolder_mc._visible = true;
         this.codeObj.searchForMods(this.Input_tf.text);
      }
      else
      {
         this.codeObj.startEditText();
      }
   }
   function SearchReturned(category)
   {
      if(category == -1)
      {
         this.Input_tf.setSelection(0,this.Input_tf.length);
         this.SpinnerHolder_mc.EmptyWarning_tf._visible = true;
         this.SpinnerHolder_mc.Spinner_mc._visible = false;
      }
   }
   function onVKBTextEntered(astrEnteredText)
   {
      if(astrEnteredText.length > 0 && Selection.getFocus() == targetPath(this.Input_tf))
      {
         this.Input_tf.text = astrEnteredText;
         this.SearchGrayText_tf._visible = false;
      }
   }
   function handleInput(details, pathToFocus)
   {
      return true;
   }
   function onSearchTextChanged()
   {
      this.SearchGrayText_tf._visible = this.Input_tf.text.length == 0;
   }
}
