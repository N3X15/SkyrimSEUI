class ModAccountSettingsList extends Shared.BSScrollingList
{
   function ModAccountSettingsList()
   {
      super();
   }
   function SetEntryText(entryClip, entryObject)
   {
      super.SetEntryText(entryClip,entryObject);
      var _loc5_ = entryClip.textField;
      var _loc6_ = entryClip.border;
      if(!entryObject.disabled)
      {
         var _loc7_ = entryObject == this.__get__selectedEntry();
         if(_loc5_)
         {
            _loc5_.textColor = !_loc7_?16777215:0;
         }
         if(_loc6_ != null)
         {
            _loc6_._alpha = !_loc7_?0:100;
         }
      }
      else
      {
         _loc5_.textColor = 16777215;
         _loc6_._alpha = 0;
      }
      var _loc4_ = this.transform.colorTransform;
      _loc4_.redOffset = !entryObject.disabled?0:-128;
      _loc4_.greenOffset = !entryObject.disabled?0:-128;
      _loc4_.blueOffset = !entryObject.disabled?0:-128;
      entryClip.transform.colorTransform = _loc4_;
   }
}
