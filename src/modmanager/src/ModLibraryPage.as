class ModLibraryPage extends Components.BSUIComponent
{
   var _FreeSpace = 0;
   var _TotalModSpace = 0;
   var RIGHT_INPUT_SCROLL_THRESHOLD = 3;
   static var LIBRARY_ITEM_PRESSED = "LibraryItemPressed";
   function ModLibraryPage()
   {
      super();
      this.focusEnabled = true;
      this._ReorderingMode = false;
      this._StateChanged = false;
      this.EmptyWarning_tf._visible = false;
      this.ReorderIcon_mc._visible = false;
      this.onEnterFrame = Shared.Proxy.create(this,this.Init);
   }
   function __get__dataArray()
   {
      return this.List_mc.__get__entryList();
   }
   function __get__stateChanged()
   {
      return this._StateChanged;
   }
   function __set__stateChanged(aVal)
   {
      this._StateChanged = aVal;
      return this.__get__stateChanged();
   }
   function __get__reorderMode()
   {
      return this._ReorderingMode;
   }
   function __set__reorderMode(aVal)
   {
      if(this.List_mc.__get__selectedEntry() != undefined && !this.List_mc.__get__selectedEntry().disabled)
      {
         this._ReorderingMode = aVal;
         this.List_mc.__set__disableInput(this._ReorderingMode);
         this.UpdateReorderIconPosition();
      }
      return this.__get__reorderMode();
   }
   function __get__freeSpace()
   {
      return this._FreeSpace;
   }
   function __set__freeSpace(aVal)
   {
      this._FreeSpace = aVal;
      this.InvalidateData(false);
      return this.__get__freeSpace();
   }
   function SetBottomButtons(buttons)
   {
      this.bottomButtons = buttons;
   }
   function handleInput(details, pathToFocus)
   {
      var _loc2_ = this.List_mc.__get__selectedIndex();
      if(Shared.GlobalFunc.IsKeyPressed(details))
      {
         if(details.navEquivalent == gfx.ui.NavigationCode.UP && this.List_mc.__get__selectedIndex() > 0 && this.List_mc.__get__entryList()[this.List_mc.__get__selectedIndex() - 1].disabled != true)
         {
            this.List_mc.moveSelectionUp();
         }
         else if(details.navEquivalent == gfx.ui.NavigationCode.DOWN && this.List_mc.__get__selectedIndex() < this.List_mc.__get__entryList().length - 1 && this.List_mc.__get__entryList()[this.List_mc.__get__selectedIndex() + 1].disabled != true)
         {
            this.List_mc.moveSelectionDown();
         }
         if(_loc2_ != this.List_mc.__get__selectedIndex() && this.__get__reorderMode())
         {
            var _loc4_ = this.List_mc.__get__entryList().splice(_loc2_,1)[0];
            this.List_mc.__get__entryList().splice(this.List_mc.__get__selectedIndex(),0,_loc4_);
            this.List_mc.UpdateList();
            this.UpdateReorderIconPosition();
            this._StateChanged = true;
         }
         if(details.navEquivalent == gfx.ui.NavigationCode.ENTER && !this.__get__reorderMode() && this.List_mc.__get__selectedEntry())
         {
            this.onItemPress();
         }
      }
      return true;
   }
   function OnRightStickInput(afXDelta, afYDelta)
   {
      this.TextScrollDeltaAccum = this.TextScrollDeltaAccum + Math.abs(afYDelta);
      if(this.TextScrollDeltaAccum >= this.RIGHT_INPUT_SCROLL_THRESHOLD)
      {
         this.TextScrollDeltaAccum = 0;
         if(afYDelta > 0.1)
         {
            this.Description_tf.scroll = this.Description_tf.scroll - 1;
         }
         if(afYDelta < -0.1)
         {
            this.Description_tf.scroll = this.Description_tf.scroll + 1;
         }
         this.UpdateTextScrollIndicators();
      }
   }
   function Init()
   {
      this.onEnterFrame = null;
      this.List_mc.ScrollUp.onRelease = Shared.Proxy.create(this,this.onListScrollUp);
      this.List_mc.ScrollDown.onRelease = Shared.Proxy.create(this,this.onListScrollDown);
      this.List_mc.addEventListener("selectionChange",Shared.Proxy.create(this,this.onSelectionChange));
      this.List_mc.addEventListener("itemPress",Shared.Proxy.create(this,this.onItemPress));
      this.TextScrollUp.onRelease = Shared.Proxy.create(this,this.onTextScrollUpClicked);
      this.TextScrollDown.onRelease = Shared.Proxy.create(this,this.onTextScrollDownClicked);
      var _loc2_ = new Object();
      _loc2_.onMouseWheel = Shared.Proxy.create(this,this.onMouseWheel);
      Mouse.addListener(_loc2_);
   }
   function UpdateReorderIconPosition()
   {
      if(!this._ReorderingMode || this.List_mc.__get__selectedIndex() == -1)
      {
         this.ReorderIcon_mc._visible = false;
      }
      else
      {
         this.ReorderIcon_mc._y = this.List_mc._y + this.List_mc.GetClipByIndex(this.List_mc.__get__selectedEntry().clipIndex)._y + this.List_mc.GetClipByIndex(this.List_mc.__get__selectedEntry().clipIndex)._height / 2;
         this.ReorderIcon_mc._visible = true;
      }
   }
   function InvalidateData()
   {
      this.List_mc.InvalidateData();
      this._TotalModSpace = 0;
      for(var _loc2_ in this.List_mc.__get__entryList())
      {
         this._TotalModSpace = this._TotalModSpace + this.List_mc.__get__entryList()[_loc2_].fileSizeDisplay;
      }
      var _loc3_ = this._FreeSpace - this._TotalModSpace;
      this.TotalModSpace_tf.SetText(ModUtils.GetFileSizeString(this._TotalModSpace),false);
      this.TotalModSpace_tf._x = this.TotalModSpaceLabel_tf._x + this.TotalModSpaceLabel_tf.textWidth + 4;
      this.FreeSpace_tf.SetText(ModUtils.GetFileSizeString(_loc3_),false);
      this.FreeSpaceLabel_tf._x = this.FreeSpace_tf._x - this.FreeSpace_tf.textWidth;
      this.FreeSpace_tf._visible = _loc0_ = _loc3_ > 0;
      this.FreeSpaceLabel_tf._visible = _loc0_;
      this.EmptyWarning_tf._visible = this.List_mc.__get__entryList().length == 0;
   }
   function onDataObjectChange(aUpdatedObj)
   {
      var _loc2_ = 0;
      while(_loc2_ < this.List_mc.__get__entryList().length)
      {
         if(this.List_mc.__get__entryList()[_loc2_].dataObj == aUpdatedObj)
         {
         }
         _loc2_ = _loc2_ + 1;
      }
   }
   function ClearArray()
   {
      var _loc2_ = 0;
      while(_loc2_ < this.List_mc.__get__entryList().length)
      {
         this.List_mc.__get__entryList()[_loc2_].dataObj = null;
         _loc2_ = _loc2_ + 1;
      }
      this.List_mc.ClearList();
   }
   function onListScrollUp()
   {
      this.List_mc.moveSelectionUp();
   }
   function onListScrollDown()
   {
      this.List_mc.moveSelectionDown();
   }
   function onSelectionChange()
   {
      if(!this._ReorderingMode)
      {
         if(this.List_mc.__get__selectedEntry() != null && this.List_mc.__get__selectedEntry() != undefined && this.List_mc.__get__selectedEntry().dataObj instanceof Object)
         {
            this.Description_tf.SetText(this.List_mc.__get__selectedEntry().description,false);
            this.bottomButtons.GetButtonByIndex(0).__set__label(!this.List_mc.__get__selectedEntry().checked?"$Mod_LibraryEnable":"$Mod_LibraryDisable");
         }
         else
         {
            this.Description_tf._visible = false;
            this.DescriptionLabel_tf._visible = false;
         }
         this.UpdateTextScrollIndicators();
      }
   }
   function onItemPress()
   {
      if(this.List_mc.__get__selectedEntry() != null && this.List_mc.__get__selectedEntry().disabled != true)
      {
         this.dispatchEvent({type:ModLibraryPage.LIBRARY_ITEM_PRESSED,target:this,data:this.List_mc.__get__selectedEntry()});
      }
   }
   function UpdateTextScrollIndicators()
   {
      this.TextScrollUp._visible = this.Description_tf.scroll > 1;
      this.TextScrollDown._visible = this.Description_tf.scroll < this.Description_tf.maxscroll;
   }
   function onTextScrollUpClicked()
   {
      this.Description_tf.scroll = this.Description_tf.scroll - 1;
      this.UpdateTextScrollIndicators();
   }
   function onTextScrollDownClicked()
   {
      this.Description_tf.scroll = this.Description_tf.scroll + 1;
      this.UpdateTextScrollIndicators();
   }
   function onMouseWheel(delta)
   {
      if(this.DescriptionLabelInputCatcher_mc.hitTest(this._xmouse,this._ymouse,true))
      {
         if(delta < 0)
         {
            this.Description_tf.scroll = this.Description_tf.scroll + 1;
         }
         else if(delta > 0)
         {
            this.Description_tf.scroll = this.Description_tf.scroll - 1;
         }
         this.UpdateTextScrollIndicators();
      }
   }
}
