class ModDetailsPage extends Components.BSUIComponent
{
   var RIGHT_INPUT_SCROLL_THRESHOLD = 3;
   var optionsList = [{text:"$Follow",id:ModDetailsPage.FOLLOW_ID},{text:"$Unfollow",id:ModDetailsPage.UNFOLLOW_ID,filterFlag:0},{text:"$Download",id:ModDetailsPage.DOWNLOAD_ID,filterFlag:0},{text:"$Update",id:ModDetailsPage.UPDATE_ID,filterFlag:0},{text:"$EnableMod",id:ModDetailsPage.INSTALL_ID,filterFlag:0},{text:"$DisableMod",id:ModDetailsPage.UNINSTALL_ID,filterFlag:0},{text:"$Delete",id:ModDetailsPage.DELETE_ID,filterFlag:0},{text:"$Rate",id:ModDetailsPage.RATE_ID,showStars:true},{text:"$Report",id:ModDetailsPage.REPORT_ID}];
   static var FOLLOW_ID = 0;
   static var UNFOLLOW_ID = 1;
   static var DOWNLOAD_ID = 2;
   static var UPDATE_ID = 3;
   static var INSTALL_ID = 4;
   static var UNINSTALL_ID = 5;
   static var DELETE_ID = 6;
   static var RATE_ID = 7;
   static var REPORT_ID = 8;
   function ModDetailsPage()
   {
      super();
      this.focusEnabled = true;
      this._TempRating = 0;
      this._RatingChanged = false;
      this.TextScrollDeltaAccum = 0;
      this._ScreenshotURL = "";
      this._ScreenshotIndex = 0;
      this._MaxScreenshotIndex = 0;
      this._ScreenshotStateChange = false;
      this._OrigScreenshotWidth = this.Screenshot_mc.width;
      this._OrigScreenshotHeight = this.Screenshot_mc.height;
      this.TextScrollUp.onRelease = Shared.Proxy.create(this,this.onTextScrollUpClicked);
      this.TextScrollDown.onRelease = Shared.Proxy.create(this,this.onTextScrollDownClicked);
      this.ScreenshotScrollLeft.onRelease = Shared.Proxy.create(this,this.onScreenshotLeftClick);
      this.ScreenshotScrollRight.onRelease = Shared.Proxy.create(this,this.onScreenshotRightClick);
      this.onEnterFrame = Shared.Proxy.create(this,this.Init);
   }
   function handleInput(details, pathToFocus)
   {
      if(Shared.GlobalFunc.IsKeyPressed(details))
      {
         if(details.navEquivalent == gfx.ui.NavigationCode.UP)
         {
            this.OptionsList_mc.moveSelectionUp();
         }
         if(details.navEquivalent == gfx.ui.NavigationCode.DOWN)
         {
            this.OptionsList_mc.moveSelectionDown();
         }
         if(this.OptionsList_mc.__get__selectedEntry().showStars)
         {
            if(details.navEquivalent == gfx.ui.NavigationCode.LEFT && this._TempRating > 0.5)
            {
               this._TempRating = Math.min(Math.max(this._TempRating - 0.5,0.5),ModListEntry.MAX_RATING);
               this._RatingChanged = true;
               this.OptionsList_mc.__get__selectedEntry().rating = this._TempRating;
               this.OptionsList_mc.UpdateSelectedEntry();
            }
            else if(details.navEquivalent == gfx.ui.NavigationCode.RIGHT && this._TempRating < ModListEntry.MAX_RATING)
            {
               this._TempRating = Math.min(Math.max(this._TempRating + 0.5,0.5),ModListEntry.MAX_RATING);
               this._RatingChanged = true;
               this.OptionsList_mc.__get__selectedEntry().rating = this._TempRating;
               this.OptionsList_mc.UpdateSelectedEntry();
            }
         }
         else
         {
            if(details.navEquivalent == gfx.ui.NavigationCode.LEFT)
            {
               this.onScreenshotLeftClick();
            }
            if(details.navEquivalent == gfx.ui.NavigationCode.RIGHT)
            {
               this.onScreenshotRightClick();
            }
         }
      }
      return true;
   }
   function Init()
   {
      this.onEnterFrame = null;
      var _loc2_ = new Object();
      _loc2_.onMouseWheel = Shared.Proxy.create(this,this.onMouseWheel);
      Mouse.addListener(_loc2_);
      this.OptionsList_mc.ScrollUp.onRelease = Shared.Proxy.create(this,this.onOptionsScrollUp);
      this.OptionsList_mc.ScrollDown.onRelease = Shared.Proxy.create(this,this.onOptionsScrollDown);
      this.OptionsList_mc.UpdateList();
   }
   function __get__dataObj()
   {
      return this._DataObj;
   }
   function __set__dataObj(aObj)
   {
      this._DataObj = aObj;
      if(this._DataObj != null)
      {
         this._TempRating = this._DataObj.rating;
         this._RatingChanged = false;
         this.optionsList[ModDetailsPage.RATE_ID].rating = this._TempRating;
         this.Description_tf.scrollV = 1;
      }
      this.SetIsDirty();
      return this.__get__dataObj();
   }
   function __get__selectedEntry()
   {
      return this.OptionsList_mc.__get__selectedEntry();
   }
   function __get__tempRating()
   {
      return this._TempRating;
   }
   function __get__hasRatingChanged()
   {
      return this._RatingChanged;
   }
   function InvalidateData()
   {
      this.SetIsDirty();
   }
   function RedrawUIComponent()
   {
      if(this._DataObj != null)
      {
         this.Title_tf.SetText(this._DataObj.text,false);
         this.AuthorName_tf.SetText(this._DataObj.author,false);
         this.Description_tf.html = true;
         this.Description_tf.SetText(this._DataObj.description,true);
         this.UpdateTextScrollIndicators();
         this.Filesize_tf.SetText("$DownloadSize: " + ModUtils.GetFileSizeString(this._DataObj.fileSizeDisplay),false);
         this.RatingHolder_mc.__set__rating(this._DataObj.rating);
         this.RatingHolder_mc.__set__ratingCount(this._DataObj.ratingCount);
         this._MaxScreenshotIndex = !(this._DataObj.detailedImages instanceof Array)?0:this._DataObj.detailedImages.length - 1;
         this.ScreenshotScrollLeft._visible = this._ScreenshotIndex > 0;
         this.ScreenshotScrollRight._visible = this._ScreenshotIndex < this._MaxScreenshotIndex;
         this.optionsList[ModDetailsPage.FOLLOW_ID].filterFlag = this._DataObj.followed == true?0:1;
         this.optionsList[ModDetailsPage.FOLLOW_ID].text = this._DataObj.followSyncing != true?"$Follow":"$Following";
         this.optionsList[ModDetailsPage.UNFOLLOW_ID].filterFlag = this._DataObj.followed != true?0:1;
         this.optionsList[ModDetailsPage.UNFOLLOW_ID].text = this._DataObj.followSyncing != true?"$Unfollow":"$Unfollowing";
         this.optionsList[ModDetailsPage.DOWNLOAD_ID].filterFlag = this._DataObj.downloaded == true?0:1;
         if(typeof this._DataObj.downloadProgress == Number)
         {
            this.optionsList[ModDetailsPage.DOWNLOAD_ID].text = "$$Downloading... " + Math.floor(this._DataObj.downloadProgress) + "%";
         }
         else if(this._DataObj.subscribeSyncing == true)
         {
            this.optionsList[ModDetailsPage.DOWNLOAD_ID].text = "$Downloading...";
         }
         else
         {
            this.optionsList[ModDetailsPage.DOWNLOAD_ID].text = "$Download";
         }
         this.optionsList[ModDetailsPage.UPDATE_ID].filterFlag = !(this._DataObj.downloaded == true && this._DataObj.needsUpdate == true)?0:1;
         if(typeof this._DataObj.downloadProgress == Number)
         {
            this.optionsList[ModDetailsPage.UPDATE_ID].text = "$$Downloading... " + Math.floor(this._DataObj.downloadProgress) + "%";
         }
         else
         {
            this.optionsList[ModDetailsPage.UPDATE_ID].text = "$Update";
         }
         this.optionsList[ModDetailsPage.DELETE_ID].filterFlag = this._DataObj.downloaded != true?0:1;
         this.optionsList[ModDetailsPage.DELETE_ID].text = this._DataObj.deleteSyncing != true?"$Delete":"$Deleting";
         this.optionsList[ModDetailsPage.INSTALL_ID].filterFlag = !(this._DataObj.downloaded == true && (this._DataObj.installed != true || this._DataObj.uninstallQueued == true) && this._DataObj.installQueued != true)?0:1;
         this.optionsList[ModDetailsPage.INSTALL_ID].text = "$EnableMod";
         this.optionsList[ModDetailsPage.UNINSTALL_ID].filterFlag = !(this._DataObj.downloaded == true && (this._DataObj.installed == true || this._DataObj.installQueued == true) && this._DataObj.uninstallQueued != true)?0:1;
         this.optionsList[ModDetailsPage.UNINSTALL_ID].text = "$DisableMod";
         if(this._DataObj.reportSyncing == true)
         {
            this.optionsList[ModDetailsPage.REPORT_ID].text = "$Reporting...";
         }
         else
         {
            this.optionsList[ModDetailsPage.REPORT_ID].text = this._DataObj.reported != true?"$Report":"$Reported";
         }
         this.optionsList[ModDetailsPage.REPORT_ID].disabled = this._DataObj.reported == true;
         this.OptionsList_mc.__get__entryList().splice(0);
         var _loc2_ = 0;
         while(_loc2_ < this.optionsList.length)
         {
            if(this.optionsList[_loc2_].filterFlag != 0)
            {
               this.OptionsList_mc.__get__entryList().push(this.optionsList[_loc2_]);
            }
            _loc2_ = _loc2_ + 1;
         }
         this.OptionsList_mc.InvalidateData();
         this._visible = true;
      }
      else
      {
         this._visible = false;
      }
   }
   function ShowError(astrErrorText)
   {
      this.Error_tf.SetText(astrErrorText,false);
      setTimeout(Shared.Proxy.create(this,this.onShowErrorTimerDone),2000);
   }
   function onShowErrorTimerDone()
   {
      this.Error_tf.SetText(" ",false);
   }
   function UpdateTextScrollIndicators()
   {
      this.TextScrollUp._visible = this.Description_tf.scroll > 1;
      this.TextScrollDown._visible = this.Description_tf.scroll < this.Description_tf.maxscroll;
   }
   function OnRightStickInput(afXDelta, afYDelta)
   {
      this.TextScrollDeltaAccum = this.TextScrollDeltaAccum + Math.abs(afYDelta);
      if(this.TextScrollDeltaAccum >= this.RIGHT_INPUT_SCROLL_THRESHOLD)
      {
         this.TextScrollDeltaAccum = 0;
         if(afYDelta > 0.1)
         {
            this.Description_tf.scroll = this.Description_tf.scroll - 1;
         }
         if(afYDelta < -0.1)
         {
            this.Description_tf.scroll = this.Description_tf.scroll + 1;
         }
         this.UpdateTextScrollIndicators();
      }
   }
   function onTextScrollUpClicked()
   {
      this.Description_tf.scroll = this.Description_tf.scroll - 1;
      this.UpdateTextScrollIndicators();
   }
   function onTextScrollDownClicked()
   {
      this.Description_tf.scroll = this.Description_tf.scroll + 1;
      this.UpdateTextScrollIndicators();
   }
   function onMouseWheel(delta)
   {
      if(!this.OptionsList_mc.hitTest(this._xmouse,this._ymouse,true) && this.hitTest(this._xmouse,this._ymouse,true))
      {
         if(delta < 0)
         {
            this.Description_tf.scroll = this.Description_tf.scroll + 1;
         }
         else if(delta > 0)
         {
            this.Description_tf.scroll = this.Description_tf.scroll - 1;
         }
         this.UpdateTextScrollIndicators();
      }
   }
   function onOptionsScrollUp()
   {
      this.OptionsList_mc.moveSelectionUp();
   }
   function onOptionsScrollDown()
   {
      this.OptionsList_mc.moveSelectionDown();
   }
   function onScreenshotLeftClick()
   {
      if(this._ScreenshotIndex > 0)
      {
         this._ScreenshotIndex = this._ScreenshotIndex - 1;
         this._ScreenshotURL = this._DataObj.detailedImages[this._ScreenshotIndex].imageTextureName;
         this._ScreenshotLoadState = this._DataObj.detailedImages[this._ScreenshotIndex].imageLoadState;
         this._ScreenshotStateChange = true;
         this.SetIsDirty();
      }
   }
   function onScreenshotRightClick()
   {
      if(this._ScreenshotIndex < this._MaxScreenshotIndex)
      {
         this._ScreenshotIndex = this._ScreenshotIndex + 1;
         this._ScreenshotURL = this._DataObj.detailedImages[this._ScreenshotIndex].imageTextureName;
         this._ScreenshotLoadState = this._DataObj.detailedImages[this._ScreenshotIndex].imageLoadState;
         this._ScreenshotStateChange = true;
         this.SetIsDirty();
      }
   }
}
