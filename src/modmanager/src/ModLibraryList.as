class ModLibraryList extends Shared.BSScrollingList
{
   var STAR_SPACING = 2.5;
   function ModLibraryList()
   {
      super();
   }
   function SetEntryText(aEntryClip, aEntryObject)
   {
      super.SetEntryText(aEntryClip,aEntryObject);
      aEntryClip.SetEntryData(aEntryObject,aEntryObject == this.__get__selectedEntry());
   }
   function GetClipByIndex(aiIndex)
   {
      return super.GetClipByIndex(aiIndex);
   }
   function UpdateSelectedEntry()
   {
      if(this.iSelectedIndex != -1)
      {
         this.SetEntry(this.GetClipByIndex(this.EntriesA[this.iSelectedIndex].clipIndex),this.EntriesA[this.iSelectedIndex]);
      }
   }
}
