class ModListEntry extends MovieClip
{
   var TEXT_STAR_INTERSTIT = 5;
   var STAR_SPACING = 2.5;
   var ICON_SPACING = 5;
   static var ILS_NOT_LOADED = 0;
   static var ILS_DOWNLOADING = 1;
   static var ILS_DOWNLOADED = 2;
   static var ILS_DISPLAYED = 3;
   static var MAX_RATING = 5;
   static var MAX_TEXT_LEN = 30;
   static var LOAD_THUMBNAIL = "ModListEntry::loadThumbnail";
   static var UNREGISTER_IMAGE = "ModListEntry::unregisterImage";
   static var DISPLAY_IMAGE = "ModListEntry::displayImage";
   function ModListEntry()
   {
      super();
      this._ItemIndex = 1.7976931348623157e308;
      var _loc3_ = new Object();
      _loc3_.onLoadComplete = Shared.Proxy.create(this,this.onThumbnailLoadComplete);
      this._ThumbnailLoader = new MovieClipLoader();
      this._ThumbnailLoader.addListener(_loc3_);
   }
   function __get__itemIndex()
   {
      return this._ItemIndex;
   }
   function __set__itemIndex(aVal)
   {
      this._ItemIndex = aVal;
      return this.__get__itemIndex();
   }
   function __get__dataObj()
   {
      return this._DataObj;
   }
   function __set__dataObj(aObj)
   {
      if(this._DataObj != null && aObj == null)
      {
         this.UnloadThumbnail();
      }
      else if(this._DataObj != null && aObj.imageTextureName != this._DataObj.imageTextureName)
      {
         this.UnloadThumbnail();
      }
      this._DataObj = aObj;
      if(this._DataObj != null)
      {
         if(this._DataObj.imageLoadState == ModListEntry.ILS_NOT_LOADED)
         {
         }
         if(this._DataObj.imageLoadState == ModListEntry.ILS_DOWNLOADED)
         {
         }
      }
      this.redrawUIComponent();
      return this.__get__dataObj();
   }
   function onThumbnailLoadComplete()
   {
   }
   function UnloadThumbnail()
   {
      if(this._DataObj != null)
      {
         if(!this._ThumbnailLoader)
         {
         }
         this._DataObj.imageLoadState = ModListEntry.ILS_NOT_LOADED;
      }
   }
   function redrawUIComponent()
   {
      if(this._DataObj == null)
      {
         this._visible = false;
      }
      else
      {
         if(this._DataObj.text.length < ModListEntry.MAX_TEXT_LEN)
         {
            this.textField.SetText(this._DataObj.text);
         }
         else
         {
            this.textField.SetText(this._DataObj.text.substr(0,ModListEntry.MAX_TEXT_LEN) + "...");
         }
         this.RatingHolder_mc._y = this.textField._y + this.textField._height;
         for(var _loc8_ in this.RatingHolder_mc)
         {
            if(this.RatingHolder_mc[_loc8_] instanceof MovieClip)
            {
               this.RatingHolder_mc[_loc8_].removeMovieClip();
            }
         }
         var _loc3_ = 0;
         while(_loc3_ < ModListEntry.MAX_RATING)
         {
            var _loc2_ = undefined;
            var _loc4_ = this._DataObj.rating - _loc3_;
            if(_loc4_ <= 0.25)
            {
               _loc2_ = this.RatingHolder_mc.attachMovie("Star_Empty","StarRating",this.RatingHolder_mc.getNextHighestDepth());
            }
            else if(_loc4_ > 0.25 && _loc4_ <= 0.75)
            {
               _loc2_ = this.RatingHolder_mc.attachMovie("Star_HalfFull","StarRating",this.RatingHolder_mc.getNextHighestDepth());
            }
            else
            {
               _loc2_ = this.RatingHolder_mc.attachMovie("Star_Full","StarRating",this.RatingHolder_mc.getNextHighestDepth());
            }
            _loc2_._x = (_loc2_._width + this.STAR_SPACING) * _loc3_;
            _loc3_ = _loc3_ + 1;
         }
         var _loc6_ = 0;
         var _loc5_ = undefined;
         for(var _loc8_ in this.IconHolder_mc)
         {
            if(this.IconHolder_mc[_loc8_] instanceof MovieClip)
            {
               this.IconHolder_mc[_loc8_].removeMovieClip();
            }
         }
         if(this._DataObj.followed == true)
         {
            _loc5_ = this.IconHolder_mc.attachMovie("Icon_Followed","Icon",this.IconHolder_mc.getNextHighestDepth());
            _loc5_._x = _loc6_ - _loc5_._width;
            _loc6_ = _loc6_ - (_loc5_._width + this.ICON_SPACING);
         }
         if(this._DataObj.downloaded == true)
         {
            _loc5_ = this.IconHolder_mc.attachMovie("Icon_Downloaded","Icon",this.IconHolder_mc.getNextHighestDepth());
            _loc5_._x = _loc6_ - _loc5_._width;
            _loc6_ = _loc6_ - (_loc5_._width + this.ICON_SPACING);
         }
         if(this._DataObj.installed == true || this._DataObj.installQueued == true || this._DataObj.uninstallQueued == true)
         {
            _loc5_ = this.IconHolder_mc.attachMovie("Icon_Installed","Icon",this.IconHolder_mc.getNextHighestDepth());
            _loc5_._x = _loc6_ - _loc5_._width;
            var _loc7_ = _loc5_.transform.colorTransform;
            _loc7_.redOffset = !(this._DataObj.installQueued == true || this._DataObj.uninstallQueued == true)?0:-128;
            _loc7_.greenOffset = !(this._DataObj.installQueued == true || this._DataObj.uninstallQueued == true)?0:-128;
            _loc7_.blueOffset = !(this._DataObj.installQueued == true || this._DataObj.uninstallQueued == true)?0:-128;
            _loc5_.transform.colorTransform = _loc7_;
            _loc6_ = _loc6_ - (_loc5_.width + this.ICON_SPACING);
         }
         this._visible = true;
      }
   }
}
