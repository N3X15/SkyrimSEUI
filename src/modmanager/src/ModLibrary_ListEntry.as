class ModLibrary_ListEntry extends MovieClip
{
   static var MAX_TEXT_LEN = 30;
   function ModLibrary_ListEntry()
   {
      super();
      _global.gfxExtensions = true;
      this.ORIG_BORDER_HEIGHT = this.border == null?0:this.border.height;
      this._HasDynamicHeight = false;
   }
   function __get__selected()
   {
      return this._selected;
   }
   function __set__selected(flag)
   {
      this._selected = flag;
      return this.__get__selected();
   }
   function SetEntryData(aEntryObject, selected)
   {
      if(aEntryObject.text.length >= ModLibrary_ListEntry.MAX_TEXT_LEN)
      {
         this.textField.SetText(aEntryObject.text.substr(0,ModLibrary_ListEntry.MAX_TEXT_LEN) + "...",false);
      }
      if(this._DataObj != null && typeof aEntryObject.dataObj == "object" && aEntryObject.dataObj.imageTextureName != this._DataObj.imageTextureName && this._ThumbnailLoader.content != null)
      {
         this.UnloadThumbnail();
      }
      else if(typeof aEntryObject.dataObj != "object")
      {
         this.UnloadThumbnail();
      }
      this._DataObj = typeof aEntryObject.dataObj != "object"?null:aEntryObject.dataObj;
      if(this._DataObj == null)
      {
         this.ScreenshotHolder_mc.Screenshot_mc.Spinner_mc._visible = false;
      }
      this.EquipIcon_mc._visible = aEntryObject.checked == true;
      var _loc2_ = this.transform.colorTransform;
      _loc2_.redOffset = !aEntryObject.disabled?0:-128;
      _loc2_.greenOffset = !aEntryObject.disabled?0:-128;
      _loc2_.blueOffset = !aEntryObject.disabled?0:-128;
      this.transform.colorTransform = _loc2_;
      _loc2_ = this.EquipIcon_mc.transform.colorTransform;
      _loc2_.redOffset = !selected?0:-255;
      _loc2_.greenOffset = !selected?0:-255;
      _loc2_.blueOffset = !selected?0:-255;
      this.EquipIcon_mc.transform.colorTransform = _loc2_;
      this.Order_tf.textColor = !this.__get__selected()?16777215:0;
      this.textField.textColor = !selected?16777215:0;
      this.border._alpha = !selected?0:100;
   }
   function onThumbnailLoadComplete()
   {
      if(this._DataObj != null)
      {
         this._ThumbnailLoader.width = this.ScreenshotHolder_mc.width / this.ScreenshotHolder_mc.scaleX;
         this._ThumbnailLoader.height = this.ScreenshotHolder_mc.height / this.ScreenshotHolder_mc.scaleY;
      }
   }
   function UnloadThumbnail()
   {
      if(this._DataObj != null)
      {
         this.ScreenshotHolder_mc.removeChild(this._ThumbnailLoader);
         this._ThumbnailLoader.unloadAndStop();
         this._DataObj.imageLoadState = ModListEntry.ILS_NOT_LOADED;
      }
   }
}
