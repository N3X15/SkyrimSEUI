class ModCategoryList extends MovieClip
{
   var SELECTION_RECT_DIRTY = 1;
   var NAME_DIRTY = 2;
   var SCROLL_DIRTY = 4;
   var ALL_DIRTY = 4294967295;
   var MAX_ENTRIES = 7;
   var ENTRY_SPACING = 7.5;
   static var PLAY_SOUND = "PlaySound";
   function ModCategoryList()
   {
      super();
      gfx.events.EventDispatcher.initialize(this);
      this.focusEnabled = true;
      this._CurrScrollPage = 0;
      this._MaxScrollPage = 0;
      this._EntryClipsA = new Array();
      this._SelectedIndex = 1.7976931348623157e308;
      this._MaxSelectedIndex = 1.7976931348623157e308;
      this._IsDirtyCustomFlag = 0;
      this.onEnterFrame = Shared.Proxy.create(this,this.redrawUIComponent);
      this.onSetFocus = Shared.Proxy.create(this,this.onListSetFocus);
      this.onKillFocus = Shared.Proxy.create(this,this.onListKillFocus);
      this.ScrollLeft.onRelease = Shared.Proxy.create(this,this.onScrollLeftClick);
      this.ScrollRight.onRelease = Shared.Proxy.create(this,this.onScrollRightClick);
      this.PopulateEntryClips();
      this.SetIsDirty_Custom(this.SELECTION_RECT_DIRTY);
   }
   function handleInput(details, pathToFocus)
   {
      if(Shared.GlobalFunc.IsKeyPressed(details))
      {
         if(details.navEquivalent == gfx.ui.NavigationCode.ENTER)
         {
            this.dispatchEvent({type:gfx.events.EventTypes.ITEM_CLICK,target:this});
         }
         if(details.navEquivalent == gfx.ui.NavigationCode.LEFT)
         {
            this.moveSelectionLeft();
         }
         else if(details.navEquivalent == gfx.ui.NavigationCode.RIGHT)
         {
            this.moveSelectionRight();
         }
      }
      return true;
   }
   function SetIsDirty_Custom(aFlag)
   {
      this._IsDirtyCustomFlag = this._IsDirtyCustomFlag | aFlag;
   }
   function __get__selectedIndex()
   {
      return this._SelectedIndex;
   }
   function __set__selectedIndex(aVal)
   {
      this._SelectedIndex = aVal;
      if(this._SelectedIndex != 1.7976931348623157e308)
      {
         this._SelectedIndex = Math.min(this._SelectedIndex,this._MaxSelectedIndex);
      }
      this.SetIsDirty_Custom(this.SELECTION_RECT_DIRTY);
      return this.__get__selectedIndex();
   }
   function __get__selectedEntry()
   {
      return this._SelectedIndex == 1.7976931348623157e308?null:this._DataObj.entries[this._CurrScrollPage + this._SelectedIndex];
   }
   function __get__currScrollPage()
   {
      return this._CurrScrollPage;
   }
   function __set__currScrollPage(aVal)
   {
      if(aVal == this._CurrScrollPage - 1 && this._CurrScrollPage > 0)
      {
         this._CurrScrollPage = this._CurrScrollPage - 1;
         this._EntryClipsA[this.MAX_ENTRIES - 1].UnloadThumbnail();
         var _loc6_ = this._EntryClipsA[0]._x;
         var _loc2_ = 0;
         while(_loc2_ < this.MAX_ENTRIES - 1)
         {
            this._EntryClipsA[_loc2_]._x = this._EntryClipsA[_loc2_ + 1]._x;
            _loc2_ = _loc2_ + 1;
         }
         this._EntryClipsA.splice(0,0,this._EntryClipsA.pop());
         this._EntryClipsA[0]._x = _loc6_;
         this._EntryClipsA[0].dataObj = this._DataObj.entries[this._CurrScrollPage];
         this.SetIsDirty_Custom(this.SCROLL_DIRTY);
      }
      else if(aVal == this._CurrScrollPage + 1 && this._CurrScrollPage < this._MaxScrollPage)
      {
         this._CurrScrollPage = this._CurrScrollPage + 1;
         this._EntryClipsA[0].UnloadThumbnail();
         var _loc5_ = this._EntryClipsA[this.MAX_ENTRIES - 1]._x;
         var _loc3_ = this.MAX_ENTRIES - 1;
         while(_loc3_ >= 1)
         {
            this._EntryClipsA[_loc3_]._x = this._EntryClipsA[_loc3_ - 1]._x;
            _loc3_ = _loc3_ - 1;
         }
         this._EntryClipsA.push(this._EntryClipsA.shift());
         this._EntryClipsA[this.MAX_ENTRIES - 1]._x = _loc5_;
         this._EntryClipsA[this.MAX_ENTRIES - 1].dataObj = this._DataObj.entries[this._CurrScrollPage + this.MAX_ENTRIES - 1];
         this.SetIsDirty_Custom(this.SCROLL_DIRTY);
      }
      else if(aVal >= 0 && aVal <= this._MaxScrollPage)
      {
         this._CurrScrollPage = aVal;
         this.RefreshEntryData();
         this.RefreshScrollVars();
      }
      if(this._DataObj != null)
      {
         this._DataObj.savedScrollPage = this._CurrScrollPage;
      }
      return this.__get__currScrollPage();
   }
   function __get__dataObj()
   {
      return this._DataObj;
   }
   function __set__dataObj(aObj)
   {
      var _loc3_ = this._SelectedIndex;
      var _loc2_ = this.NAME_DIRTY;
      this._DataObj = aObj;
      if(typeof this._DataObj.savedScrollPage == "number")
      {
         this._CurrScrollPage = this._DataObj.savedScrollPage;
      }
      else
      {
         this._CurrScrollPage = 0;
         this._DataObj.savedScrollPage = 0;
      }
      this.RefreshEntryData();
      if(this._SelectedIndex != _loc3_)
      {
         _loc2_ = _loc2_ | this.SELECTION_RECT_DIRTY;
      }
      this.RefreshScrollVars();
      this.SetIsDirty_Custom(_loc2_);
      return this.__get__dataObj();
   }
   function onDataObjectChange(aUpdatedObj)
   {
      var _loc2_ = 0;
      while(_loc2_ < this.MAX_ENTRIES)
      {
         if(this._DataObj.entries[this._CurrScrollPage + _loc2_] == aUpdatedObj)
         {
            this._EntryClipsA[_loc2_].dataObj = aUpdatedObj;
         }
         _loc2_ = _loc2_ + 1;
      }
   }
   function JumpLeft()
   {
      if(this._CurrScrollPage > 0)
      {
         var _loc2_ = 0;
         while(_loc2_ < this.MAX_ENTRIES)
         {
            _loc2_ = _loc2_ + 1;
         }
         this.__set__currScrollPage(this.__get__currScrollPage() - Math.min(this.__get__currScrollPage(),this.MAX_ENTRIES));
         this.dispatchEvent({type:ModCategoryList.PLAY_SOUND,target:this,sound:"UIMenuFocus"});
      }
   }
   function JumpRight()
   {
      if(this._CurrScrollPage < this._MaxScrollPage)
      {
         var _loc2_ = 0;
         while(_loc2_ < this.MAX_ENTRIES)
         {
            _loc2_ = _loc2_ + 1;
         }
         this.__set__currScrollPage(this.__get__currScrollPage() + Math.min(this._MaxScrollPage - this.__get__currScrollPage(),this.MAX_ENTRIES));
         this.dispatchEvent({type:ModCategoryList.PLAY_SOUND,target:this,sound:"UIMenuFocus"});
      }
   }
   function PopulateEntryClips()
   {
      if(this._EntryClipsA.length > 0)
      {
         for(var _loc3_ in this._EntryClipsA)
         {
            (MovieClip)this._EntryClipsA[_loc3_].removeMovieClip();
         }
      }
      this._EntryClipsA.splice(0);
      var _loc2_ = 0;
      while(_loc2_ < this.MAX_ENTRIES)
      {
         this.AddModListEntry(_loc2_);
         _loc2_ = _loc2_ + 1;
      }
   }
   function AddModListEntry(index)
   {
      if(index == undefined)
      {
         index = 0;
      }
      var _loc2_ = (ModListEntry)this.EntryHolder_mc.attachMovie("ModListEntry","ModListEntry" + index,this.EntryHolder_mc.getNextHighestDepth());
      _loc2_.onRelease = Shared.Proxy.create(this,this.onEntryPress,_loc2_);
      _loc2_.onRollOver = Shared.Proxy.create(this,this.onEntryRollover,_loc2_);
      _loc2_._x = (_loc2_._width + this.ENTRY_SPACING) * index;
      _loc2_._visible = false;
      _loc2_.__set__itemIndex(index);
      this._EntryClipsA.push(_loc2_);
      return _loc2_;
   }
   function RefreshEntryData()
   {
      this._MaxSelectedIndex = 1.7976931348623157e308;
      var _loc2_ = 0;
      while(_loc2_ < this._EntryClipsA.length)
      {
         if(this._DataObj != null && this._DataObj.entries != null && this._CurrScrollPage + _loc2_ < this._DataObj.entries.length)
         {
            this._EntryClipsA[_loc2_].dataObj = this._DataObj.entries[this._CurrScrollPage + _loc2_];
            this._MaxSelectedIndex = _loc2_;
         }
         else
         {
            this._EntryClipsA[_loc2_].dataObj = null;
         }
         _loc2_ = _loc2_ + 1;
      }
      if(this._DataObj == null || this._DataObj.entries.length == 0)
      {
         this.EmptyClip_mc._visible = true;
         this._MaxSelectedIndex = 0;
      }
      else
      {
         this.EmptyClip_mc._visible = false;
      }
      if(this._MaxSelectedIndex == 1.7976931348623157e308)
      {
         this._SelectedIndex = 1.7976931348623157e308;
      }
      else if(this._SelectedIndex != 1.7976931348623157e308)
      {
         this._SelectedIndex = Math.min(this._SelectedIndex,this._MaxSelectedIndex);
      }
   }
   function GetParentEntry(aChildClip)
   {
      var _loc1_ = aChildClip;
      while(_loc1_ != null && !(_loc1_ instanceof ModListEntry))
      {
         _loc1_ = _loc1_._parent;
      }
      return (ModListEntry)_loc1_;
   }
   function onEntryRollover(value1, value2, value3, mc)
   {
      var _loc2_ = this.GetParentEntry(mc);
      if(_loc2_ != undefined)
      {
         var _loc3_ = this._SelectedIndex;
         this.__set__selectedIndex(_loc2_.itemIndex);
         if(_loc3_ != this._SelectedIndex)
         {
            this.dispatchEvent({type:ModCategoryList.PLAY_SOUND,target:this,sound:"UIMenuFocus"});
         }
         this.dispatchEvent({type:gfx.events.EventTypes.ITEM_ROLL_OVER,target:mc});
      }
   }
   function onEntryPress(value1, value2, value3, mc)
   {
      if(this.GetParentEntry(mc) != undefined)
      {
         this.dispatchEvent({type:gfx.events.EventTypes.ITEM_CLICK,target:this});
      }
   }
   function RefreshScrollVars()
   {
      if(this._DataObj == null || this._DataObj.entries == null || this._DataObj.entries.length <= this.MAX_ENTRIES)
      {
         this._MaxScrollPage = 0;
      }
      else
      {
         this._MaxScrollPage = this._DataObj.entries.length - this.MAX_ENTRIES;
      }
      this._CurrScrollPage = Math.min(this._CurrScrollPage,this._MaxScrollPage);
   }
   function redrawUIComponent()
   {
      if((this._IsDirtyCustomFlag & this.NAME_DIRTY) != 0)
      {
         this.CategoryName_tf.SetText(this._DataObj.text);
      }
      if((this._IsDirtyCustomFlag & this.SELECTION_RECT_DIRTY) != 0)
      {
         if(Selection.getFocus() != targetPath(this))
         {
            this.Selection_mc._visible = false;
         }
         else
         {
            this.Selection_mc._x = this._EntryClipsA[this._SelectedIndex]._x + 0;
            this.Selection_mc._y = this._EntryClipsA[this._SelectedIndex]._y + 0;
            this.Selection_mc._visible = true;
         }
      }
      if(this.ScrollLeft != null)
      {
         this.ScrollLeft._visible = this._CurrScrollPage > 0;
      }
      if(this.ScrollRight != null)
      {
         this.ScrollRight._visible = this._MaxScrollPage > 0 && this._CurrScrollPage < this._MaxScrollPage;
      }
      this._IsDirtyCustomFlag = 0;
   }
   function onListSetFocus()
   {
      this.SetIsDirty_Custom(this.SELECTION_RECT_DIRTY);
   }
   function onListKillFocus()
   {
      this.SetIsDirty_Custom(this.SELECTION_RECT_DIRTY);
   }
   function moveSelectionLeft()
   {
      if(this.__get__selectedIndex() > 0)
      {
         this.__set__selectedIndex(this.__get__selectedIndex() - 1);
         this.dispatchEvent({type:ModCategoryList.PLAY_SOUND,target:this,sound:"UIMenuFocus"});
      }
      else if(this._CurrScrollPage > 0)
      {
         this.__set__currScrollPage(this.__get__currScrollPage() - 1);
         this.dispatchEvent({type:ModCategoryList.PLAY_SOUND,target:this,sound:"UIMenuFocus"});
      }
   }
   function moveSelectionRight()
   {
      if(this.__get__selectedIndex() < this.MAX_ENTRIES - 1)
      {
         this.__set__selectedIndex(this.__get__selectedIndex() + 1);
         this.dispatchEvent({type:ModCategoryList.PLAY_SOUND,target:this,sound:"UIMenuFocus"});
      }
      else if(this._CurrScrollPage < this._MaxScrollPage)
      {
         this.__set__currScrollPage(this.__get__currScrollPage() + 1);
         this.dispatchEvent({type:ModCategoryList.PLAY_SOUND,target:this,sound:"UIMenuFocus"});
      }
   }
   function onScrollLeftClick()
   {
      if(this._CurrScrollPage > 0)
      {
         this.__set__currScrollPage(this.__get__currScrollPage() - 1);
         this.dispatchEvent({type:ModCategoryList.PLAY_SOUND,target:this,sound:"UIMenuFocus"});
      }
   }
   function onScrollRightClick()
   {
      if(this._CurrScrollPage < this._MaxScrollPage)
      {
         this.__set__currScrollPage(this.__get__currScrollPage() + 1);
         this.dispatchEvent({type:ModCategoryList.PLAY_SOUND,target:this,sound:"UIMenuFocus"});
      }
   }
}
