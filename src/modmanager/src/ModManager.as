class ModManager extends MovieClip
{
   var _DataArray = [];
   var iPlatform = 0;
   var PS3Switch = false;
   var _CurrScrollPage = 0;
   var _codeObjInitialized = false;
   var _LoggedIn = false;
   var screenshots = [{imageTextureName:"https://mods.bethesda.net/content/90971844-3bb5-47e8-a81d-d45a8dcd3038_preview_826074_12045768523281250912_small.png",size:736704,imageLoadState:0},{imageTextureName:"https://mods.bethesda.net/content/42b23e91-f290-4de5-b03a-b71548687d89_preview_826074_3448875642746513496_small.png",size:754153,imageLoadState:0},{imageTextureName:"https://mods.bethesda.net/content/627d65c8-fc01-4f2e-abe5-62290346ba79_preview_753498_16200222302657877289_small.png",size:238630,imageLoadState:0},{imageTextureName:"https://mods.bethesda.net/content/9911cd65-b3ba-434d-8e81-2ec47d62374b_preview_783502_2452649863427861541_small.png",size:534569,imageLoadState:0},{imageTextureName:"https://mods.bethesda.net/content/d7d00113-359c-4254-a370-ed7bc4814968_preview_783502_7939058998672058091_small.png",size:848530,imageLoadState:0}];
   var texts = ["Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n\nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n\nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n\nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n\nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n\nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n\nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\n","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. \n\nNemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? \n\nQuis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\n\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. \n\nNemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? \n\nQuis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\n\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. \n\nNemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? \n\nQuis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\n\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. \n\nNemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? \n\nQuis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\n\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. \n\nNemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? \n\nQuis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\n\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. \n\nNemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? \n\nQuis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\n\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. \n\nNemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? \n\nQuis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\n\n"];
   static var LOGIN_STATE = "LoginState";
   static var MOD_CATEGORY_LIST_STATE = "ModCategoryListState";
   static var MOD_DETAIL_STATE = "ModDetailState";
   static var MOD_LIBRARY_STATE = "ModLibraryState";
   static var MOD_LIBRARY_REORDER_STATE = "ModLibraryReorderState";
   static var MOD_SEARCH_STATE = "ModSearchState";
   static var ACCOUNT_SETTINGS_STATE = "AccountSettingsState";
   static var DELETE_ALL_MODS_OPTION = "DeleteAllModsOption";
   static var DISABLE_ALL_MODS_OPTION = "DisableAllModsOption";
   function ModManager()
   {
      super();
      _global.gfxExtensions = true;
      Shared.GlobalFunc.MaintainTextFormat();
      _root.CodeObj = this.codeObj = new Object();
      _root.InitExtensions = Shared.Proxy.create(this,this.InitExtensions);
      _root.onCodeObjectInit = Shared.Proxy.create(this,this.onCodeObjectInit);
      _root.ReleaseCodeObject = Shared.Proxy.create(this,this.ReleaseCodeObject);
      _root.SetPlatform = Shared.Proxy.create(this,this.SetPlatform);
      _root.onRightStickInput = Shared.Proxy.create(this,this.OnRightStickInput);
      this.DisplayScreen(null);
      this.onEnterFrame = Shared.Proxy.create(this,this.Init);
   }
   function __get__dataArray()
   {
      return this._DataArray;
   }
   function onInitDataComplete()
   {
      var _loc2_ = 0;
      var _loc3_ = 0;
      if(!this.IsValidCategoryObj(this._DataArray[0]))
      {
         _loc2_ = this.GetNextValidDataArray_Index(0);
      }
      _loc3_ = this.GetNextValidDataArray_Index(_loc2_);
      if(_loc2_ != 1.7976931348623157e308)
      {
         this._CurrScrollPage = _loc2_;
         this.ModCategoryList1.__set__dataObj(this._DataArray[_loc2_]);
      }
      if(_loc3_ != 1.7976931348623157e308)
      {
         this.ModCategoryList2.__set__dataObj(this._DataArray[_loc3_]);
      }
      this.AccountSettingsPage_mc.List_mc.focusEnabled = true;
      this.AccountSettingsPage_mc.List_mc.InvalidateData();
      this.ModCategoryList1.__set__selectedIndex(0);
      this.UpdateScrollMarkers();
      this._LoggedIn = true;
      this.BeginState(ModManager.MOD_CATEGORY_LIST_STATE);
   }
   function BackToLogin()
   {
      this.BeginState(ModManager.LOGIN_STATE);
   }
   function onDataCategoryUpdate(auiCategory)
   {
      if(auiCategory == this._CurrScrollPage)
      {
         if(this._DataArray[auiCategory].entries.length > 0)
         {
            this.ModCategoryList1.__set__dataObj(this._DataArray[auiCategory]);
         }
         else
         {
            var _loc3_ = this.GetNextValidDataArray_Index(auiCategory);
            var _loc5_ = this.GetNextValidDataArray_Index(_loc3_);
            if(_loc3_ != 1.7976931348623157e308)
            {
               this._CurrScrollPage = _loc3_;
               this.ModCategoryList1.__set__dataObj(this._DataArray[_loc3_]);
            }
            if(_loc5_ != 1.7976931348623157e308)
            {
               this.ModCategoryList2.__set__dataObj(this._DataArray[_loc5_]);
            }
         }
      }
      if(auiCategory == this.GetNextValidDataArray_Index(this._CurrScrollPage))
      {
         if(this._DataArray[auiCategory].entries.length > 0)
         {
            this.ModCategoryList2.__set__dataObj(this._DataArray[auiCategory]);
         }
         else
         {
            var _loc4_ = this.GetNextValidDataArray_Index(auiCategory);
            if(_loc4_ != 1.7976931348623157e308)
            {
               this.ModCategoryList2.__set__dataObj(this._DataArray[_loc4_]);
            }
         }
      }
   }
   function onDataObjectChange(aUpdatedObj)
   {
      switch(this.CurrentState)
      {
         case ModManager.MOD_CATEGORY_LIST_STATE:
            this.ModCategoryList1.onDataObjectChange(aUpdatedObj);
            this.ModCategoryList2.onDataObjectChange(aUpdatedObj);
            break;
         case ModManager.MOD_DETAIL_STATE:
            if(aUpdatedObj == this.DetailsPage_mc.__get__dataObj())
            {
               this.DetailsPage_mc.InvalidateData();
            }
            break;
         case ModManager.MOD_LIBRARY_STATE:
            this.LibraryPage_mc.onDataObjectChange(aUpdatedObj);
      }
   }
   function onLibraryArrayChange()
   {
      this.LibraryPage_mc.__get__dataArray().sort(this.sortModsFunc);
      this.LibraryPage_mc.InvalidateData();
      this.UpdateAccountSettingsList();
   }
   function ShowDetailsPageError(astrErrorText)
   {
      if(this.DetailsPage_mc._visible)
      {
         this.DetailsPage_mc.ShowError(astrErrorText);
      }
   }
   function onConfirmCloseManager()
   {
      this.LibraryPage_mc.ClearArray();
      if(!this.codeObj.attemptCloseManager())
      {
         this.BeginState(ModManager.LOGIN_STATE);
      }
   }
   function onModSearchReturned(resultCategory)
   {
   }
   function onVKBTextEntered(astrEnteredText)
   {
      if(this.CurrentState == ModManager.LOGIN_STATE)
      {
         this.LoginMenu.onVKBTextEntered(astrEnteredText);
      }
      else if(this.CurrentState == ModManager.MOD_SEARCH_STATE)
      {
         this.SearchPage_mc.onVKBTextEntered(astrEnteredText);
      }
   }
   function SetLibraryFreeSpace(aFreedSpace)
   {
      this.LibraryPage_mc.__set__freeSpace(aFreedSpace);
      this.LibraryPage_mc.InvalidateData();
   }
   function Init()
   {
      if(this._codeObjInitialized)
      {
         this.onEnterFrame = null;
         this.ModCategoryList1 = this.ListsHolder_mc.List1_mc;
         this.ModCategoryList1.addEventListener(gfx.events.EventTypes.ITEM_CLICK,Shared.Proxy.create(this,this.onModItemPress));
         this.ModCategoryList1.addEventListener(gfx.events.EventTypes.ITEM_ROLL_OVER,Shared.Proxy.create(this,this.onListRollover,this.ModCategoryList1));
         this.ModCategoryList1.addEventListener(ModCategoryList.PLAY_SOUND,Shared.Proxy.create(this,this.onPlaySound));
         this.ModCategoryList2 = this.ListsHolder_mc.List2_mc;
         this.ModCategoryList2.addEventListener(gfx.events.EventTypes.ITEM_CLICK,Shared.Proxy.create(this,this.onModItemPress));
         this.ModCategoryList2.addEventListener(gfx.events.EventTypes.ITEM_ROLL_OVER,Shared.Proxy.create(this,this.onListRollover,this.ModCategoryList2));
         this.ModCategoryList2.addEventListener(ModCategoryList.PLAY_SOUND,Shared.Proxy.create(this,this.onPlaySound));
         (MovieClip)this.ListsHolder_mc.ScrollUp.onRelease = Shared.Proxy.create(this,this.onScrollUpClick);
         (MovieClip)this.ListsHolder_mc.ScrollDown.onRelease = Shared.Proxy.create(this,this.onScrollDownClick);
         this.BottomButtons_mc.SetPlatform(this.iPlatform,this.PS3Switch);
         this.BottomButtons_mc.addEventListener(BottomButtons.BUTTON_CLICKED,Shared.Proxy.create(this,this.OnBottomButtonClicked));
         this.SearchPage_mc.__set__CodeObject(this.codeObj);
         this.LibraryPage_mc.addEventListener(ModLibraryPage.LIBRARY_ITEM_PRESSED,Shared.Proxy.create(this,this.OnLibraryItemPressed));
         this.CreateMockData();
         this.AccountSettingsPage_mc.List_mc.entryList = [{text:"$DeleteAllMods",key:ModManager.DELETE_ALL_MODS_OPTION},{text:"$DisableAllMods",key:ModManager.DISABLE_ALL_MODS_OPTION}];
         var _loc3_ = new Object();
         _loc3_.onLoadInit = Shared.Proxy.create(this,this.OnLoginLoadInit);
         var _loc2_ = new MovieClipLoader();
         _loc2_.addListener(_loc3_);
         _loc2_.loadClip("BethesdaNetLogin.swf",this.LoginHolder_mc);
      }
   }
   function CreateMockData()
   {
      var _loc2_ = undefined;
      var _loc4_ = 0;
      while(_loc4_ < 9)
      {
         _loc2_ = new Object();
         _loc2_.loaded = false;
         switch(_loc4_)
         {
            case 0:
               _loc2_.text = "$Need Updates";
               _loc2_.loaded = true;
               break;
            case 1:
               _loc2_.text = "$My Library";
               break;
            case 2:
               _loc2_.text = "$My Favorites";
               break;
            case 3:
               _loc2_.text = "$My Creations";
               break;
            case 4:
               _loc2_.text = "$MostPopularThisWeek";
               break;
            case 5:
               _loc2_.text = "$$MostPopularAllTime";
               break;
            case 6:
               _loc2_.text = "$HighestRated";
               break;
            case 7:
               _loc2_.text = "$MostFavorited";
               break;
            case 8:
               _loc2_.text = "$Latest";
         }
         _loc2_.entries = [];
         var _loc5_ = Math.ceil(Math.random() * 28);
         var _loc3_ = 0;
         while(_loc3_ < _loc5_)
         {
            _loc2_.entries.push(this.CreateMockValueObject(_loc3_,_loc2_.text));
            _loc3_ = _loc3_ + 1;
         }
         this._DataArray.push(_loc2_);
         _loc4_ = _loc4_ + 1;
      }
   }
   function CreateMockValueObject(index, category)
   {
      var _loc3_ = new Object();
      _loc3_.detailedImages = [];
      var _loc4_ = Math.ceil(Math.random() * 5);
      var _loc2_ = 0;
      while(_loc2_ < _loc4_)
      {
         _loc3_.detailedImages.push(this.screenshots[_loc2_]);
         _loc2_ = _loc2_ + 1;
      }
      _loc3_.text = category + " - Better hair " + index + ":" + _loc3_.detailedImages.length;
      _loc3_.author = "Bob Cook";
      _loc3_.description = this.texts[Math.floor(this.texts.length * Math.random())];
      _loc3_.contentId = "754816";
      _loc3_.imageTextureName = "754816";
      _loc3_.projectId = 4;
      _loc3_.branchId = 241;
      _loc3_.version = 3;
      _loc3_.fileSizeDisplay = Math.round(Math.random() * 10 * 1024 * 1024);
      _loc3_.rating = Math.random() * 5;
      _loc3_.ratingCount = Math.round(Math.random() * 1000);
      _loc3_.imageLoadState = 0;
      _loc3_.thumbnailURL = "https://mods.bethesda.net/content/cd9179eb-237e-4bfb-8ba0-5977995a581c_preview_754816_1529376565194536326_small.png";
      _loc3_.thumbnailSize = 52071;
      _loc3_.followed = Math.round(Math.random());
      _loc3_.downloaded = Math.round(Math.random());
      _loc3_.installed = Math.round(Math.random());
      return _loc3_;
   }
   function handleInput(details, pathToFocus)
   {
      var _loc3_ = false;
      if(Shared.GlobalFunc.IsKeyPressed(details))
      {
         _loc3_ = this.DoHandleInput(details.navEquivalent,details.code);
      }
      if(!_loc3_)
      {
         pathToFocus[0].handleInput(details,pathToFocus.slice(1));
      }
      return true;
   }
   function DoHandleInput(nav, keyCode)
   {
      var _loc3_ = false;
      if(this.CurrentState == ModManager.MOD_CATEGORY_LIST_STATE)
      {
         if(nav == gfx.ui.NavigationCode.ESCAPE || nav == gfx.ui.NavigationCode.GAMEPAD_B || nav == gfx.ui.NavigationCode.TAB || keyCode == 9)
         {
            this.onLoginCanceled();
            _loc3_ = true;
         }
         if(nav == gfx.ui.NavigationCode.UP)
         {
            this.scrollCategoryListUp();
            _loc3_ = true;
         }
         else if(nav == gfx.ui.NavigationCode.DOWN)
         {
            this.scrollCategoryListDown();
            _loc3_ = true;
         }
         if(nav == gfx.ui.NavigationCode.GAMEPAD_Y || keyCode == 84)
         {
            this.BeginState(ModManager.MOD_LIBRARY_STATE);
            _loc3_ = true;
         }
         if(nav == gfx.ui.NavigationCode.GAMEPAD_X || keyCode == 88)
         {
            this.BeginState(ModManager.MOD_SEARCH_STATE);
            _loc3_ = true;
         }
         if(nav == gfx.ui.NavigationCode.PAGE_UP || nav == gfx.ui.NavigationCode.GAMEPAD_L1)
         {
            if(Selection.getFocus() == targetPath(this.ModCategoryList1))
            {
               this.ModCategoryList1.JumpLeft();
            }
            else if(Selection.getFocus() == targetPath(this.ModCategoryList2))
            {
               this.ModCategoryList2.JumpLeft();
            }
         }
         if(nav == gfx.ui.NavigationCode.PAGE_DOWN || nav == gfx.ui.NavigationCode.GAMEPAD_R1)
         {
            if(Selection.getFocus() == targetPath(this.ModCategoryList1))
            {
               this.ModCategoryList1.JumpRight();
            }
            else if(Selection.getFocus() == targetPath(this.ModCategoryList2))
            {
               this.ModCategoryList2.JumpRight();
            }
         }
      }
      else if(this.CurrentState == ModManager.MOD_DETAIL_STATE)
      {
         if(nav == gfx.ui.NavigationCode.ESCAPE || nav == gfx.ui.NavigationCode.GAMEPAD_B || nav == gfx.ui.NavigationCode.TAB || keyCode == 9)
         {
            this.BeginState(ModManager.MOD_CATEGORY_LIST_STATE);
            _loc3_ = true;
         }
      }
      else if(this.CurrentState == ModManager.MOD_LIBRARY_STATE)
      {
         if(nav == gfx.ui.NavigationCode.ESCAPE || nav == gfx.ui.NavigationCode.GAMEPAD_B || nav == gfx.ui.NavigationCode.TAB || keyCode == 9)
         {
            if(this._LoggedIn)
            {
               this.BeginState(ModManager.MOD_CATEGORY_LIST_STATE);
            }
            else
            {
               this.BeginState(ModManager.LOGIN_STATE);
            }
            _loc3_ = true;
         }
         if(nav == gfx.ui.NavigationCode.GAMEPAD_A)
         {
            this.ToggleModActive(this.LibraryPage_mc.List_mc.__get__selectedEntry());
         }
         if(nav == gfx.ui.NavigationCode.GAMEPAD_Y || keyCode == 84)
         {
            this.DeleteLibraryMod(this.LibraryPage_mc.List_mc.__get__selectedEntry());
            _loc3_ = true;
         }
         if(nav == gfx.ui.NavigationCode.GAMEPAD_X || keyCode == 88)
         {
            this.onModReorderPressed();
            _loc3_ = true;
         }
         if(nav == gfx.ui.NavigationCode.GAMEPAD_BACK || keyCode == 86)
         {
            this.BeginState(ModManager.ACCOUNT_SETTINGS_STATE);
            _loc3_ = true;
         }
      }
      else if(this.CurrentState == ModManager.MOD_LIBRARY_REORDER_STATE)
      {
         if(nav == gfx.ui.NavigationCode.GAMEPAD_X || keyCode == 88)
         {
            this.onModReorderPressed();
            _loc3_ = true;
         }
      }
      else if(this.CurrentState == ModManager.MOD_SEARCH_STATE)
      {
         if(nav == gfx.ui.NavigationCode.ESCAPE || nav == gfx.ui.NavigationCode.GAMEPAD_B || nav == gfx.ui.NavigationCode.TAB || keyCode == 9)
         {
            this.BeginState(ModManager.MOD_CATEGORY_LIST_STATE);
            _loc3_ = true;
         }
         if(nav == gfx.ui.NavigationCode.ENTER || nav == gfx.ui.NavigationCode.GAMEPAD_A)
         {
            this.SearchPage_mc.SearchForMods();
            _loc3_ = true;
         }
      }
      else if(this.CurrentState == ModManager.ACCOUNT_SETTINGS_STATE)
      {
         if(nav == gfx.ui.NavigationCode.ESCAPE || nav == gfx.ui.NavigationCode.GAMEPAD_B || nav == gfx.ui.NavigationCode.TAB || keyCode == 9)
         {
            this.BeginState(ModManager.MOD_LIBRARY_STATE);
            _loc3_ = true;
         }
         if(nav == gfx.ui.NavigationCode.ENTER || nav == gfx.ui.NavigationCode.GAMEPAD_A)
         {
            if(this.AccountSettingsPage_mc.List_mc.selectedEntry.key == ModManager.DELETE_ALL_MODS_OPTION)
            {
               this.DeleteAllMods();
            }
            else if(this.AccountSettingsPage_mc.List_mc.selectedEntry.key == ModManager.DISABLE_ALL_MODS_OPTION)
            {
               this.DisableAllMods();
            }
            _loc3_ = true;
         }
      }
      else if(this.CurrentState == ModManager.LOGIN_STATE)
      {
         if(Selection.getFocus().indexOf(targetPath(this.LoginHolder_mc)) == -1)
         {
            if(nav == gfx.ui.NavigationCode.ESCAPE)
            {
               this.onLoginCanceled(null);
               _loc3_ = true;
            }
            if(nav == gfx.ui.NavigationCode.ENTER)
            {
               this.LoginMenu.AcceptLogin();
               _loc3_ = true;
            }
         }
         if(nav == gfx.ui.NavigationCode.GAMEPAD_Y || keyCode == 84)
         {
            this.BeginState(ModManager.MOD_LIBRARY_STATE);
            _loc3_ = true;
         }
      }
      return _loc3_;
   }
   function sortModsFunc(aModData1, aModData2)
   {
      var _loc1_ = 0;
      if(aModData1 != null && aModData2 == null)
      {
         _loc1_ = -1;
      }
      else if(aModData1 == null && aModData2 != null)
      {
         _loc1_ = 1;
      }
      if(aModData1 != null && aModData2 != null)
      {
         if(aModData1.checked == true && aModData2.checked != true)
         {
            _loc1_ = -1;
         }
         else if(aModData1.checked != true && aModData2.checked == true)
         {
            _loc1_ = 1;
         }
         if(_loc1_ == 0)
         {
            if(aModData1.sortIndex != undefined && aModData2.sortIndex == undefined)
            {
               _loc1_ = -1;
            }
            else if(aModData1.sortIndex == undefined && aModData2.sortIndex != undefined)
            {
               _loc1_ = 1;
            }
            else if(aModData1.sortIndex != undefined && aModData2.sortIndex != undefined)
            {
               if(aModData1.sortIndex < aModData2.sortIndex)
               {
                  _loc1_ = -1;
               }
               if(aModData1.sortIndex > aModData2.sortIndex)
               {
                  _loc1_ = 1;
               }
            }
         }
         if(_loc1_ == 0)
         {
            if(aModData1.text < aModData2.text)
            {
               _loc1_ = -1;
            }
            if(aModData1.text > aModData2.text)
            {
               _loc1_ = 1;
            }
         }
      }
      return _loc1_;
   }
   function InitExtensions()
   {
   }
   function onCodeObjectInit()
   {
      this._codeObjInitialized = true;
   }
   function ReleaseCodeObject()
   {
      this.LoginMenu.Destroy();
      this.SearchPage_mc.Destroy();
      delete this.codeObj;
      delete _root.CodeObj;
   }
   function SetPlatform(aiPlatform, abPS3Switch)
   {
      this.iPlatform = aiPlatform;
      this.PS3Switch = abPS3Switch;
      if(this._codeObjInitialized)
      {
         this.BottomButtons_mc.SetPlatform(aiPlatform,abPS3Switch);
      }
   }
   function OnRightStickInput(afXDelta, afYDelta)
   {
      if(this.CurrentState == ModManager.MOD_DETAIL_STATE)
      {
         this.DetailsPage_mc.OnRightStickInput(afXDelta,afYDelta);
      }
      else if(this.CurrentState == ModManager.MOD_LIBRARY_STATE)
      {
         this.LibraryPage_mc.OnRightStickInput(afXDelta,afYDelta);
      }
   }
   function OnLoginLoadInit(mc)
   {
      mc._visible = false;
      mc.onEnterFrame = Shared.Proxy.create(this,this.OnLoginLoadInitFinished,mc);
   }
   function OnLoginLoadInitFinished(mc)
   {
      if(this.LoginHolder_mc.LoginMenu_mc.Constructed)
      {
         this.LoginHolder_mc.onEnterFrame = null;
         this.LoginHolder_mc._visible = false;
         this.LoginMenu = this.LoginHolder_mc.LoginMenu_mc;
         this.LoginMenu.InitView();
         this.LoginMenu.__set__CodeObject(this.codeObj);
         this.LoginMenu.SetPlatform(this.iPlatform,this.PS3Switch);
         this.LoginMenu.SetBottomButtons(this.BottomButtons_mc);
         this.LoginMenu.addEventListener(BethesdaNetLogin.LOGIN_ACTIVATED,Shared.Proxy.create(this,this.onLoginActivated));
         this.LoginMenu.addEventListener(BethesdaNetLogin.LOGIN_CANCELED,Shared.Proxy.create(this,this.onLoginCanceled));
         this.codeObj.InitModManager(this,this.LoginMenu,this.LibraryPage_mc.__get__dataArray());
      }
   }
   function IsValidCategoryObj(aCategoryObj)
   {
      return aCategoryObj.entries instanceof Array && (aCategoryObj.entries.length > 0 || aCategoryObj.loaded == false);
   }
   function GetPrevValidDataArray_Index(aTargetIndex)
   {
      var _loc3_ = aTargetIndex;
      var _loc2_ = aTargetIndex - 1;
      while(_loc3_ == aTargetIndex && _loc2_ >= 0)
      {
         if(this.IsValidCategoryObj(this._DataArray[_loc2_]))
         {
            _loc3_ = _loc2_;
         }
         _loc2_ = _loc2_ - 1;
      }
      return _loc3_;
   }
   function GetNextValidDataArray_Index(aTargetIndex)
   {
      var _loc3_ = aTargetIndex;
      var _loc2_ = aTargetIndex + 1;
      while(_loc3_ == aTargetIndex && _loc2_ < this._DataArray.length)
      {
         if(this.IsValidCategoryObj(this._DataArray[_loc2_]))
         {
            _loc3_ = _loc2_;
         }
         _loc2_ = _loc2_ + 1;
      }
      return _loc3_;
   }
   function scrollCategoryListUp(abForceScroll)
   {
      if(abForceScroll == undefined)
      {
         abForceScroll = false;
      }
      if(!abForceScroll && Selection.getFocus() == targetPath(this.ModCategoryList2))
      {
         Selection.setFocus(this.ModCategoryList1);
         this.ModCategoryList1.__set__selectedIndex(this.ModCategoryList2.selectedIndex);
         this.ModCategoryList2.__set__selectedIndex(1.7976931348623157e308);
         this.codeObj.PlaySound("UIMenuFocus");
      }
      else
      {
         var _loc2_ = this.GetPrevValidDataArray_Index(this._CurrScrollPage);
         if(_loc2_ < this._CurrScrollPage)
         {
            this._CurrScrollPage = _loc2_;
            this.ModCategoryList2.__set__dataObj(this._DataArray[this._CurrScrollPage + 1]);
            this.ModCategoryList1.__set__dataObj(this._DataArray[this._CurrScrollPage]);
            this.codeObj.PlaySound("UIMenuFocus");
            this.ListsHolder_mc.gotoAndPlay("scrollUp");
         }
      }
      this.UpdateScrollMarkers();
   }
   function scrollCategoryListDown(abForceScroll)
   {
      if(abForceScroll == undefined)
      {
         abForceScroll = false;
      }
      if(!abForceScroll && Selection.getFocus() == targetPath(this.ModCategoryList1))
      {
         Selection.setFocus(this.ModCategoryList2);
         this.ModCategoryList2.__set__selectedIndex(this.ModCategoryList1.selectedIndex);
         this.ModCategoryList1.__set__selectedIndex(1.7976931348623157e308);
         this.codeObj.PlaySound("UIMenuFocus");
      }
      else
      {
         var _loc3_ = this.GetNextValidDataArray_Index(this._CurrScrollPage);
         var _loc2_ = this.GetNextValidDataArray_Index(_loc3_);
         if(_loc2_ > _loc3_)
         {
            this._CurrScrollPage = _loc3_;
            this.ModCategoryList1.__set__dataObj(this._DataArray[_loc2_ - 1]);
            this.ModCategoryList2.__set__dataObj(this._DataArray[_loc2_]);
            this.codeObj.PlaySound("UIMenuFocus");
            if(this._DataArray[_loc2_].loaded == false)
            {
               this.codeObj.StartCategoryRefresh(_loc2_);
            }
            this.ListsHolder_mc.gotoAndPlay("scrollDown");
         }
      }
      this.UpdateScrollMarkers();
   }
   function onScrollUpClick()
   {
      this.scrollCategoryListUp();
   }
   function onScrollDownClick()
   {
      this.scrollCategoryListDown();
   }
   function UpdateScrollMarkers()
   {
      var _loc2_ = this.GetNextValidDataArray_Index(this._CurrScrollPage);
      if(this.ListsHolder_mc.ScrollUp != null)
      {
         this.ListsHolder_mc.ScrollUp._visible = this.GetPrevValidDataArray_Index(this._CurrScrollPage) < this._CurrScrollPage;
      }
      if(this.ListsHolder_mc.ScrollDown != null)
      {
         this.ListsHolder_mc.ScrollDown._visible = this.GetNextValidDataArray_Index(_loc2_) > _loc2_;
      }
   }
   function onModItemPress(event)
   {
      if(event.target == this.ModCategoryList1)
      {
         this._StoredSelectedEntry = this.ModCategoryList1.selectedEntry;
      }
      else if(event.target == this.ModCategoryList2)
      {
         this._StoredSelectedEntry = this.ModCategoryList2.selectedEntry;
      }
      else
      {
         this._StoredSelectedEntry = undefined;
      }
      if(this.ListsHolder_mc._visible && this._StoredSelectedEntry != null)
      {
         this.BeginState(ModManager.MOD_DETAIL_STATE);
      }
   }
   function onPlaySound(event, scope)
   {
      this.codeObj.PlaySound(event.sound);
   }
   function onListRollover(event, scope)
   {
      if(scope == this.ModCategoryList1)
      {
         Selection.setFocus(this.ModCategoryList1);
         this.ModCategoryList1.__set__selectedIndex(this.ModCategoryList1.selectedIndex);
         this.ModCategoryList2.__set__selectedIndex(1.7976931348623157e308);
      }
      else if(scope == this.ModCategoryList2)
      {
         Selection.setFocus(this.ModCategoryList2);
         this.ModCategoryList2.__set__selectedIndex(this.ModCategoryList2.selectedIndex);
         this.ModCategoryList1.__set__selectedIndex(1.7976931348623157e308);
      }
   }
   function onLoginActivated(event)
   {
      this.BeginState(ModManager.LOGIN_STATE);
   }
   function onLoginCanceled(event)
   {
      this.codeObj.confirmCloseManager();
   }
   function onModReorderPressed()
   {
      this.LibraryPage_mc.__set__reorderMode(!this.LibraryPage_mc.__get__reorderMode());
      if(this.LibraryPage_mc.__get__reorderMode())
      {
         this.BeginState(ModManager.MOD_LIBRARY_REORDER_STATE);
      }
      else
      {
         this.BeginState(ModManager.MOD_LIBRARY_STATE);
      }
   }
   function UpdateAccountSettingsList()
   {
      if(this.AccountSettingsPage_mc.List_mc.entryList.length > 0)
      {
         this.AccountSettingsPage_mc.List_mc.entryList[0].disabled = this.LibraryPage_mc.__get__dataArray().length == 0;
         var _loc3_ = false;
         for(var _loc4_ in this.LibraryPage_mc.__get__dataArray())
         {
            var _loc2_ = this.LibraryPage_mc.__get__dataArray()[_loc4_];
            if(_loc2_.checked == true)
            {
               _loc3_ = true;
            }
         }
         this.AccountSettingsPage_mc.List_mc.entryList[1].disabled = !_loc3_;
         this.AccountSettingsPage_mc.List_mc.UpdateList();
      }
   }
   function onAccountSettingsPress(event)
   {
      switch(event.index)
      {
         case 0:
            this.DeleteAllMods();
            break;
         case 1:
            this.DisableAllMods();
      }
   }
   function DeleteAllMods()
   {
      this.codeObj.DeleteAllMods();
   }
   function DisableAllMods()
   {
      this.codeObj.DisableAllMods();
   }
   function BeginState(state)
   {
      if(this.CurrentState == state)
      {
         return undefined;
      }
      this.EndState(this.CurrentState);
      this.PreviousState = this.CurrentState;
      this.CurrentState = state;
      switch(this.CurrentState)
      {
         case ModManager.LOGIN_STATE:
            this.DisplayScreen(this.LoginHolder_mc);
            this.LoginMenu.ShowLoginScreen("");
            break;
         case ModManager.MOD_CATEGORY_LIST_STATE:
            this.DisplayScreen(this.ListsHolder_mc);
            if(this.ModCategoryList2.__get__selectedIndex() == 1.7976931348623157e308)
            {
               Selection.setFocus(this.ModCategoryList1);
            }
            else
            {
               Selection.setFocus(this.ModCategoryList2);
            }
            this.BottomButtons_mc.SetButtons([BottomButtons.DETAILS,BottomButtons.LIBRARY,BottomButtons.SEARCH,BottomButtons.CANCEL]);
            break;
         case ModManager.MOD_DETAIL_STATE:
            if(this._StoredSelectedEntry != null)
            {
               this.DetailsPage_mc.__set__dataObj(this._StoredSelectedEntry);
               this.DetailsPage_mc.OptionsList_mc.__set__selectedIndex(0);
               this.DisplayScreen(this.DetailsPage_mc);
               Selection.setFocus(this.DetailsPage_mc);
               this.BottomButtons_mc.SetButtons([BottomButtons.CONFIRM,BottomButtons.CANCEL]);
            }
            break;
         case ModManager.MOD_LIBRARY_STATE:
            if(this.LibraryPage_mc.List_mc.__get__entryList().length == 0)
            {
               this.BottomButtons_mc.SetButtons([BottomButtons.OPTIONS,BottomButtons.CANCEL]);
            }
            else
            {
               this.BottomButtons_mc.SetButtons([BottomButtons.ENABLE_MOD,BottomButtons.REORDER_MOD,BottomButtons.DELETE_MOD,BottomButtons.OPTIONS,BottomButtons.CANCEL]);
            }
            if(this.PreviousState != ModManager.MOD_LIBRARY_REORDER_STATE)
            {
               this.LibraryPage_mc.SetBottomButtons(this.BottomButtons_mc);
               this.LibraryPage_mc.List_mc.__set__selectedIndex(0);
               this.DisplayScreen(this.LibraryPage_mc);
            }
            Selection.setFocus(this.LibraryPage_mc);
            break;
         case ModManager.MOD_LIBRARY_REORDER_STATE:
            this.BottomButtons_mc.SetButtons([BottomButtons.DONE_REORDER_MOD]);
            this.DisplayScreen(this.LibraryPage_mc);
            break;
         case ModManager.MOD_SEARCH_STATE:
            this.DisplayScreen(this.SearchPage_mc);
            Selection.setFocus(this.SearchPage_mc.Input_tf);
            this.BottomButtons_mc.SetButtons([BottomButtons.SEARCH_CONFIRM,BottomButtons.CANCEL]);
            if(this.iPlatform == 0 || this.iPlatform == 1)
            {
               this.codeObj.startEditText();
            }
            break;
         case ModManager.ACCOUNT_SETTINGS_STATE:
            this.UpdateAccountSettingsList();
            this.AccountSettingsPage_mc.List_mc.selectedIndex = 0;
            this.AccountSettingsPage_mc.List_mc.addEventListener("itemPress",Shared.Proxy.create(this,this.onAccountSettingsPress));
            this.DisplayScreen(this.AccountSettingsPage_mc);
            this.AccountSettingsPage_mc.List_mc.focusEnabled = true;
            Selection.setFocus(this.AccountSettingsPage_mc.List_mc);
            this.BottomButtons_mc.SetButtons([BottomButtons.CANCEL]);
      }
   }
   function EndState()
   {
      switch(this.CurrentState)
      {
         case ModManager.LOGIN_STATE:
            this.LoginMenu.HideLoginScreen();
            break;
         case ModManager.MOD_CATEGORY_LIST_STATE:
            break;
         case ModManager.MOD_DETAIL_STATE:
            if(this.LibraryPage_mc.__get__stateChanged())
            {
               this.LibraryPage_mc.__set__stateChanged(false);
            }
            if(!this.DetailsPage_mc.__get__hasRatingChanged())
            {
            }
            this._StoredSelectedEntry = null;
            this.DetailsPage_mc.__set__dataObj(null);
            break;
         case ModManager.MOD_LIBRARY_STATE:
            if(this.LibraryPage_mc.__get__stateChanged())
            {
               this.LibraryPage_mc.__set__stateChanged(false);
            }
            break;
         case ModManager.MOD_LIBRARY_REORDER_STATE:
            this.codeObj.onLibraryReorderComplete();
            break;
         case ModManager.MOD_SEARCH_STATE:
            this.codeObj.endEditText();
            break;
         case ModManager.ACCOUNT_SETTINGS_STATE:
            this.AccountSettingsPage_mc.List_mc.removeAllEventListeners("itemPress");
            this.codeObj.PlaySound("UIMenuCancel");
      }
   }
   function DisplayScreen(mc)
   {
      this.ListsHolder_mc._visible = this.ListsHolder_mc == mc;
      this.LoginHolder_mc._visible = this.LoginHolder_mc == mc;
      this.DetailsPage_mc._visible = this.DetailsPage_mc == mc;
      this.LibraryPage_mc._visible = this.LibraryPage_mc == mc;
      this.AccountSettingsPage_mc._visible = this.AccountSettingsPage_mc == mc;
      this.SearchPage_mc._visible = this.SearchPage_mc == mc;
   }
   function OnBottomButtonClicked(event)
   {
      var _loc2_ = event.data;
      this.DoHandleInput(null,_loc2_.KeyCode);
   }
   function OnLibraryItemPressed(event)
   {
      var _loc2_ = event.data;
      this.ToggleModActive(_loc2_);
   }
   function ToggleModActive(data)
   {
      if(data.dataObj instanceof Object)
      {
         if(data.checked)
         {
            this.codeObj.UninstallMod(data);
         }
         else
         {
            this.codeObj.InstallMod(data);
         }
      }
      else if(data.checked)
      {
         this.codeObj.UninstallMod_NonBnet(data);
      }
      else
      {
         this.codeObj.InstallMod_NonBnet(data);
      }
   }
   function DeleteLibraryMod(data)
   {
      this.codeObj.DeleteModFromLibrary(data);
   }
}
