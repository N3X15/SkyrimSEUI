class ContainerMenu extends ItemMenu
{
   var bPCControlsReady = true;
   static var NULL_HAND = -1;
   static var RIGHT_HAND = 0;
   static var LEFT_HAND = 1;
   function ContainerMenu()
   {
      super();
      this.ContainerButtonArt = [{PCArt:"M1M2",XBoxArt:"360_LTRT",PS3Art:"PS3_LTRT"},{PCArt:"E",XBoxArt:"360_A",PS3Art:"PS3_A"},{PCArt:"R",XBoxArt:"360_X",PS3Art:"PS3_X"}];
      this.InventoryButtonArt = [{PCArt:"M1M2",XBoxArt:"360_LTRT",PS3Art:"PS3_LTRT"},{PCArt:"E",XBoxArt:"360_A",PS3Art:"PS3_A"},{PCArt:"R",XBoxArt:"360_X",PS3Art:"PS3_X"},{PCArt:"F",XBoxArt:"360_Y",PS3Art:"PS3_Y"}];
      this.bNPCMode = false;
      this.bShowEquipButtonHelp = true;
      this.iEquipHand = undefined;
   }
   function InitExtensions()
   {
      super.InitExtensions(false);
      gfx.io.GameDelegate.addCallBack("AttemptEquip",this,"AttemptEquip");
      gfx.io.GameDelegate.addCallBack("XButtonPress",this,"onXButtonPress");
      this.ItemCardFadeHolder_mc.StealTextInstance._visible = false;
      this.updateButtons();
   }
   function ShowItemsList()
   {
      this.InventoryLists_mc.ShowItemsList(false);
   }
   function handleInput(details, pathToFocus)
   {
      super.handleInput(details,pathToFocus);
      if(this.ShouldProcessItemsListInput(false))
      {
         if(this.iPlatform == 0 && details.code == 16)
         {
            this.bShowEquipButtonHelp = details.value != "keyUp";
            this.updateButtons();
         }
      }
      return true;
   }
   function onXButtonPress()
   {
      if(this.isViewingContainer() && !this.bNPCMode)
      {
         gfx.io.GameDelegate.call("TakeAllItems",[]);
      }
      else if(!this.isViewingContainer())
      {
         this.StartItemTransfer();
      }
   }
   function UpdateItemCardInfo(aUpdateObj)
   {
      super.UpdateItemCardInfo(aUpdateObj);
      this.updateButtons();
      if(aUpdateObj.pickpocketChance != undefined)
      {
         this.ItemCardFadeHolder_mc.StealTextInstance._visible = true;
         this.ItemCardFadeHolder_mc.StealTextInstance.PercentTextInstance.html = true;
         this.ItemCardFadeHolder_mc.StealTextInstance.PercentTextInstance.htmlText = "<font face=\'$EverywhereBoldFont\' size=\'24\' color=\'#FFFFFF\'>" + aUpdateObj.pickpocketChance + "%</font>" + (!this.isViewingContainer()?_root.TranslationBass.ToPlaceTextInstance.text:_root.TranslationBass.ToStealTextInstance.text);
      }
      else
      {
         this.ItemCardFadeHolder_mc.StealTextInstance._visible = false;
      }
   }
   function onShowItemsList(event)
   {
      this.iSelectedCategory = this.InventoryLists_mc.__get__CategoriesList().selectedIndex;
      this.updateButtons();
      super.onShowItemsList(event);
   }
   function onHideItemsList(event)
   {
      super.onHideItemsList(event);
      this.BottomBar_mc.UpdatePerItemInfo({type:InventoryDefines.ICT_NONE});
      this.updateButtons();
   }
   function updateButtons()
   {
      this.BottomBar_mc.SetButtonsArt(!this.isViewingContainer()?this.InventoryButtonArt:this.ContainerButtonArt);
      if(this.InventoryLists_mc.__get__currentState() != InventoryLists.TRANSITIONING_TO_TWO_PANELS && this.InventoryLists_mc.__get__currentState() != InventoryLists.TWO_PANELS)
      {
         this.BottomBar_mc.SetButtonText("",0);
         this.BottomBar_mc.SetButtonText("",1);
         this.BottomBar_mc.SetButtonText("",2);
         this.BottomBar_mc.SetButtonText("",3);
      }
      else
      {
         this.updateEquipButtonText();
         if(this.isViewingContainer())
         {
            this.BottomBar_mc.SetButtonText("$Take",1);
         }
         else
         {
            this.BottomBar_mc.SetButtonText(!!this.bShowEquipButtonHelp?"":InventoryDefines.GetEquipText(this.ItemCard_mc.__get__itemInfo().type),1);
         }
         if(!this.isViewingContainer())
         {
            if(this.bNPCMode)
            {
               this.BottomBar_mc.SetButtonText("$Give",2);
            }
            else
            {
               this.BottomBar_mc.SetButtonText("$Store",2);
            }
         }
         else if(this.bNPCMode)
         {
            this.BottomBar_mc.SetButtonText("",2);
         }
         else if(this.isViewingContainer())
         {
            this.BottomBar_mc.SetButtonText("$Take All",2);
         }
         this.updateFavoriteText();
      }
   }
   function onMouseRotationFastClick(aiMouseButton)
   {
      gfx.io.GameDelegate.call("CheckForMouseEquip",[aiMouseButton],this,"AttemptEquip");
   }
   function updateEquipButtonText()
   {
      this.BottomBar_mc.SetButtonText(!this.bShowEquipButtonHelp?"":InventoryDefines.GetEquipText(this.ItemCard_mc.__get__itemInfo().type),0);
   }
   function updateFavoriteText()
   {
      if(!this.isViewingContainer())
      {
         this.BottomBar_mc.SetButtonText(!this.ItemCard_mc.__get__itemInfo().favorite?"$Favorite":"$Unfavorite",3);
      }
      else
      {
         this.BottomBar_mc.SetButtonText("",3);
      }
   }
   function isViewingContainer()
   {
      var _loc2_ = this.InventoryLists_mc.__get__CategoriesList().__get__dividerIndex();
      return _loc2_ != undefined && this.iSelectedCategory < _loc2_;
   }
   function onQuantityMenuSelect(event)
   {
      if(this.iEquipHand != undefined)
      {
         gfx.io.GameDelegate.call("EquipItem",[this.iEquipHand,event.amount]);
         this.iEquipHand = undefined;
      }
      else if(this.InventoryLists_mc.__get__ItemsList().__get__selectedEntry().enabled)
      {
         gfx.io.GameDelegate.call("ItemTransfer",[event.amount,this.isViewingContainer()]);
      }
      else
      {
         gfx.io.GameDelegate.call("DisabledItemSelect",[]);
      }
   }
   function AttemptEquip(aiSlot, abCheckOverList)
   {
      var _loc2_ = abCheckOverList == undefined?true:abCheckOverList;
      if(this.ShouldProcessItemsListInput(_loc2_))
      {
         if(this.iPlatform == 0)
         {
            if(!this.isViewingContainer() || this.bShowEquipButtonHelp)
            {
               this.StartItemEquip(aiSlot);
            }
            else
            {
               this.StartItemTransfer();
            }
         }
         else
         {
            this.StartItemEquip(aiSlot);
         }
      }
   }
   function onItemSelect(event)
   {
      if(event.keyboardOrMouse != 0)
      {
         if(!this.isViewingContainer())
         {
            this.StartItemEquip(ContainerMenu.NULL_HAND);
         }
         else
         {
            this.StartItemTransfer();
         }
      }
   }
   function StartItemTransfer()
   {
      if(this.ItemCard_mc.__get__itemInfo().weight == 0 && this.isViewingContainer())
      {
         this.onQuantityMenuSelect({amount:this.InventoryLists_mc.__get__ItemsList().__get__selectedEntry().count});
      }
      else if(this.InventoryLists_mc.__get__ItemsList().__get__selectedEntry().count <= InventoryDefines.QUANTITY_MENU_COUNT_LIMIT)
      {
         this.onQuantityMenuSelect({amount:1});
      }
      else
      {
         this.ItemCard_mc.ShowQuantityMenu(this.InventoryLists_mc.__get__ItemsList().__get__selectedEntry().count);
      }
   }
   function StartItemEquip(aiEquipHand)
   {
      if(this.isViewingContainer())
      {
         this.iEquipHand = aiEquipHand;
         this.StartItemTransfer();
      }
      else
      {
         gfx.io.GameDelegate.call("EquipItem",[aiEquipHand]);
      }
   }
   function onItemCardSubMenuAction(event)
   {
      super.onItemCardSubMenuAction(event);
      if(event.menu == "quantity")
      {
         gfx.io.GameDelegate.call("QuantitySliderOpen",[event.opening]);
      }
   }
   function SetPlatform(aiPlatform, abPS3Switch)
   {
      super.SetPlatform(aiPlatform,abPS3Switch);
      this.iPlatform = aiPlatform;
      this.bShowEquipButtonHelp = aiPlatform != 0;
      this.updateButtons();
   }
}
