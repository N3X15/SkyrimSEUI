class Map.MapMarker extends gfx.controls.Button
{
   static var TopDepth = 0;
   static var IconTypes = new Array("EmptyMarker","CityMarker","TownMarker","SettlementMarker","CaveMarker","CampMarker","FortMarker","NordicRuinMarker","DwemerMarker","ShipwreckMarker","GroveMarker","LandmarkMarker","DragonlairMarker","FarmMarker","WoodMillMarker","MineMarker","ImperialCampMarker","StormcloakCampMarker","DoomstoneMarker","WheatMillMarker","SmelterMarker","StableMarker","ImperialTowerMarker","ClearingMarker","PassMarker","AltarMarker","RockMarker","LighthouseMarker","OrcStrongholdMarker","GiantCampMarker","ShackMarker","NordicTowerMarker","NordicDwellingMarker","DocksMarker","ShrineMarker","RiftenCastleMarker","RiftenCapitolMarker","WindhelmCastleMarker","WindhelmCapitolMarker","WhiterunCastleMarker","WhiterunCapitolMarker","SolitudeCastleMarker","SolitudeCapitolMarker","MarkarthCastleMarker","MarkarthCapitolMarker","WinterholdCastleMarker","WinterholdCapitolMarker","MorthalCastleMarker","MorthalCapitolMarker","FalkreathCastleMarker","FalkreathCapitolMarker","DawnstarCastleMarker","DawnstarCapitolMarker","DLC02MiraakTempleMarker","DLC02RavenRockMarker","DLC02StandingStonesMarker","DLC02TelvanniTowerMarker","DLC02ToSkyrimMarker","DLC02ToSolstheimMarker","DLC02CastleKarstaagMarker","","DoorMarker","QuestTargetMarker","QuestTargetDoorMarker","MultipleQuestTargetMarker","PlayerSetMarker","YouAreHereMarker");
   function MapMarker()
   {
      super();
      this.hitArea = this.HitAreaClip;
      this.textField = this.TextClip.MarkerNameField;
      this.textField.autoSize = "left";
      this.Index = -1;
      this.__set__disableFocus(true);
      this._FadingIn = false;
      this._FadingOut = false;
      this.stateMap.release = ["up"];
   }
   function configUI()
   {
      super.configUI();
      this.onRollOver = function()
      {
      };
      this.onRollOut = function()
      {
      };
   }
   function __get__FadingIn()
   {
      return this._FadingIn;
   }
   function __set__FadingIn(value)
   {
      if(value != this._FadingIn)
      {
         this._FadingIn = value;
         if(this._FadingIn)
         {
            this._visible = true;
            this.gotoAndPlay("fade_in");
         }
      }
      return this.__get__FadingIn();
   }
   function __get__FadingOut()
   {
      return this._FadingOut;
   }
   function __set__FadingOut(value)
   {
      if(value != this._FadingOut)
      {
         this._FadingOut = value;
         if(this._FadingOut)
         {
            this.gotoAndPlay("fade_out");
         }
      }
      return this.__get__FadingOut();
   }
   function setState(state)
   {
      if(!this._FadingOut && !this._FadingIn)
      {
         super.setState(state);
      }
   }
   function MarkerRollOver()
   {
      var _loc2_ = false;
      this.setState("over");
      _loc2_ = this.state == "over";
      if(_loc2_)
      {
         var _loc3_ = this._parent.getInstanceAtDepth(Map.MapMarker.TopDepth);
         if(undefined != _loc3_)
         {
            _loc3_.swapDepths((Map.MapMarker)_loc3_.Index);
         }
         this.swapDepths(Map.MapMarker.TopDepth);
         gfx.io.GameDelegate.call("PlaySound",["UIMapRollover"]);
      }
      return _loc2_;
   }
   function MarkerRollOut()
   {
      this.setState("out");
   }
   function MarkerClick()
   {
      gfx.io.GameDelegate.call("MarkerClick",[this.Index]);
   }
}
