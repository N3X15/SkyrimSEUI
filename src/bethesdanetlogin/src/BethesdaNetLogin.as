class BethesdaNetLogin extends MovieClip
{
   var constructed = false;
   static var CONTROLLER_PC = 0;
   static var CONTROLLER_PCGAMEPAD = 1;
   static var CONTROLLER_DURANGO = 2;
   static var CONTROLLER_ORBIS = 3;
   static var LOGIN_ACTIVATED = "BethesdaNetLogin.LoginActivated";
   static var LOGIN_CANCELED = "BethesdaNetLogin.LoginCanceled";
   function BethesdaNetLogin()
   {
      super();
      gfx.events.EventDispatcher.initialize(this);
      _global.gfxExtensions = true;
      Shared.GlobalFunc.MaintainTextFormat();
      this.constructed = true;
   }
   function __get__Constructed()
   {
      return this.constructed;
   }
   function __get__CodeObject()
   {
      return this.codeObj;
   }
   function __set__CodeObject(value)
   {
      this.codeObj = value;
      return this.__get__CodeObject();
   }
   function ShowLoginScreen(aUserName)
   {
      this.dispatchEvent({type:BethesdaNetLogin.LOGIN_ACTIVATED,target:this});
      this.DisplayScreen(this.LoginPage_mc);
      if(this.iPlatform == BethesdaNetLogin.CONTROLLER_PC || this.iPlatform == BethesdaNetLogin.CONTROLLER_PCGAMEPAD)
      {
         this.codeObj.startEditText();
      }
      this.LoginPage_mc.UsernameInput_tf.text = aUserName;
      this.LoginPage_mc.PasswordInput_tf.text = "";
      this.LoginPage_mc.PasswordInput_tf.onChanged = Shared.Proxy.create(this,this.OnPasswordFieldUpdate);
      this.LoginPage_mc.UsernameGrayText_tf._visible = this.LoginPage_mc.UsernameInput_tf.text.length <= 0;
      this.LoginPage_mc.PasswordGrayText_tf._visible = this.LoginPage_mc.PasswordInput_tf.text.length <= 0;
      Selection.setFocus(this.LoginPage_mc.UsernameInput_tf);
      this.bottomButtons.SetButtons([BottomButtons.ACCEPT,BottomButtons.CANCEL,BottomButtons.LIBRARY]);
   }
   function HideLoginScreen()
   {
      this.codeObj.endEditText();
      this.LoginPage_mc._visible = false;
   }
   function ShowLoginScreen_AfterFailure(strErrorString)
   {
      this.dispatchEvent({type:BethesdaNetLogin.LOGIN_ACTIVATED,target:this});
      this.LoginPage_mc.Error_tf.SetText("$LoginError_" + strErrorString,false);
      this.ShowLoginScreen(this.LoginPage_mc.UsernameInput_tf.text);
   }
   function ShowSpinner(aMessageText)
   {
      this.dispatchEvent({type:BethesdaNetLogin.LOGIN_ACTIVATED,target:this});
      this.DisplayScreen(this.Spinner_mc);
      this.Spinner_mc.textField.SetText(aMessageText,false);
      Selection.setFocus(undefined);
   }
   function HideSpinner()
   {
      this.Spinner_mc._visible = false;
   }
   function onVKBTextEntered(astrEnteredText)
   {
      if(astrEnteredText.length > 0)
      {
         if(Selection.getFocus() == targetPath(this.LoginPage_mc.UsernameInput_tf))
         {
            this.LoginPage_mc.UsernameInput_tf.SetText(astrEnteredText,false);
            Selection.setFocus(this.LoginPage_mc.PasswordInput_tf);
            this.LoginPage_mc.PasswordGrayText_tf._visible = false;
         }
         else if(Selection.getFocus() == targetPath(this.LoginPage_mc.PasswordInput_tf))
         {
            this.LoginPage_mc.PasswordInput_tf.SetText(astrEnteredText,false);
         }
      }
      if(this.LoginPage_mc.UsernameInput_tf.text.length > 0)
      {
         this.LoginPage_mc.UsernameGrayText_tf._visible = false;
      }
      if(this.LoginPage_mc.PasswordInput_tf.text.length > 0)
      {
         this.LoginPage_mc.PasswordGrayText_tf._visible = false;
      }
   }
   function ShowNewAccountPage(strErrorText)
   {
      this.dispatchEvent({type:BethesdaNetLogin.LOGIN_ACTIVATED,target:this});
      this.DisplayScreen(this.NewAccountPage_mc);
      this.NewAccountPage_mc.UsernameGrayText_tf._visible = this.NewAccountPage_mc.NewUsernameInput_tf.text.length <= 0;
      this.NewAccountPage_mc.EmailGrayText_tf._visible = this.NewAccountPage_mc.NewEmailInput_tf.text.length <= 0;
      this.codeObj.startEditText();
      if(this.NewAccountPage_mc.NewUsernameInput_tf.text.length > 0)
      {
         this.NewAccountPage_mc.NewUsernameInput_tf.setSelection(0,this.NewAccountPage_mc.NewUsernameInput_tf.text.length);
      }
      if(strErrorText != null)
      {
         this.NewAccountPage_mc.Error_tf.SetText("$CreateAcct_" + strErrorText,false);
      }
      else
      {
         this.NewAccountPage_mc.Error_tf.SetText(" ",false);
      }
      this.bottomButtons.SetButtons([BottomButtons.ACCEPT,BottomButtons.CANCEL]);
   }
   function InitView()
   {
      this.CoppaCheckbox = this.NewAccountPage_mc.CoppaCheckbox;
      this.NewsOptInCheckbox = this.NewAccountPage_mc.NewsOptInCheckbox;
      this.CoppaCheckbox.__set__text("$CoppaText");
      this.CoppaCheckbox.textField.autoSize = true;
      this.NewsOptInCheckbox.__set__text("$NewsOptInMessage");
      this.NewsOptInCheckbox._y = this.CoppaCheckbox._y + this.CoppaCheckbox._height + 0;
      this.DisplayScreen(null);
   }
   function Destroy()
   {
      return delete this.codeObj;
   }
   function SetBottomButtons(buttons)
   {
      this.bottomButtons = buttons;
      this.bottomButtons.addEventListener(BottomButtons.BUTTON_CLICKED,Shared.Proxy.create(this,this.OnBottomButtonClicked));
   }
   function SetPlatform(aiPlatform, abPS3Switch)
   {
      this.iPlatform = aiPlatform;
      this.ps3Switch = abPS3Switch;
   }
   function ShowEULAScreen()
   {
      this.codeObj.PopulateEULA(this.EULAPagesA);
      this._CurrEULAIndex = 0;
      this.LoadEULAPage();
      this.EULAPage_mc._visible = true;
   }
   function handleInput(details, pathToFocus)
   {
      if(Shared.GlobalFunc.IsKeyPressed(details))
      {
         switch(details.navEquivalent)
         {
            case gfx.ui.NavigationCode.UP:
               this.handleUpInput();
               break;
            case gfx.ui.NavigationCode.DOWN:
               this.handleDownInput();
               break;
            case gfx.ui.NavigationCode.TAB:
               if(this.iPlatform == BethesdaNetLogin.CONTROLLER_PC)
               {
                  this.handleTabInput();
               }
               else
               {
                  this.dispatchEvent({type:BethesdaNetLogin.LOGIN_CANCELED,target:this});
               }
               break;
            case gfx.ui.NavigationCode.ENTER:
               this.onLoginAccept();
               break;
            case gfx.ui.NavigationCode.ESCAPE:
               this.dispatchEvent({type:BethesdaNetLogin.LOGIN_CANCELED,target:this});
         }
      }
      return true;
   }
   function AcceptLogin()
   {
      this.onLoginAccept();
   }
   function LoadEULAPage()
   {
      this.EULAPage_mc.Title_tf.SetText(this.EULAPagesA[this._CurrEULAIndex].title,true);
      this.EULAPage_mc.EULA_tf.SetText(this.EULAPagesA[this._CurrEULAIndex].text,true);
      this.EULAPage_mc.EULA_tf.scroll = 0;
   }
   function onLoginAccept()
   {
      if(this.LoginPage_mc._visible)
      {
         if(this.LoginPage_mc.UsernameInput_tf.text.length > 0 && this.LoginPage_mc.PasswordInput_tf.text.length > 0)
         {
            this.HideLoginScreen();
            this.codeObj.attemptLogin(this.LoginPage_mc.UsernameInput_tf.text,this.LoginPage_mc.PasswordInput_tf.text);
         }
         else if(this.iPlatform != BethesdaNetLogin.CONTROLLER_PC && this.iPlatform != BethesdaNetLogin.CONTROLLER_PCGAMEPAD)
         {
            if(Selection.getFocus() == targetPath(this.LoginPage_mc.UsernameInput_tf))
            {
               this.LoginPage_mc.UsernameGrayText_tf._visible = false;
               this.codeObj.startEditText(this.LoginPage_mc.UsernameInput_tf.text.length <= 0?"":this.LoginPage_mc.UsernameInput_tf.text);
            }
            else if(Selection.getFocus() == targetPath(this.LoginPage_mc.PasswordInput_tf))
            {
               this.LoginPage_mc.PasswordGrayText_tf._visible = false;
               this.codeObj.startEditText("");
            }
         }
      }
      else if(this.EULAPage_mc._visible)
      {
         this.codeObj.AcceptLegalDoc(this.EULAPagesA[this._CurrEULAIndex].id);
         if(this._CurrEULAIndex < this.EULAPagesA.length - 1)
         {
            this._CurrEULAIndex = this._CurrEULAIndex + 1;
            this.LoadEULAPage();
         }
         else
         {
            this.HideEULAScreen();
            this.ShowNewAccountPage();
         }
      }
      else if(this.NewAccountPage_mc._visible)
      {
         if(this.NewAccountPage_mc.NewUsernameInput_tf.text.length > 0 && this.NewAccountPage_mc.NewEmailInput_tf.text.length > 0)
         {
            this.HideNewAccountPage();
            this.codeObj.createQuickAccount(this.NewAccountPage_mc.NewUsernameInput_tf.text,this.NewAccountPage_mc.NewEmailInput_tf.text,this.CoppaCheckbox.__get__checked(),this.NewsOptInCheckbox.__get__checked());
         }
      }
   }
   function HideEULAScreen()
   {
      Selection.setFocus(null);
      this.EULAPage_mc._visible = false;
   }
   function HideNewAccountPage()
   {
      this.codeObj.endEditText();
      this.NewAccountPage_mc._visible = false;
      Selection.setFocus(null);
   }
   function handleTabInput()
   {
      if(this.LoginPage_mc._visible)
      {
         if(Selection.getFocus() == targetPath(this.LoginPage_mc.UsernameInput_tf))
         {
            Selection.setFocus(this.LoginPage_mc.PasswordInput_tf);
         }
         else if(Selection.getFocus() == targetPath(this.LoginPage_mc.PasswordInput_tf))
         {
            Selection.setFocus(this.LoginPage_mc.UsernameInput_tf);
         }
      }
   }
   function handleUpInput()
   {
      if(this.LoginPage_mc._visible && this.iPlatform != BethesdaNetLogin.CONTROLLER_PC)
      {
         if(Selection.getFocus() == targetPath(this.LoginPage_mc.PasswordInput_tf))
         {
            Selection.setFocus(this.LoginPage_mc.UsernameInput_tf);
         }
      }
   }
   function handleDownInput()
   {
      if(this.LoginPage_mc._visible && this.iPlatform != BethesdaNetLogin.CONTROLLER_PC)
      {
         if(Selection.getFocus() == targetPath(this.LoginPage_mc.UsernameInput_tf))
         {
            Selection.setFocus(this.LoginPage_mc.PasswordInput_tf);
         }
      }
   }
   function DisplayScreen(mc)
   {
      this.NewAccountPage_mc._visible = this.NewAccountPage_mc == mc;
      this.LoginPage_mc._visible = this.LoginPage_mc == mc;
      this.EULAPage_mc._visible = this.EULAPage_mc == mc;
      this.Spinner_mc._visible = this.Spinner_mc == mc;
   }
   function OnPasswordFieldUpdate()
   {
      this.LoginPage_mc.PasswordGrayText_tf._visible = this.LoginPage_mc.PasswordInput_tf.text.length <= 0;
   }
   function OnBottomButtonClicked(event)
   {
      var _loc2_ = event.data.KeyCode;
      if(_loc2_ == 13)
      {
         this.onLoginAccept();
      }
      if(_loc2_ == 9)
      {
         this.dispatchEvent({type:BethesdaNetLogin.LOGIN_CANCELED,target:this});
      }
   }
}
