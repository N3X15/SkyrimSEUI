class gfx.events.EventTypes
{
   static var CLICK = "click";
   static var DOUBLE_CLICK = "doubleClick";
   static var ROLL_OVER = "rollOver";
   static var PRESS = "press";
   static var ROLL_OUT = "rollOut";
   static var RELEASE_OUTSIDE = "releaseOutside";
   static var ITEM_CLICK = "itemClick";
   static var ITEM_DOUBLE_CLICK = "itemDoubleClick";
   static var ITEM_ROLL_OVER = "itemRollOver";
   static var ITEM_PRESS = "itemPress";
   static var ITEM_ROLL_OUT = "itemRollOut";
   static var FOCUS_IN = "focusIn";
   static var FOCUS_OUT = "focusOut";
   static var SHOW = "show";
   static var HIDE = "hide";
   static var INIT = "init";
   static var CHANGE = "change";
   static var STATE_CHANGE = "stateChange";
   static var SELECT = "select";
   static var IO_ERROR = "ioError";
   static var PROGRESS = "progress";
   static var COMPLETE = "complete";
   static var SCROLL = "scroll";
   static var TEXT_CHANGE = "textChange";
   static var INPUT = "input";
   static var DRAG_BEGIN = "dragBegin";
   static var DRAG_END = "dragEnd";
   static var DROP = "drop";
   function EventTypes()
   {
   }
}
