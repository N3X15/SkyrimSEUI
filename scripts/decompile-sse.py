'''
decompile-sse.py

Decompiles Skyrim and Skyrim SE UI SWFs, and organizes the code.

(c)2016 Rob "N3X15" Nelson <nexisentertainment@gmail.com>

MIT License
'''
import codecs
import os
import shutil
import sys
import tempfile

import toml
import yaml
from lxml import etree

script_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

sys.path.append(os.path.join(script_dir, 'lib', 'python-build-tools'))
sys.path.append(os.path.join(script_dir, 'lib', 'elderas3'))

from buildtools import log, os_utils, utils
from buildtools.config import YAMLConfig
from elderas3.flashdevelop import FlashDevelopProject


CONFIG = None
PATHS = {}
FILECFG = {}
IMPORT_OVERRIDES = {}
COMMON_RELDIR_NAMES = []
COMMON_PACKAGE_PATHS = []
SHARED_LIBRARIES = {}
SHARED_PACKAGES = {}
BASE_PATH = os.path.abspath('.')
ADDED_FILES = []
HASHES = {}
PROJECTINFO = []


def formatNSPrefix(prefix):
    return '{{{}}}'.format(prefix)


def xKey(key):
    if key is None:
        return 'x'
    return key
XFL_NSMAP = {
    None: "http://ns.adobe.com/xfl/2008/",
    'xsi': "http://www.w3.org/2001/XMLSchema-instance"
}
XFL_XNSMAP = {xKey(k): v for k, v in XFL_NSMAP.items()}
XFL_DEFAULT_NS = formatNSPrefix(XFL_NSMAP[None])
XFL_XSI_NS = formatNSPrefix(XFL_NSMAP['xsi'])


def ParseAsEtree(path):
    xml = None
    strippingParser = etree.XMLParser(remove_blank_text=True)
    with codecs.open(path, 'r', encoding='utf-8-sig') as f:
        xml = etree.parse(f, strippingParser)
    return xml


class XPathProcessor(object):

    def __init__(self, xml, xpath_expr, nsmap, handlers=[]):
        self.xml = xml
        self.expr = xpath_expr
        self.nsmap = nsmap
        self.handlers = handlers

    def Execute(self):
        changes = 0
        nodes = 0
        for node in self.xml.xpath(self.expr, namespaces=self.nsmap):
            nodes += 1
            changes += self.ProcessSingleNode(node)
        return changes

    def ProcessSingleNode(self, node):
        changes = 0
        for handler in self.handlers:
            handler.changes = 0
            handler.OnNode(node)
            changes += handler.changes
        return changes


class NodeIterationHandler(object):

    def __init__(self):
        self.changes = 0

    def OnNode(self, node):
        pass


class RemoveAttribute(NodeIterationHandler):

    def __init__(self, attribNames):
        super(RemoveAttribute, self).__init__()
        self.attribNames = attribNames

    def OnNode(self, node):
        for attribName in self.attribNames:
            if attribName in node.attrib:
                # include.attrib['lastModified']='0' # Honk
                node.attrib.pop(attribName)
                self.changes += 1


class FixBitmapDataHRef(NodeIterationHandler):

    def __init__(self):
        super(FixBitmapDataHRef, self).__init__()

    def OnNode(self, node):
        if 'name' not in node.attrib:
            return
        basename = os.path.splitext(node.attrib['name'])[0]
        node.attrib['bitmapDataHRef'] = ' '.join(node.attrib['bitmapDataHRef'].split(' ')[:-1] + [basename + '.dat'])


def fixDOMDocument(path):
    xml = None
    with log.info('Fixing DOMDocument.xml...'):
        xml = ParseAsEtree(path)
        #<Include href="Symbol 8.xml" loadImmediate="false" lastModified="1478114396"/>
        proc = XPathProcessor(xml, '//x:Include', nsmap=XFL_XNSMAP)
        proc.handlers += [RemoveAttribute(['lastModified'])]
        changes = proc.Execute()
        #<DOMBitmapItem name="bitmap62.png" sourceLastImported="1478551399" externalFileSize="3073" allowSmoothing="true" useImportedJPEGData="false" compressionType="lossless" originalCompressionType="lossless" quality="50" href="bitmap62.png" bitmapDataHRef="M 3 1478551399.dat" frameRight="193" frameBottom="33"/>
        proc = XPathProcessor(xml, '//x:DOMBitmapItem', nsmap=XFL_XNSMAP)
        proc.handlers += [RemoveAttribute(['sourceLastImported'])]
        proc.handlers += [FixBitmapDataHRef()]
        changes += proc.Execute()
        log.info('Made %d changes.', changes)
    with codecs.open(path, 'w', encoding='utf-8') as f:
        xml.write(f, pretty_print=True)


def fixSymbol(path):
    xml = None
    #log.info('Fixing %s...',path)
    xml = ParseAsEtree(path)
    #<DOMSymbolItem xmlns="http://ns.adobe.com/xfl/2008/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" name="Symbol 3" lastModified="1478114396">
    root = xml.getroot()
    changes = XPathProcessor(xml, '/', nsmap=XFL_XNSMAP, handlers=[RemoveAttribute(['lastModified'])]).ProcessSingleNode(root)
    with codecs.open(path, 'w', encoding='utf-8') as f:
        xml.write(f, pretty_print=True)
    return changes


def removeEmptyFolders(path, removeRoot=True):
    'Function to remove empty folders'
    if not os.path.isdir(path):
        return

    # remove empty subfolders
    files = os.listdir(path)
    if len(files):
        for f in files:
            fullpath = os.path.join(path, f)
            if os.path.isdir(fullpath):
                removeEmptyFolders(fullpath)

    # if folder empty, delete it
    files = os.listdir(path)
    if len(files) == 0 and removeRoot:
        # print "Removing empty folder:", path
        os.rmdir(path)


def DecompileSWF(filename, destination, dependencies={}, options={}, filecfg={}):
    tmpdir = tempfile.mkdtemp('skyseui')
    os_utils.safe_rmtree(destination)
    os_utils.ensureDirExists(tmpdir)
    success = False
    projectName,_=os.path.splitext(os.path.basename(filename))
    with os_utils.Chdir(tmpdir):
        with log.info('Copying dependencies...'):
            for dest, dep in dependencies.items():
                finaldest = os.path.join(tmpdir, dest)
                log.info('%s -> %s', dep, finaldest)
                os_utils.ensureDirExists(os.path.dirname(finaldest))
                os_utils.single_copy(dep, finaldest, as_file=True, verbose=False)
        with log.info('Decompiling %s...', os.path.basename(filename)):
            cmd = [
                'java', '-jar',
                PATHS['ffdec'],
                '-cli',
                '-export', 'xfl', destination, filename,  # We use XFL because it's easier to get to. FLA is the same, but compressed as a ZIP.
            ]
            if len(options) > 0:
                cmd += ['-config']
                opts = []
                for k, v in options.items():
                    opts += ['{}={}'.format(k, v)]
                cmd += [','.join(opts)]
            success = os_utils.cmd(cmd, critical=True, echo=True, show_output=True)
    with log.info('Fixing paths...'):
        # AS3PackagePaths=;
        for root, _, files in os.walk(destination):
            for filename in files:
                if not filename.endswith('.as'):
                    continue
                absfilename = os.path.abspath(os.path.join(root, filename))
                filenameonly = os.path.basename(absfilename)
                relfilename = os.path.relpath(absfilename, destination)
                reldir = os.path.dirname(relfilename)
                #destdir = os.path.join(destination, reldir)
                finalfile = ''
                relfinal = ''
                destdir = os.path.join(destination, 'src', reldir)
                if destdir != os.path.dirname(absfilename):
                    os_utils.ensureDirExists(destdir)
                    os_utils.single_copy(absfilename, destdir, verbose=True)
                    os.remove(absfilename)
                    finalfile = os.path.join(destdir, filenameonly)
                else:
                    finalfile = absfilename

                if relfilename.replace('\\', '/') in filecfg:
                    FILECFG[finalfile] = filecfg[relfilename]
                if os.path.join('src', 'COMMON') in finalfile:
                    continue
                if FILECFG.get(finalfile, {}).get('skip-dupe-check', False):
                    continue
                md5 = utils.md5sum(finalfile)  # Yes, MD5 has been cracked.  We're not hashing a password, so this is fine.
                if md5 not in HASHES:
                    HASHES[md5] = []
                HASHES[md5].append((finalfile, relfilename))
    with log.info('Stripping modification timestamps...'):
        changes = 0
        symbols = 0
        for root, _, files in os.walk(destination):
            for filename in files:
                abspath = os.path.abspath(os.path.join(root, filename))
                if filename.endswith('DOMDocument.xml'):
                    fixDOMDocument(abspath)
                elif filename.endswith('.xml'):
                    changes += fixSymbol(abspath)
                    symbols += 1
        log.info('Made %d changes in %d symbol files', changes, symbols)
    with log.info('Correcting XFL PublishSettings...'):
        xml = None
        with codecs.open(os.path.join(destination, 'PublishSettings.xml'), 'r', encoding='utf-8-sig') as f:
            xml = etree.parse(f)
        # As of CS6:
        # <AS3PackagePaths>src;COMMON</AS3PackagePaths>
        for result in xml.xpath("//AS3PackagePaths"):
            result.text = ';'.join(['src'] + COMMON_PACKAGE_PATHS)
        with codecs.open(os.path.join(destination, 'PublishSettings.xml'), 'w', encoding='utf-8') as f:
            xml.write(f, pretty_print=True)
        log.info('DONE!')
    PROJECTINFO.append((projectName, os.path.abspath(destination), dependencies, options))
    with log.info('Cleaning up...'):
        os_utils.safe_rmtree(tmpdir)
        #removeEmptyFolders(destination, False)

    return success


def WriteFlashDevelopProject(info):
    global ADDED_FILES
    projectName, destination, _, options = info
    targets = []

    for root, _, files in os.walk(destination):
        for filename in files:
            targetpath = os.path.relpath(os.path.join(root, filename), destination)
            #print(filename)
            if filename.endswith('.as'):
                #print(filename)
                targets.append(targetpath)
    as3proj=os.path.join(destination,projectName + '.as3proj')
    with log.info('Writing FlashDevelop project for %s...', as3proj):
        #log.info(repr(targets))
        fdp = FlashDevelopProject()
        fdp.output.outputType = options.get('output-type', 'Application')
        fdp.output.path = options.get('output-path', os.path.relpath(os.path.abspath(os.path.join('dist', 'interface', projectName+'.swf')), destination))
        fdp.output.fps = options.get('fps', 30)
        fdp.output.width = options.get('width', 800)
        fdp.output.height = options.get('height', 600)
        fdp.output.version = options.get('version', 21)
        fdp.output.minorVersion = options.get('minorVersion', 0)
        fdp.output.platform = options.get('platform', 'Flash Player')
        fdp.output.background = options.get('background', '#FFFFFF')
        fdp.libraryPaths.append(os.path.relpath(os.path.abspath(os.path.join('dist', 'interface')), destination))
        fdp.classpaths = [os.path.normpath(x) for x in COMMON_PACKAGE_PATHS]
        fdp.compileTargets = targets
        fdp.Save(as3proj)
        ADDED_FILES+=[as3proj]


def ResolveDependency(url):
    if url in IMPORT_OVERRIDES:
        return IMPORT_OVERRIDES[url]
    else:
        resolved = os.path.join('exported', 'interface', url)
        IMPORT_OVERRIDES[url] = resolved
        return resolved


def DetectSWFInfo(filename):
    os_utils.ensureDirExists('swfxml')
    destfilename = os.path.join('swfxml', os.path.basename(filename) + '.xml')
    cmd = [
        'java', '-jar',
        PATHS['ffdec'],
        '-cli',
        '-swf2xml', filename, destfilename
    ]
    os_utils.cmd(cmd, critical=True, echo=True, show_output=True)
    data = {
        'dependencies': {},
        'ffdec-options': {},
        'files': {}  # File options
    }
    with codecs.open(destfilename, 'r', encoding='utf-8-sig') as f:
        xml = etree.parse(f)
        # As of FFDEC 9.0.0:
        # <item downloadNow="1" hasDigest="0" type="ImportAssets2Tag" url="gfxfontlib.swf">
        for result in xml.xpath("//item[@type='ImportAssets2Tag']"):
            url = result.attrib['url']
            url = os.path.normpath(url)
            resolved = ResolveDependency(url)
            if resolved not in data['dependencies']:
                data['dependencies'][url] = resolved
    return data

if __name__ == '__main__':
    import argparse
    argp = argparse.ArgumentParser()
    argp.add_argument('--i-understand-this-will-break-stuff', action='store_true', help='Required.')
    argp.add_argument('--no-dupechecks', action='store_true', help="Do not perform any duplication reduction operations.")
    argp.add_argument('--no-git', action='store_true', help="Skip git operations.")
    argp.add_argument('--resume', action='store_true', help="Only move files. No decompiling.")
    args = argp.parse_args()
    if not args.i_understand_this_will_break_stuff:
        log.info('Safety enabled, not breaking stuff.')
        log.info('To break everything and perform a full decompile of the Scaleform files, seek --help.')
        sys.exit(0)

    log.info('Preparing...')
    CONFIG = YAMLConfig('config.yml', {})
    CONFIG.Load('user-config.yml', merge=True, defaults={
        'paths': {
            'ffdec': 'path/to/ffdec.jar'
        }
    })
    PATHS = CONFIG['paths']
    IMPORT_OVERRIDES = CONFIG.get('import-overrides', [])
    SHARED_LIBRARIES = CONFIG.get('shared-libraries', {})
    COMMON_PACKAGE_PATHS = ['/'.join(['..', x, 'src']) for x in SHARED_LIBRARIES.keys() + ['COMMON']]
    for libname, libinfo in SHARED_LIBRARIES.items():
        for pkgname in libinfo['packages']:
            SHARED_PACKAGES[pkgname] = libname
            log.info('Shared package %s assigned to library %s', pkgname, libname)
            if not args.resume: os_utils.safe_rmtree(os.path.join('src', libname))
    '''
    for root,dirs,files in os.walk('exported'):
        for filename in files:
            if filename.endswith('.swf'):
                absfilename=os.path.abspath(os.path.join(root,filename))
                relfilename = os.path.relpath(absfilename,'exported')
                CONFIG['targets'][relfilename]=DetectSWFInfo(absfilename)
    CONFIG['import-overrides']=IMPORT_OVERRIDES
    with open('interpreted.config.yml','w') as f:
        yaml.dump(CONFIG.cfg,f,default_flow_style=False)
    '''
    if not args.resume:
        os_utils.safe_rmtree(os.path.join('src', 'common'))
        for target, targetconfig in CONFIG['targets'].items():
            fulltarget = os.path.abspath(os.path.join('exported', target))
            targetname = os.path.splitext(os.path.basename(target))[0]
            fulldest = os.path.abspath(os.path.join('src', targetname))
            DecompileSWF(fulltarget, fulldest, {k: os.path.abspath(v) for k, v in targetconfig.get('dependencies', {}).items()}, targetconfig.get('ffdec-options', {}), targetconfig.get('files', {}))

    '''
    for root,_,files in os.walk('src'):
        for filename in files:
            if not filename.endswith('.as'): continue
            absfilename = os.path.abspath(os.path.join(root, filename))
            filenameonly = os.path.basename(absfilename)
            relfilename = os.path.relpath(absfilename,'.')
            if os.path.dirname(relfilename) == os.path.join('src','COMMON'): continue
            # src/butts/src/subdir/aaaaa.as -> subdir/aaa.as
            relfilename = os.sep.join(relfilename.split(os.sep)[3:])

            md5 = utils.md5sum(absfilename) # Yes, MD5 has been cracked.  We're not hashing a password, so this is fine.
            if md5 not in Hashes:
                Hashes[md5]=[]
            Hashes[md5].append((absfilename,relfilename))
    '''
    if not args.resume:
        with codecs.open('.hashes.yml', 'w') as f:
            yaml.dump_all([HASHES, PROJECTINFO], f, default_flow_style=False)
    if args.resume:
        data = []
        with codecs.open('.hashes.yml', 'r') as hf:
            HASHES, PROJECTINFO = yaml.load_all(hf)

    ADDED_FILES = []
    for md5, filerecords in HASHES.items():
        absfilename, relfilename = filerecords[0]
        rfnChunks = relfilename.split(os.sep)
        #indexLastSrc=len(rfnChunks) - 1 - rfnChunks[::-1].index('src')
        # packageChunks=rfnChunks[indexLastSrc+1:-1]
        packageChunks = rfnChunks[0:-1]
        package = ''
        if len(packageChunks) > 0:
            package = packageChunks[0]
        # print(repr(package))
        relfilename = os.path.join(BASE_PATH, 'src', SHARED_PACKAGES[package] if package in SHARED_PACKAGES else 'COMMON', 'src', relfilename)
        if (len(filerecords) > 1 and not args.no_dupechecks) or (package in SHARED_PACKAGES):
            with log.info('MD5 %s has %d known files', md5, len(filerecords)):
                destfilename = relfilename
                if os.path.isfile(absfilename) and destfilename != absfilename:
                    if os.path.isfile(destfilename):
                        os.remove(destfilename)
                    log.info('%s -> %s', os.path.relpath(absfilename), os.path.relpath(destfilename))
                    os_utils.ensureDirExists(os.path.dirname(destfilename))
                    shutil.copy(absfilename, destfilename)
                ADDED_FILES += [destfilename]
                for absfilename, _ in filerecords:
                    cfg = FILECFG.get(absfilename, {})
                    if os.path.isfile(absfilename) and not cfg.get('keep', False):
                        log.info('Removing duplicate: %s', os.path.relpath(absfilename))
                        os.remove(absfilename)
        else:
            ADDED_FILES += [absfilename]
    for info in PROJECTINFO:
        WriteFlashDevelopProject(info)
    removeEmptyFolders('src')
    if not args.no_git and os.path.isdir('.git'):
        with log.info('Updating git...'):
            while len(ADDED_FILES) > 0:
                currentAdded = [os.path.relpath(x) for x in ADDED_FILES[:10]]
                del ADDED_FILES[:10]
                os_utils.cmd(['git', 'add'] + [x for x in currentAdded if os.path.isfile(x)], critical=False, echo=True, show_output=True)
