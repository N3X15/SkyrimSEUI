# Skyrim Special Edition GUI - Decompiled
This is the decompiled Scaleform UI code and assets from Skyrim Special Edition.  <s>It will hopefully be used in porting SkyUI to SkyrimSE.</s> It will be used in a new UI framework for Skyrim SE, since SkyUI's developer specifically asked me not to port SkyUI to SE.

Flash CS6 is required to compile these.  In the future, I will work on making these files available to FlashDevelop users.

I hope you find it useful.  I'll try to update this and comment it when I have time. MRs are welcome. - N3X15

# Version
1.1.50.0.8

-----

The following is copied, (mostly) verbatim, from https://github.com/Mardoxx/skyrimui, the inspiration behind this project.  All credits go to the SkyUI team.

# Permission instructions
You may use the decompiled/reconstructed vanilla source files in your mod without explicit permission.
You must give us, the SkyUI team, credit when doing so. While we are not the original authors of this code, decompiling and fixing it up again took a lot of work that deserves mentioning.

# Disclaimer
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Thanks to MIT license for providing this standard disclaimer.

Thanks to Bethesda Game Studios for creating The Elder Scrolls V: Skyrim, providing the base content and allowing us to mod it.
